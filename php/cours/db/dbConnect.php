<?php
//Pour se connecter à une base de données en PHP
//Il faut utiliser une classe d'objets appelée PDO (Php Data Objects)
//la clase PDO contient des méthodes permettant de dialoguer avec une BDD 
//ici, on va contacter un serveur de bdd mysql et demander l'accès a une base de test
//Pour instancier PDO il faut lui préciser où se connecter, et qui souhaite se connecter

//Le DSN (Data Source Name) demandé en premier paramètre est la string qui contient les informations de connexion
//Elle s'écrit au format suivant $driver:host=$hostname;dbname=$dbname;charset=utf8
//Les deux paramètres suivants sont le nom d'utilisateur et le mot de passe
//NE JAMAIS UTILISER ROOT AILLEURS QUE DANS CE COURS OU POUR VOS EXERCICES PERSONNELS
//IL FAUT CREER DES UTILISATEURS AVEC DES DROITS MOINS IMPORTANTS POUR DIALOGUER AVEC UNE BDD

//dbh signifie database handler (gestionnaire de base de donneés)
//dans $dbh est stocké l'objet PDO qu'on a instancié avec les données de notre SGBD
$dbh = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'password', [PDO::ATTR_EMULATE_PREPARES => false]);
//PDO::ATTR_EMULATE_PREPARES => false permet de ne pas faire semblant de préparer les requêtes
//et donc d'avoir de véritable requêtes préparées

//pour préparer nos requêtes SQL on écrit des chaines de caractères 
//pour créer une table par exemple
//$sqlQuery = "CREATE TABLE test_php (id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, label VARCHAR(255))";
//ou pour lire des données
$sqlQuery = "SELECT * FROM test_php";
//ensuite on demande à notre Database Handler (PDO) d'établir un statement (une déclaration)
//pour ça on appelle la méthode prepare
$stmt = $dbh->prepare($sqlQuery);
//enfin pour exécuter notre requête on appelle la méthode execute de notre objet statement
$stmt->execute();
//si on attends aucune réponse on peut s'arrêter là, mais dans le cas d'un SELECT par exemple, on a besoin de récupérer les résultats de notre requête
//pour ça on utilise les méthodes fetch ou fetchAll
//fetch récupère une ligne à la fois, et fait avancer le curseur pour le prochain fetch
//ce qui veut dire que pour 10 lignes de résultat, il faudrait exécuter fetch 10 fois
//fetchAll quand à lui récupère toutes les lignes et les range dans un tableau 
$res = $stmt->fetchAll(PDO::FETCH_CLASS);
//par défaut fetchAll renvoie les données sous deux formes
//Une forme de tableau ou chaque colonne est identifiée par un index
//une forme de clés valeur ou chaque colonne est identifiée par son nom
//Pour ne choisir qu'une seule forme il faut préciser un paramètre à fetchAll()
//Ici, avec PDO::FETCH_CLASS, le résultat sera un tableau d'objets dont les propriétés contiendront les valeurs de nos colonnes

//on peut ensuite manipuler ce tableau pour par exemple afficher le résultat à l'écran
foreach ($res as $ligne){
    //pour chaque ligne de résultat, on affiche les données récupérées
    echo sprintf("id: %d, label: %s<br>", $ligne->id, $ligne->label);
}

//une fois notre utilisation de la base de données terminée
//on ferme la connexion de la façon suivante
$stmt = null;
$dbh = null;
//normalement, PHP se charge de nettoyer derrière nous, mais ça peut être une précaution supplémentaire