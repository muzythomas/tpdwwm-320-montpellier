<?php
include('db.php');

$dbh = dbConnect('test', 'root', 'password');
$username = "toto";
$password = "tata";
//pour effectuer une connexion on doit vérifier que l'utilisateur et le mot de passe correspondent
//seulement dans le cas d'un mot de passe haché, on va comparer les hash et pas les mot de passe directement
//pour ce faire il faut qu'on récupère le hash stocké en base 
$stmt = $dbh->prepare("SELECT * FROM users WHERE username = :username");

$stmt->execute([
    ':username' => $username
]);

//une fois notre utilisateur et son mot de passe récupérés
$user = $stmt->fetch(PDO::FETCH_OBJ);
if ($user){
    //on utilise password verify
    if (password_verify($password, $user->password)) {
        //si les deux hash correspondent on affiche un message de réussite
        echo "connexion reussie";
    }
}