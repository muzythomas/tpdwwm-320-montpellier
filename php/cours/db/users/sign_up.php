<?php
include('db.php');

$dbh = dbConnect('test', 'root', 'password');
$username = "toto";
$password = "tata";
//pour hacher un mot de passe on utilise password hash
//password_hash demande le mot de passe en clair, ainsi que l'algorithme à utiliser
//utiliser PASSWORD_DEFAULT signifique que PHP utilisera l'alogirhtme par défaut qui est souvent le meilleur disponible
$password = password_hash($password, PASSWORD_DEFAULT);
$stmt = $dbh->prepare('INSERT INTO users (username, password) VALUES (:username, :password)');
$stmt->execute([
    ':username' => $username,
    ':password' => $password
]);