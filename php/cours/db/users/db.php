<?php

function dbConnect($dbname, $username, $password){
    //Une exception est un type d'erreurs envoyé par un objet dans certains cas
    //dans le corps d'une classe on peut utiliser "throw new Exception" pour envoyer une erreur sous forme d'exception
    //dans la gestion des erreurs via Exceptions, on peut donc attraper ces erreurs pour décider de les gérer à notre façon au lieu de juste faire planter le programme 
    //Try signifie "essaye d'exécuter le code suivant"
    try{
        $dbh = new PDO("mysql:host=localhost;dbname={$dbname};charset=utf8", $username, $password, [PDO::ATTR_EMULATE_PREPARES => false]);
    } catch (PDOException $exception){
        //catch signifie attrappe les Exceptions (ou erreurs) qui pourraient survenir
        //on peut indiquer à catch quel genre d'erreur attrapper en spécifiant son type
        //ici on attrappe notre PDO Exception, on la nomme $exception
        //et si on en attrappe une, on décide de planter la page avec die() et afficher le message d'erreur
        die('Une erreur de base de données est survenue, réessayez plus tard ou contactez l\'administateur');
    }

    return $dbh;
}
