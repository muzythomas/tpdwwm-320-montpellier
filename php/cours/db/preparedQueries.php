<?php
//avec PDO on a la possibilité d'utiliser des requêtes SQL dites préparées
//ces requêtes sont comme des phrases à trou, avec des paramètres qui peuvent changer à chaque exécution
//l'utilisation classique serait de passer à note requête préparée des paramètres récupérés depuis un formulaire
//Lorsqu'on utilise prepare(), on peut mettre dans le texte de notre requête SQL un paramètre en tapant un nom précédé de :, :id par exemple serait le paramètre id 
//imaginons que les deux variables suivantes aient été récupérées depuis $_POST
$id = 15;
$rent = 200;
$orderBy = "rent";

//on se connecte ensuite a notre base de données
$dbh = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'password');
//on prépare notre requête en laissant des paramètres à l'endroit où l'on devrait préciser l'id qui nous intéresse ainsi que le loyer minimum de notre requête
//le paramètre id devient :id, rent devient :rent
$stmt = $dbh->prepare("SELECT * FROM appart WHERE id = :id AND rent > :rent ORDER BY ".$rent);
//au moment de l'execution, on indique à PDO par quoi l'on veut remplacer ces paramètres 
//on lui donne pour se faire un tableau associatif contenant, en couple clé valeur : le nom du paramètre suivi de sa valeur, chaque nouveau couple clé valeur séparé par des virgules
$stmt->execute(
    [
    ":id" => $id,
    ":rent" => $rent
    ]
);
//on récupère ensuite notre résultat normalement avec fetch ou fetchAll
var_dump($stmt->fetchAll(PDO::FETCH_ASSOC));

//certaines partie d'une requête SQL ne peuvent pas être paramétrables via PDO
//c'est le cas pour la selection de la table, on ne peut pas faire  SELECT * from :table par exemple, on doit toujours préciser la table directement
//c'est le cas également pour ORDER BY, on ne peut pas faire ORDER BY :column par exemple
