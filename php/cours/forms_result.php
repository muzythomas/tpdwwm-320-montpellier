<?php
//ici on traite les données reçues via notre formulaire forms.php

//$_POST est une variable dite "superglobale" contenant sous forme de tableau associatif
//les données d'une requête HTTP POST, si requête POST il y a
//le tableau sera donc rempli par des couples clé/valeur, les clés étant les noms des input
//d'un formulaire par exemple, et les valeurs les données de ce formulaire

//pour vérifier si des données ont bien été envoyées, il faut donc vérifier si $_POST a été initialisée
//pour ce faire, on peut utiliser la fonction isset() 
//on vérifie qu'on ait reçu des données
if ( isset($_POST['nickname']) && isset($_POST['fruit']) ){
    //une fois l'existence de ces données vérifiées on peut décider de les traiter
    echo "Bonjour {$_POST['nickname']}, votre fruit préféré est {$_POST['fruit']}";
} else {
    echo "Erreur formulaire";
}