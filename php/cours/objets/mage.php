<?php
//require_once est un include obligatoire, seulement si le fichier n'a pas déjà été importé auparavant
require_once('personnage.php');


//La notion d'héritage est une technique de POO permettant de créer 
//des sous classes à une classe Mère, réutilisant ainsi des caractéristiques et méthodes
//d'une classe, dans le but de la spécialiser
//grâce au mot clé extends on indique ici que notre nouvelle classe Mage
//hérite de Personnage, ce qui fera des objets instanciés de Mage des Personnages également

//class Mage extends Personnage signifie "nouvelle classe Mage héritant de Personnage"
class Mage extends Personnage
{
    //ici la classe Mage possède une nouvelle propriété par rapport à Personnage
    //la mana représente les points de magie de notre personnage
    private $_mana = 100;
    private $_longueurBarbe;

    //on surcharge la constante présente dans Personnage pour personnaliser notre Mage
    const MIN_INT = 6;

    //si on a besoin de préciser de nouveaux attributs dans un constructeur propre à notre classe Mage
    //il faut donc réécrire la fonction __constructor
    //cependant, il faut tout de même qu'on précise toutes les propriétés qui sont propres aux Personnages
    //étant donné que le Mage reste un Personnage
    //Dans notre implémentation, on décide cependant de ne pas pouvoir préciser la force et la dexterité d'un Mage
    public function __construct($hp, $int, $name, $longueurBarbe){
        //pour construire un Mage, il faut construire un Personnage
        //pour construire un Personnage on appelle parent::__construct qui est le constructeur du parent
        //ici parent cible la class Personnage 
        //et le constructeur est celui de Personnage
        //ce constructeur ayant des paramètres qu'on ne peut pas changer ici, on doit les préciser
        parent::__construct($hp, 2, 2, $int, $name);
        //maintenant que le constructeur parent a été appelé, on peut s'occuper des attributs de Mage
        $this->_longueurBarbe = $longueurBarbe;
    }

    //on peut également surcharger/surimplémenter les fonctions déjà existantes dans la classe mère
    //pour les personnaliser/changer leur résultat
    public function presentation() {
        echo "{$this->name()}, enchanté, j'ai une intelligence de {$this->int()}. Ma barbe fait {$this->_longueurBarbe} cm";
    }

    //et bien évidemment on peut créer des fonctions nouvelles 
    public function jeterSort($personnageCible){
        //ici on pourrait écrire du code enlevant du mana
        //et inffligeant des dégâts en fonction de l'intelligence
    }
}
