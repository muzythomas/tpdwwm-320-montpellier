<?php
class Personnage
{
    //une classe d'objet possède des propriétés, ou attributs
    //qui permettent de personnaliser notre objet
    //private signifie que l'attribut ne peut être lu que par l'objet lui même
    private $_xp = 0; //un personnage possèe de l'expérience, on lui mets une valeur par défaut de 0
    private $_hp; //un personnage possède des points de vie
    private $_str; //un personnage possède de la force
    private $_dex; //un personnage possède de l'agilité
    private $_int; //un personnage possède de l'intelligence
    private $_name; //un personnage possède un nom
    private $_alive = true; //un personnage possède un statut en vie

    //on peut également définir des constantes (écrites par convention en majuscules)
    //pour accéder aux constantes et aux propriétés statiques d'une classe
    //on utilise :: au lieu de ->
    //Dans le corps de la classe, on doit utiliser static au lieu de $this pour cibler la classe
    //et non l'objet 
    const MAX_STR = 99;
    const MAX_DEX = 99;
    const MAX_INT = 99;
    const MAX_HP = 999;
    const MIN_STR = 1;
    const MIN_DEX = 1;
    const MIN_INT = 1;
    const MIN_HP = 0;


    //le constructeur est une fonction spéciale propre aux classes
    //qui permet l'instanciation des objets a partir d'une classe
    //il est toujours écrit __construct() et est toujours public
    public function __construct($hp, $str, $dex, $int, $name)
    {
        //notre constructeur utilise nos méthodes pour initialiser les attributs de notre personnage
        $this->setHp($hp);
        $this->setStr($str);
        $this->setDex($dex);
        $this->setInt($int);
        $this->setName($name);
    }

    //en POO, on utilise des accesseurs et mutateurs (getters & setters)
    //pour pouvoir lire et modifier les attributs privés d'un objet

    //nos getters (accesseurs) sont comme une vitrine permettant de lire les valeurs mais pas les modifier
    public function str()
    {
        return $this->_str;
    }
    public function hp()
    {
        return $this->_hp;
    }
    public function name()
    {
        //un getter (accesseur) peut également contenir de la logique de programmation
        //ici on décide que la lecture du nom se fait forcément en majuscule
        return strtoupper($this->_name);
    }
    public function xp()
    {
        return $this->_xp;
    }
    public function dex()
    {
        return $this->_dex;
    }
    public function int()
    {
        return $this->_int;
    }

    //nos setters (mutateurs) servent de "pare feu" lors de la modification des valeurs d'attributs
    //et permettent de placer des conditions sur ces modifications
    //ici on empêche une statistique d'être modifiée en dehors d'un certain intervalle
    public function setStr($str)
    {
        //is_int permet de vérifier qu'une valeur soit un nombre entier
        if (is_int($str)) {
            $this->_str = $str; //on modifie la valeur interne
        }
        //puis on la ramène dans les bornes acceptables après coup
        //ici pour accéder a notre constante de classe
        //on utilise self pour cibler la classe, et :: pour accéder à une constante ou propriété statique
        //de cette classe. Ici on utiliserait self::MIN_STR  au lieu d'utiliser $this->MIN_STR par exemple
        //dans le cas présent, comme on veut pouvoir hériter de ces constantes et les modifier à loisir
        //ainsi on remplace self par static pour que la constante utilisée soit celle de la classe courante
        //c'est à dire Personnage ou n'importe quelle classe fille
        if ($this->_str < static::MIN_STR) {
            $this->_str = static::MIN_STR;
        }
        if ($this->_str > static::MAX_STR) {
            $this->_str = static::MAX_STR;
        }
    }
    public function setDex($dex)
    {
        if (is_int($dex)) {
            $this->_dex = $dex;
        }
        if ($this->_dex < static::MIN_DEX) {
            $this->_dex = static::MIN_DEX;
        }
        if ($this->_dex > static::MAX_DEX) {
            $this->_dex = static::MAX_DEX;
        }
    }
    public function setInt($int)
    {
        //ne pas confondre $int notre caractéristique d'intelligence
        //et int le nombre entier
        if (is_int($int)) {
            $this->_int = $int;
        }
        if ($this->_int < static::MIN_INT) {
            $this->_int = static::MIN_INT;
        }
        if ($this->_int > static::MAX_INT) {
            $this->_int = static::MAX_INT;
        }
    }

    public function setHp($hp)
    {
        if (is_int($hp)) {
            $this->_hp = $hp;
        }
        //ici, les hp peuvent tomber à zéro pour qu'un personnage meure
        if ($this->_hp < static::MIN_HP) {
            $this->_hp = static::MIN_HP;
        }
        if ($this->_hp > static::MAX_HP) {
            $this->_hp = static::MAX_HP;
        }

        //si les points de vie tombent à zéro
        if ($this->hp() == static::MIN_HP) {
            //le personnage est mort
            $this->_alive = false;
            echo "{$this->name()} nous a quitté...<br/>";
        }
    }

    public function setName($name)
    {
        $this->_name = $name;
    }

    //ici on a pas besoin d'une fonction setXp car l'xp ne peut qu'augmenter dans notre jeu
    //on crée donc une fonction qu'on nomme addXp pour plus de clarté
    //qui ne permet que d'ajouter de l'xp en plus au personnage
    public function addXp($xp)
    {
        //on empêche d'ajouter un nombre négatif
        if ($xp > 0) {
            $this->_xp += $xp;
        }
    }

    public function alive()
    {
        return $this->_alive;
    }

    //on peut déclarer des méthodes dans une classe
    //les méthodes sont des fonctions propres à la classe dans laquelle elles sont déclarées

    //public permet a la méthode d'être accessible, par tout le monde, en dehors de l'objet 
    public function presentation()
    {
        //$this est un mot clé spécifique aux objets permettant à ceux ci de se cibler eux même
        echo
            "Je suis {$this->name()}, tremblez devant mon intelligence de {$this->int()}, 
        ma force de {$this->str()} et mon agilité incroyable de {$this->dex()}<br/>";
    }

    public function frapper($personnageCible)
    {
        //on vérifie si un personnage est en vie avant de l'attaquer
        if ($personnageCible->alive()) {
            $hpCible = $personnageCible->hp(); //on récupère les hp de la cible de l'attaque
            $degats = $this->str() * 2; //on multiplie les dégats par un ratio fixe 
            //on affiche un message d'information
            echo "{$this->name()} frappe {$personnageCible->name()} pour {$degats} de dégats. <br/>";
            $personnageCible->setHp($hpCible - $degats); //on donne a notre cible la valeur des hp - les dégats
            //si notre cible est toujours en vie 
            if ($personnageCible->alive()) {
                //on affiche ses points de vie restants
                echo "{$personnageCible->name()} a désormais {$personnageCible->hp()} points de vie!<br/>";
            }
        } else {
            //on affiche un message d'information différent
            echo "{$this->name()} essaye de taper... Mais pas besoin car {$personnageCible->name()} est déjà mort !<br/>";
        }
    }
}
