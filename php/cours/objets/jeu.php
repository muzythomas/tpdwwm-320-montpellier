<?php
//on récupère notre classe de personnage
include('personnage.php');
include('mage.php');

//pour instancier un objet de classe Personnage on utilise new
//Ici on appelle Personnage::MIN_STR par exemple pour accéder a une constante de la classe Personnage
$heros = new Personnage(100, Personnage::MIN_STR, Personnage::MIN_DEX, Personnage::MIN_INT, "Bruno le lèche cul");

//pour accéder à une propriété ou méthode d'un objet instancié
//on appelle la variable dans laquelle l'objet est stocké, et on utilise une ->
//ici on invoque la méthode (ou fonction) presentation dans l'objet de classe Personnage
$heros->presentation();

$ennemi = new Personnage(100, 10, 10, 10, "JM le Ninja");
$ennemi->presentation();

$heros->addXp(50);
$heros->setStr(40); //on renforce un peu notre héros à l'aide de notre setter setStr()
echo "{$heros->name()} gagne en puissance, il a désormais {$heros->str()} de Force! ";

$heros->frapper($ennemi);
$heros->frapper($ennemi);
$heros->frapper($ennemi);

$magicien = new Mage(100, 4, 'Gondolf le Vénitien', 12);
$magicien->presentation();