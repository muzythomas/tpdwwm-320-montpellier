<?php
//pour récupérer une session déjà ouverte, on utilise également
session_start(); //qui démarre/récupère une session php
//si on a un panier stocké dans notre session
if (isset($_SESSION['panier'])){
    //on y rajoute des chaussettes
    array_push($_SESSION['panier'], 'chaussettes');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page 2</title>
</head>
<body>
    <nav>
        <a href="session.php">Page 1</a>
        <a href="destroy.php">Deconnexion</a>
    </nav>
    <h1>Page 2</h1>
    <?php
    //on affiche notre panier s'il n'est pas vide
    if (!empty($_SESSION['panier'])){
        print_r($_SESSION['panier']);
    } else {
        echo "Panier vide";
    }
    ?>
</body>
</html>