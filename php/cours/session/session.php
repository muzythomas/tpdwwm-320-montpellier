<?php
//une session en PHP est un moyen d'utiliser un stockage de variables
//qui sera accessible sur toutes les pages de notre site 
//de façon à pouvoir effectivement "transmettre" des données d'une page à une autre
//pour commencer une session dans PHP 
//on utilise session_start()
//session_start() doit être invoqué avant qu'un quelconque texte soit imprimé sur la page
//donc avant tout code html, ou "echo" ou "var_dump" etc
//session_id() permet de lire et modifier l'id de session PHP
//echo session_id() afficherai l'id de la session (l'id stocké dans le cookie PHPSESSID)
//et session_id("nouvelid") permet de définir l'id de la session, et donc de le stocker dans PHPSESSID
//si session_id n'est pas précisé, session_start() génère un id neuf, sauf si PHPSESSID est présent dans les cookies
session_start();
//pour définir un temps d'expiration de notre session
//on peut utiliser session_cache_expire()
//session_cache_expire prends une valeur numérique définissant le temps en minute jusqu'à expiration de la session
//session_cache_expire(30) donne 30 minutes avant expiration

//de la même façon que pour les requêtes POST et GET
//on accède aux données de session via une superglobale nommée $_SESSION
//exemple de stockage d'une donnée en session 
//exemple initialisation d'un panier vide
//si un panier n'existe pas déjà en session
if (!isset($_SESSION['panier'])){
    $_SESSION['panier'] = []; //notre panier est modélisé par un tableau
}
//recommandations de sécurité 
//https://www.php.net/manual/fr/session.security.php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Page 1</title>
</head>
<body>
    <nav>
        <a href="session2.php">Page 2</a>
        <a href="destroy.php">Deconnexion</a>
    </nav>
    <h1>Page 1</h1>
    <?php
        if (!empty($_SESSION['panier'])){
            print_r($_SESSION['panier']);
        } else {
            echo "Panier vide";
        }
    ?>
</body>
</html>