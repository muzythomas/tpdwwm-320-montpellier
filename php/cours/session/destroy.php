<?php
//pour supprimer une session (ex: déconnexion)
//il faut d'abord la récupérer 
session_start();
//puis on la supprime
session_destroy();
header('Location: session.php');