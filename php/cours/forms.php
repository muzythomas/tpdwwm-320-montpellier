<!--
    Un formulaire HTML se définit à l'aide de la balise form
    on peut définir le type de requête ainsi que la destination 
    de traitement du formulaire en précisant "method" (GET, POST...)
    ainsi que "action" (l'url du script de traitement)
    Si aucune "action" n'est définie, le traitement se fait sur 
    la page courante
!-->

<form method="POST" action="forms_result.php">
    <!--
         pour récupérer des données entrées du côté client (front)
         on définit des champs input 
         Le type d'input permet de définir quelle forme notre champ
         prendra du côté client, mais ne sert pas de vérification
         ou de sécurité
    !-->
    <label for="nickname">Pseudo</label>
    <!-- for est lié à l'id du champ input correspondant
        name est la clé de la valeur envoyée par HTTP POST
    -->
    <input id="nickname" name="nickname" type="text">

    <!-- select définit une liste déroulante !-->
    <select name="fruit">
    <!-- chaque option est définie grace a la balise option 
        value est la valeur envoyée dans http post, le texte entre
        balise est juste un label de l'option (ce qui va s'afficher)
    !-->
        <option value="fraise">Fraise</option>
        <option value="banane">Banane</option>
        <option value="pomme">Pomme</option>
        <option value="poire">Poire</option>
        <option value="mangue">Mangue</option>
    </select>

    <!--input type checkbox définit une case à cocher, la case à cocher
    envoie une donnée si elle est cochée, et n'envoie rien sinon !-->
    <label for="agree">Acceptez vous le contrat d'utilisation ?</label>
    <input id="agree" name="agree" type="checkbox">

    <!-- le type radio définit un bouton radio
    les boutons radios sont un groupe de boutons parmi lesquels
    un seul peut être selectionné 
    on donne le même nom aux boutons radio d'un même groupe,
    mais pas la même valeur !-->

    <label for="oui">Oui</label>
    <input id="oui" name="choix_unique" value="y" type="radio">
    <label for="non">Non</label>
    <input id="non" name="choix_unique" value="n" type="radio">
    <label for="peutetre">Peut être</label>
    <input id="peutetre" name="choix_unique" value="mb" type="radio">

    <!-- le bouton d'envoi se définit ainsi !-->
    <input type="submit" value="Envoyer"> 
</form>