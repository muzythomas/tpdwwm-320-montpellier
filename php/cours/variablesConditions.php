<?php
//les variables en php ont leur déclaration (et nom) précédées de $
$message = "Coucou"; //type string
$entier = 15; //type int (entier)
$flottant = 15.5; //type float (double ou + de précision)
$boolean = true; //booléens 
$rien = NULL; //vide

//étant donné que PHP renvoie toujours du texte
//pour afficher quelque chose à l'écran il faut pouvoir écrire du texte
//pour ça la fonction echo existe
echo "Message de bienvenue : $message";
//php permet d'insérer des variables dans une chaîne directement avec $
//mais pour améliorer la lecture/maintanabilité du code on utilise
//souvent les concaténations
//la concaténation en php s'utilise avec le . 
echo "Le nombre est : " . ($entier + 5) . " !";

//mise en place d'un branchement conditionnel
$age = 11;
echo "<p> age : $age -> ";

if ($age < 18){
    echo "Accès interdit";
} elseif ($age > 20) {
    echo "venez boire un coup";
} else {
    echo "on regarde mais on ne boit pas";
}

echo "</p>";

//opérateurs AND(&&) et OR(||)
$langue = "fr";
$autorisation = true;
$messageFr = "Bienvenue !";
$messageEn = "Welcome !";
if ($autorisation && $langue === "fr"){
    echo $messageFr;
} elseif ($autorisation and $langue === "en"){
    echo $messageEn;
}