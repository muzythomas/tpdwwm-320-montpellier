<?php
//historiquement en php un tableau se crée à l'aide d'une fonction
//array()
$valeurs = array(1,2,3,4); //dans $valeurs sera stocké [1, 2, 3, 4]
//depuis php5 le tableau peut se créer "à la volée" comme en js 
$voitures = ["fiat", "ferrari", "alfa romeo"];
//on accède à une case du tableau via son index : 
echo $voitures[1]; //affiche ferrari 

//une grosse différence avec js est que les tableaux php sont mutables
//pour affecter une nouvelle valeur à une case de tableau
//on peut agir comme avec n'importe quelle variable
$voitures[0] = "lancia";
echo $voitures[0];

//pour ajouter une valeur à la suite d'un tableau 
//on peut utiliser des crochets vides
$voitures[] = "lamborghini";
echo $voitures[3];
//on peut également utiliser array_push() permettant d'ajouter
//plusieurs variables en une fois

//pour ajouter deux éléments en fin de tableau
array_push($voitures, "peugeot", "renault");
//pour ajouter des éléments en début de tableau
array_unshift($voitures, "mitsubishi", "toyota");

//si on désire afficher chaque élément d'un tableau
//on peut classiquement parcourir le tableau avec for
//et utiliser echo
//count() permet de renvoyer le nombre d'éléments dans un tableau
//et donc sa taille
for ($i = 0; $i < count($voitures); $i++){
    echo '<br/>'. $voitures[$i] ;
}
//count sert aux tableaux mais pas aux string
//les string utilisent mb_strlen() (et non strlen())
//de cette façon : mb_strlen("salut")


//en php il existe aussi un autre type de tableaux
//les tableaux associatifs, ou dictionnaire clé/valeur
$villes = [
    "34000" => "Montpellier",
    "75000" => "Paris",
    "31000" => "Toulouse",
    "13000" => "Marseille"
];

echo $villes["34000"]; //renvoie Montpellier
//les données d'un tableau associatif ne sont pas accessible par des index
//les clés sont des chaines de caractère
//pour récupérer chaque élément d'un tableau associatif on ne peut pas
//utiliser un for avec $i
//on doit donc utiliser un foreach($tableau as $cle => $valeur){}
foreach($villes as $code_postal => $ville) {
    echo "<br/>" . $code_postal . " : " . $ville;
}

//si on veut vérifier la présence d'une valeur dans un tableau
//on utilise in_array()
if (in_array("Montpellier", $villes)) {
    echo "<br/>Livraison possible à Montpellier";
}

