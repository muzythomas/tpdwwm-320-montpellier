<?php
$title = "Page 1";
//un include permet d'intégrer une portion de page a une autre
//en executant le code de la page inclue
//ici on inclut l'header de la page contenant les balises html d'en tête, body etc
include('header.php');
?>

<main class="container">
    <article id="article1">
        <h2>Article1</h2>
        <section>
            <h3>Section1</h3>
        </section>
        <section>
            <h3>Section2</h3>
        </section>
        <section>
            <h3>Section3</h3>
        </section>
    </article>
    <article id="article2">
        <h2>Article2</h2>
        <section>
            <h3>Section1</h3>
        </section>
        <section>
            <h3>Section2</h3>
        </section>
        <section>
            <h3>Section3</h3>
        </section>
    </article>
    <article id="article3">
        <h2>Article3</h2>
        <section>
            <h3>Section1</h3>
        </section>
        <section>
            <h3>Section2</h3>
        </section>
        <section>
            <h3>Section3</h3>
        </section>
    </article>
</main>

<?php
//ici on inclut le footer qui se chargera de finir la page
include('footer.php');