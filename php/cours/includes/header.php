<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title><?=$title?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link rel='stylesheet' type='text/css' media='screen' href='main.css'>
    <script src='main.js'></script>
</head>
<body>
    <nav>
        <ul>
            <li><a href="page1.php">Page1</a></li>
            <li><a href="page2.php">Page2</a></li>
            <li><a href="page3.php">Page3</a></li>
            <li><a href="page4.php">Page4</a></li>
            <li><a href="page5.php">Page5</a></li>
        </ul>
    </nav>
    <header>
        <h1>Super Site</h1>
        <p>
            Enorme site
        </p>
    </header>
