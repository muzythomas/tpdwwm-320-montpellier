<?php

//une fonction est une suite d'instructions effectuant une action et retournant un résultat
//les fonctions ont pour intérêt principal de permettre de réutiliser une portion de code
//sans le réécrire, et donc rendre le code plus lisible et plus modulable
//une fonction ne renvoyant pas de résultat est appelée procédure

//exemple une fonction calculant le volume d'un cone
function volumeCone($rayon, $hauteur){
    //return dans une fonction interrompt l'execution de celle ci 
    //et renvoie la valeur suivante 
    return pi() * $rayon * $rayon * $hauteur * 1/3;
}

//pour appeler la fonction, on invoque son nom et lui passe des paramètres
$volume = volumeCone(8, 10); //on peut stocker le résultat
echo volumeCone(3, 16); //ou l'utiliser autrement


//beaucoup de fonctions existent déjà dans le langage pour aider au développement
//de pi() à array_key_exists ou mb_strlen() etc