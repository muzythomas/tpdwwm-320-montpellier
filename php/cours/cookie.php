<?php
//un cookie est une forme de stockage côté client 
//permettant de persister des données via des couples clé valeur
//pour stocker une donnée sous forme de cookie on utilise la fonction 
//setcookie()
//clé : last_connected, valeur : date au format année-mois-jour
setcookie('last_connected', date('y-m-d:h-i-s'));

//il existe un troisième paramètre pour passer une date d'expiration au cookie
//s'il n'est précisé, le cookie expirera à la session
//pour passer une date d'expiration on utilise une timestamp
//une timestamp est le nombre de secondes passées depuis l'Epoch Unix (1er janvier 1970)
//la timestamp actuelle se récupère via time()
//pour rajouter une semaine de temps, on doit donc faire
setcookie('expire_dans_une_semaine', 'coucou', time() + (7* 24 * 60 * 60));

//pour accéder à la valeur d'un cookie, une fois défini,
//on utilise la superglobale $_COOKIE
echo $_COOKIE['last_connected'];

//pour modifier un cookie déjà mis en place on réutilise setcookie
setcookie('expire_dans_une_semaine', 'coucou salut', time() + (7* 24 * 60 * 60));

//pour supprimer un cookie on doit légèrement truander 
setcookie('last_connected', null, -1);
//pour être sûr de ne pas pouvoir utiliser la valeur du cookie après l'avoir 
//fait expirer
//on peut utiliser unset (qui désinitialise une variable) pour supprimer le registre
unset($_COOKIE['last_connected']);
