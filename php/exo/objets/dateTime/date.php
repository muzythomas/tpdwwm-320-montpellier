<?php

/**
 * Implémenter une class MaDate permettant de manipuler les dates du calendrier
 * une Date contiendra les attributs annee, mois et jour
 * une année est comprise entre 1 et 9999
 * un mois entre 1 et 12
 * un jour entre 1 et 31
 * 
 * la class MaDate doit également comprendre les méthode suivantes :
 * reglerJour(), reglerMois(), reglerAnnee()
 * qui permettront de régler une partie de la date avec une valeur 
 * MaDate devra également renvoyer un message d'erreur si une date invalide est rentrée par ce moyen
 * 
 * jourSuivant(), jourPrecedent()
 * moisSuivant(), moisPrecedent()
 * anneeSuivante(), anneePrecedente()
 * qui font avancer/reculer les différents composants de la date d'1 unité
 * 
 * estBissextile() qui renvoie true si l'année stockée est bissextile, false sinon
 * estValide() qui renvoie true si la date stockée est valide, false sinon
 * une date est Valide si l'année, le mois et le jour concordent.
 * Par exemple, 2020, 6, 5 est une année valide
 * mais 2020, 2, 31 est invalide car il n'y a pas 31 jours en février
 * 
 * 
 * jourSemaine() qui renvoie le jour correspondant à la date sous la forme d'index
 * (0 pour Dimanche, 1 pour Lundi... etc)
 * 
 * Enfin, une fonction afficher() qui prendra un paramètre $complete (true ou false)
 * si $complete est false, la date sera affichée au format suivant "2020/06/05"
 * si $complete est true, la date sera affichée au format "Vendredi, 05 Juin 2020"
 * 
 * Une fois la classe MaDate implémentée, écrire une méthode differenceDate($date) calculant la durée entre la date stockée et une date de type MaDate fournie en paramètre
 */


/**
 * NB correction:
 * ce code de notre classe ne prend pas en compte le fait de régler une date valide en une date invalide, ex: un 29 février 2008 peut passer en 29 février 2007 sans message d'erreur
 * TODO : on réglera ça plus tard 
 * TODO: gérer l'affichage complet de la date 
 */
class MaDate
{
    private $_annee;
    private $_mois;
    private $_jour;

    public function __construct($annee, $mois, $jour)
    {
        $this->reglerAnnee($annee);
        $this->reglerMois($mois);
        $this->reglerJour($jour);
    }

    const JOURS = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    const MOIS = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    public function affichage($complete) {
                
        //on fait correspondre nos jours avec l'index retrouvé
        $jourSemaine = self::JOURS[$this->jourSemaine()];
        $mois = self::MOIS[$this->mois() - 1];
        if ($complete){
            echo sprintf("%s, %02d %s %d", $jourSemaine, $this->jour(), $mois, $this->annee() );
        } else {
            echo sprintf("%d/%02d/%02d<br>", $this->annee(), $this->mois(), $this->jour());
        }
    }

    //renvoie le jour de la semaine sous forme d'index
    //0 pour dimanche 1 pour lundi etc
    
    public function jourSemaine(){
        if ($this->mois() >= 3){
            $res = ((23*$this->mois()/9) + $this->jour() + 4 + $this->annee() + ($this->annee()/4) - ($this->annee()/100) + ($this->annee()/400) - 2) % 7;
        } else {
            $res = ((23*$this->mois()/9) + $this->jour() + 4 + $this->annee() + (($this->annee()-1)/4) - (($this->annee()-1)/100) + (($this->annee()-1)/400)) % 7;
        }

        //NB: Pour tout millénaire, pour une dizaine supérieure a 0, et une unité à 9
        //le calcul de jour en décembre est augmenté de 1
        //PS: ceci est immonde, ne pas reproduire, c'est 100% du bonus 
        if ($this->mois() == 12 && ("" . $this->annee())[2] > 0 && ("" . $this->annee())[3] == 9){
            $res--;
        }
        return $res;
    }

    /** accesseurs */
    public function annee()
    {
        return $this->_annee;
    }
    public function mois()
    {
        return $this->_mois;
    }
    public function jour()
    {
        return $this->_jour;
    }

    /** mutateurs */
    public function reglerAnnee($annee)
    {
        $this->_annee = $annee;
        if (!$this->estAnneeValide()){
            echo "Annee invalide";
        }      
    }
    public function reglerMois($mois)
    {
        $this->_mois = $mois;
        if (!$this->estMoisValide()){
            echo "mois invalide";
        }
    }
    public function reglerJour($jour)
    {
        $this->_jour = $jour;
        if (!$this->estJourValide()){
            echo "jour invalide";
        }
    }

    /** incrémenteurs */
    //passe a l'année suivante
    public function anneeSuivante(){
        $this->reglerAnnee($this->annee() + 1);
    }
    //passe au mois suivant
    public function moisSuivant(){
        //si on est en décembre
        if ($this->mois() == 12){
            //et l'année augmente
            $this->anneeSuivante();
            //le mois suivant est janvier
            $this->reglerMois(1);
        } else {
            //sinon on augmente juste de 1
            $this->reglerMois($this->mois() + 1);
        }
    }
    //passe aujour suivant
    public function jourSuivant(){
        //si on est au jour maximum du mois courant 
        if ($this->jour() == $this->recupererJourMax()){
            //au avance au premier du mois suivant
            $this->reglerJour(1);
            $this->moisSuivant();
        } else {
            $this->reglerJour($this->jour() + 1);
        }
    }

    /** décrementeurs */
    public function anneePrecedente(){
        $this->reglerAnnee($this->annee() - 1);
    }

    public function moisPrecedent(){
        if ($this->mois() == 1){
            $this->anneePrecedente();
            $this->reglerMois(12);
        } else {
            $this->reglerMois($this->mois() -1 );
        }
    }

    public function jourPrecedent(){
        if ($this->jour() == 1){
            $this->moisPrecedent();
            $this->reglerJour($this->recupererJourMax());
        } else {
            $this->reglerJour($this->jour() - 1);
        }
    }    

    //renvoie true si la date stockée dans l'objet est valide
    public function estValide(){
        return $this->estAnneeValide() && $this->estMoisValide() && $this->estJourValide();
    }

    //renvoie true si l'année stockée est bissextile
    public function estBissextile(){
        //une année n'est bissextile que si elle divisible par 4 mais pas par 100
        //ou s'il est divisible par 400
        return ($this->annee() % 4 == 0 && $this->annee() % 100 != 0)  || $this->annee() % 400 == 0;
    }

    /** fonctions outils de notre classe */
    //vérifie qu'une année soit comprise entre 1 et 9999
    private function estAnneeValide(){
        return $this->_annee >= 1 && $this->_annee <= 9999;
    }
    //vérifie qu'un mois soit entre 1 et 12
    private function estMoisValide(){
        return $this->_mois >= 1 && $this->_mois <= 12;
    }
    //vérifie qu'un jour soit entre 1 et le jour maximum du mois courant
    private function estJourValide(){
        //si notre mois est valide
        if ($this->estMoisValide()){
            //on peut vérifier précisement si le jour est correct
            return $this->_jour >= 1 && $this->_jour <= $this->recupererJourMax();
        } else {
            //sinon on renvoie un test de validité moins précis ne prenant en compte que les valeurs minimales et maximales
            return $this->_jour >= 1 && $this->_jour <= 31;
        }
    }

    CONST JOURS_MOIS = [1 => 31, 2 => 28, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31, 11 => 30, 12 => 31];
    //renvoie le jour maximum de chaque mois en prenant compte février en année bissextile
    private function recupererJourMax(){
        if ($this->estBissextile() && $this->mois() == 2){
            //en février d'année bissextile 
            return 29;
        } else {
            return self::JOURS_MOIS[$this->mois()];
        }
    }
    
}
