<?php

/**
 * Implémenter une class MonTemps permettant de gérer des heures, minutes et secondes
 * Cette classe permettra d'instancier des objets pour stocker une heure précise 
 * et possèdera des méthodes pour incrémenter ou décrementer le temps selon un ordre de grandeur
 * par exemple, MonTemps->secondePrecedente() fera reculer le temps stocké d'une seconde
 * MonTemps->minuteSuivante() fera avancer le temps stocké d'une minute
 * Il faut aussi pouvoir changer directement n'importe quelle unité avec reglerSeconde ou reglerMinute ou reglerHeure
 * Egalement ajouter n'importe quelle unité avec ajouterSeconde, ajouterMinute ou ajouterHeure
 * Puis soustraire n'importe quelle unité avec soustraireSeconde, soustraireMinute ou soustraireHeure
 * L'objet possèdera également une méthode affichage() qui affichera l'heure complète au format hh:mm:ss
 *
 * Bonus : écrire une méthode lire() qui nous "lira" l'heure, au format "Il est 10 heures moins le quart" au lieu de il est 10:45
 * 
 * $temps = new MonTemps(10, 30, 12); 
 * $temps->affichage(); renverra 10:30:12
 * $temps->minuteSuivante();
 * $temps->affichage(); renverra 10:31:12
 * $temps->reglerMinutes(59);
 * $temps->affichage(); renverra 11:59:12
 */

class MonTemps
{
    private $_heure;
    private $_minute;
    private $_seconde;

    public function __construct($heure, $minute, $seconde)
    {
        $this->reglerHeure($heure);
        $this->reglerMinute($minute);
        $this->reglerSeconde($seconde);
    }

    /** accesseurs */
    public function heure()
    {
        return $this->_heure;
    }
    public function minute()
    {
        return $this->_minute;
    }
    public function seconde()
    {
        return $this->_seconde;
    }

    /** mutateurs */
    public function reglerHeure($heure)
    {
        if ($heure < 0 || $heure > 23){
            echo "heure invalide";
            $this->_heure = 0;
        } else {
            $this->_heure = $heure;
        }
    }
    public function reglerMinute($minute)
    {
        if ($minute < 0 || $minute > 59){
            echo "minute invalide";
            $this->_minute = 0;   
        } else {
            $this->_minute = $minute;
        }
    }
    public function reglerSeconde($seconde)
    {
        if ($seconde < 0 || $seconde > 59){
            echo "seconde invalide";
            $this->_seconde = 0;   
        } else {
            $this->_seconde = $seconde;
        }
    }


    /** incrémenteurs */
    public function heureSuivante(){
        $this->_heure++;
        if ($this->heure() > 23){
            $this->_heure = 0;
        }
    }

    public function minuteSuivante(){
        $this->_minute++;
        if ($this->minute() > 59){
            $this->_minute = 0;
            $this->heureSuivante();
        }
    }

    public function secondeSuivante(){
        $this->_seconde++;
        if ($this->seconde() > 59){
            $this->_seconde = 0;
            $this->minuteSuivante();
        }
    }

    /** décrementeur */
    public function heurePrecedente(){
        $this->_heure--;
        if ($this->heure() < 0){
            $this->_heure = 23;
        }
    }

    public function minutePrecedente(){
        $this->_minute--;
        if ($this->minute() < 0){
            $this->_minute = 59;
            $this->heurePrecedente();
        }
    }

    public function secondePrecedente(){
        $this->_seconde--;
        if ($this->seconde() < 0){
            $this->_seconde = 59;
            $this->minutePrecedente();
        }
    }

    public function affichage(){
        //dans sprintf utiliser %d permet d'insérer un nombre, %s une string etc
        //%02d veut dire qu'un chiffre 8 par exemple serait écrit 08
        return sprintf('%02d:%02d:%02d', $this->heure(), $this->minute(), $this->seconde());
    }

    //bonus inutile juste pour faire chier Fred en vrai, j'avoue
    public function lire(){
        // PAS FINIE PARCE QUE FLEMME MAIS PRINCIPE COMPRIS ok ? 
        $heure = $this->heure();
        $minute = $this->minute();
        
        //ce if doit forcément venir avant les heures, car moins le quart détermine quelle heure sera prononcée
        if ($this->minute() == 45){
            $affichageMinute = "moins le quart";
            $heure++;
        } 
        if ($this->minute() == 15) {
            $affichageMinute = "et quart";
        }
        if ($this->minute() == 30){
            $affichageMinute = "et demie";    
        }
        if ($heure != 12 && $heure != 0){
            $affichageHeure = sprintf("%02d heures", $heure);
        } else {
            if ($heure == 12){
                $affichageHeure = "midi";
            }
            if ($heure == 0){
                $affichageHeure = "minuit";
            }
        }
        
        return sprintf('Il est %s %s', $affichageHeure, $affichageMinute);
    }
}
