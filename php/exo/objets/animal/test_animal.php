<?php
require_once('animal.php');
require_once('chien.php');

$specimen = new Animal(Animal::MAMMALIA, 'Joe', 'Canis Lupus', 10, Animal::SEXE_M);
echo $specimen->description();

$rex = new Chien("rexe", 4, Animal::SEXE_F, "Berger Allemand", 150, "Noire");
echo $rex->description();