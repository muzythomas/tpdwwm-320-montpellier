<?php

/**
 * Créer une classe Animal qui prend en compte des caractéristiques propres à tous les animaux
 * Puis créer des classes fille d'animaux en leur attribuant des spécificités dues a leur espèce
 * ex : une classe Oiseau aura une envergure, alors que des mammifères auront une Robe etc
 */

class Animal
{
    //protected sert à rendre un attribut privé pour tout le monde
    //SAUF les classes héritant de la classe courante 
    //donc une classe héritant de Animal pourra utiliser un attribut protected
    protected $_nom;
    protected $_nomLatin;
    protected $_age;
    protected $_sexe;
    protected $_classe; 

    const SEXE_M = "Mâle";
    const SEXE_F = "Femelle";

    const MAMMALIA = "Mammifère";
    const AVES = "Oiseau";
    const REPTILIA = "Reptile";

    public function __construct($classe, $nom, $nomLatin, $age, $sexe)
    {
        $this->_classe = $classe;
        $this->_nom = $nom;
        $this->_nomLatin = $nomLatin;
        $this->_age = $age;
        $this->_sexe = $sexe;
    }

    public function description()
    {
        return "{$this->_nom} : {$this->_classe}, {$this->_nomLatin}, {$this->_sexe}, {$this->_age} mois";
    }
}
