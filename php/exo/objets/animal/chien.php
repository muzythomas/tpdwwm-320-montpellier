<?php
require_once('animal.php');

class Chien extends Animal{

    protected $_race;
    protected $_taille;
    protected $_robe;

    public function __construct($nom, $age, $sexe, $race, $taille, $robe){
        //ici on appelle le constructeur parent, pour pouvoir renseigner les 
        //attributs propre à la classe Animal
        parent::__construct(Animal::MAMMALIA, $nom, 'CANIS LUPUS', $age, $sexe);
        $this->_race = $race;
        $this->_taille = $taille;
        $this->_robe = $robe;
    }

    public function description(){
        if ($this->_sexe == Animal::SEXE_F){
            $message = "une chienne";
        } else {
            $message = "un chien";
        }
        return "$this->_nom, est $message de $this->_age mois, de $this->_taille cm et arbore une robe $this->_robe.";
    }
}