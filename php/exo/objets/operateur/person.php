<?php

//abstract signifie que notre classe est Abstraite
//une classe abstraite ne sera jamais instanciée (on ne peut pas faire new Person par exemple)
//et n'est là que pour qu'on en hérite, pour pouvoir utiliser ses propriétés/méthodes etc
abstract class Person {
    //protected est comme private, excepté que les classes héritantes peuvent les utiliser
    protected $_id;
    protected $_name;
    protected $_dob;
    protected $_address;

    public function __construct($id, $name, $dob, $address)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_dob = $dob;
        $this->_address = $address;
    }

    public function id(){
        return $this->_id;
    }

    public function name(){
        return $this->_name;
    }

    //une fonction abstraite permet de signaler aux classes héritant de celle ci 
    //qu'une fonction toString par exemple doit être implémentée
    //étant donné que la classe abstraite ne sera jamais instanciée
    //une fonction abstraite n'a pas besoin de corps
    abstract public function __toString();
}