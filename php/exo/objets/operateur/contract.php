<?php

class Contract {
    private $_id;
    private $_client;
    private $_salesman;
    private $_phoneNumber;
    private $_pricePerMonth;

    public function __construct($id, $client, $salesman, $phoneNumber, $pricePerMonth)
    {
        $this->_id = $id;
        $this->_client = $client;
        $this->_salesman = $salesman;
        $this->_phoneNumber = $phoneNumber;
        $this->_pricePerMonth = $pricePerMonth;
    }

    public function __toString(){
        //si on utilise sprintf on peut régler la précision des nombres flottants
        //en définissant %.nf, n étant le montant de chiffres après la virgule désiré
        return sprintf('%08d, Client : %s, Vendeur : %s, %s, Prix/mois: %.2f€', $this->_id, $this->_client->name(), $this->_salesman->name(), $this->_phoneNumber, $this->pricePerMonth());
    }

    public function pricePerMonth(){
        //pour calculer l'intervalle de temps entre la date d'inscription et la date du jour
        //on récupère la date du jour en instanciant un nouvel objet DateTime
        $now = new DateTime();
        //puis on utilise la méthode diff de DateTime pour effectuer la différence
        //entre la date d'aujourd'hui et la date d'inscription 
        $diff = $now->diff($this->_client->signUpDate());
        //le résultat est un objet de type DateTimeInterval de PHP
        //qui contient le nombre de jours, mois et années entre les deux dates
        //on a plus qu'à récupérer l'année en demandant à notre DateTimeInterval de formater
        //le résultat sous la forme d'années, avec format('%y')
        $years = $diff->format('%y');
        if ($years >= 5){
            //si le nombre d'années d'ancienneté est au moins de 5
            //on applique une réduction de 10%
            return $this->_pricePerMonth * 0.9;
        } else {
            return $this->_pricePerMonth;
        }
    }
}