<?php
require_once('person.php');
class Client extends Person{
    private $_signUpDate;
    
    public function __construct($id, $name, $dob, $address, $signUpDate)
    {
        parent::__construct($id, $name, $dob, $address);
        $this->_signUpDate = $signUpDate;
    }

    public function __toString()
    {
        return sprintf('%08d, %s, %s, %s, %s', $this->_id, $this->_name, $this->_dob->format('Y/m/d'), $this->_address, $this->_signUpDate->format('Y/m/d'));
    }

    public function signUpDate(){
        return $this->_signUpDate;
    }
}