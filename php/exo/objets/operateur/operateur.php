<?php
/**
 * Implémenter les classes présentes dans le schema fourni 
 * Le schéma représente l'organisation des données d'un opérateur téléphonique
 * Person est une classe Abstraite (n'étant pas vouée à être instanciée) dont vont hériter deux classes : 
 * - une classe Client, avec une sign_up_date (date d'inscription)
 * - une classe Salesman, avec un point_of_sale (point de vente, une string)
 * 
 * La classe abstraite Person quant à elle contient les informations suivantes :
 * - id
 * - name
 * - dob (date de naissance)
 * - address
 * 
 * Une autre classe est nécessaire au fonctionnement et c'est celle des contrats 
 * La classe Contract possède donc les attributs suivants :
 * - id
 * - client de type Client
 * - salesman de type Salesman
 * - phone_number
 * - price_per_month
 * 
 * Le prix par mois du contrat sera diminué de 10% si l'ancienneté du Client est d'au moins 5 ans
 */

require_once('client.php');
require_once('salesman.php');
require_once('contract.php');

$pigeon = new Client(1, "Jean Robert", new DateTime('1965/06/06'), "10 Rue de Jean Robert", new DateTime('2013/05/24'));
$patoche = new Salesman(1, "Patrick Balkany", new DateTime("1948/08/16"), "Parc de la blanchette, Levallois", "Levallois-Perret");

$contract = new Contract(666, $pigeon, $patoche, "0618596130", 340.78);

echo $pigeon;
echo $patoche;

echo $contract;