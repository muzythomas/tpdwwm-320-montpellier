<?php
require_once('person.php');

class Salesman extends Person{
    private $_pointOfSale;
    
    public function __construct($id, $name, $dob, $address, $pointOfSale)
    {
        parent::__construct($id, $name, $dob, $address);
        $this->_pointOfSale = $pointOfSale;
    }

    public function __toString()
    {
        return sprintf('%08d, %s, %s, %s, %s', $this->_id, $this->_name, $this->_dob->format('Y/m/d'), $this->_address, $this->_pointOfSale);
    }
}