<?php

/**
 * Créer une classe ClientBanque possédant comme attributs :  
 * - un ID (nombre entier)
 * - un nom 
 * - un genre (f ou m)
 * et comme méthodes : 
 * - __toString() qui affiche id, nom et genre du client
 * 
 * Le reste de l'exercice se trouve dans le fichier compte.php 
 */

class ClientBanque
{
    private $_id;
    private $_name;
    private $_genre;

    const GENRE_F = "f";
    const GENRE_M = "m";

    public function __construct($id, $name, $genre)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_genre = $genre;
    }

    public function id(){
        return $this->_id;
    }

    public function name(){
        return $this->_name;
    }

    public function genre(){
        return $this->_genre;
    }

    //__toString() est une méthode "magique" appelée au moment ou on tente de convertir un objet en string, par exemple en appelant l'objet dans une chaine de caractère de cette façons
    // "Le client $client", dans lequel $client est un objet, sans méthode __toString() cela renverrait une erreur
    public function __toString(){
        return sprintf("%d, %s, %s<br>", $this->id(), $this->name(), $this->genre());
    }
}
