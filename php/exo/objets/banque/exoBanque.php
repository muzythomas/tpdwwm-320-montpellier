<?php
require_once('client.php');
require_once('compte.php');

$client = new ClientBanque(1, "Jean", ClientBanque::GENRE_M);

echo $client;

$compte = new CompteBancaire(9933, $client, 0);
echo $compte;
$compte->depot(1000);
echo $compte;
$compte->retrait(450);
echo $compte;
$compte->retrait(600);