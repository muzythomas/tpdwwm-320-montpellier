<?php

/**
 * Implémenter une classe CompteBancaire possédant comme attributs :
 * - un id de compte
 * - un client de classe ClientBanque
 * - un solde 
 * et comme méthodes: 
 * - __toString() qui affiche le nom du client, l'id du compte, et le solde
 * - depot($montant) pour déposer de l'argent sur le compte
 * - retrait($montant) pour retirer de l'argent sur le compte
 */

class CompteBancaire
{
    private $_id;
    private $_client;
    private $_solde;

    public function __construct($id, $client, $solde)
    {
        $this->_id = $id;
        $this->_client = $client;
        $this->_solde = $solde;
    }

    public function id(){
        return $this->_id;
    }
    public function client(){
        return $this->_client;
    }
    public function solde(){
        return $this->_solde;
    }

    public function __toString(){
        //pour atteindre le nom du client
        //il faut d'abord récupérer le client contenu dans compte ( $this->client())
        //puis récupérer le nom du client dans client (client()->name())
        //on peut le faire en une fois en disant $this->client()->name()
        return sprintf('%d, %s, %d<br>', $this->id(), $this->client()->name(), $this->solde());
    }

    public function depot($somme){
        $this->_solde += $somme;
    }

    public function retrait($somme){
        if ($this->solde() >= $somme){
            $this->_solde -= $somme;
        } else {
            echo "Fonds insuffisants";
        }
    }
}
