<?php
if (isset($_POST['hauteur']) && isset($_POST['rayon'])){
    $hauteur = $_POST['hauteur'];
    $rayon = $_POST['rayon'];
    //on vérifie qu'on ait bien des valeurs numériques pour travailler
    if ( is_numeric($hauteur) && is_numeric($rayon)){
        //pour savoir si une checkbox a été cochée ou pas
        //il suffit de vérifier la présence de la clé associée dans $_POST avec isset
        //le résultat de isset (1 ou 0) nous dit si la checkbox a été cochée
        $arrondi = isset($_POST['arrondi']);
    
        $volume = (pi() * pow($rayon, 2) * $hauteur)/3;
    
        //si notre checkbox a été cochée
        if ($arrondi){
            //on envoie un résultat arrondi
            echo round($volume);
        } else {
            echo $volume;
        }
    } else {
        echo "Veuillez entrer des nombres valides";
    }

} else {
    echo "Erreur survenue veuillez réessayer";
}