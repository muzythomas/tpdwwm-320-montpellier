<?php 
if (isset($_POST['num1']) && isset($_POST['operation']) && isset($_POST['num2'])) {
    $num1 = $_POST['num1'];
    $num2 = $_POST['num2'];
    $operation = $_POST['operation'];
    
    //is_numeric permet de vérifier qu'une valeur soit bien d'un format numérique
    //elle nous permet ici de vérifier qu'on ne reçoive pas de chaine vide, ou du texte
    if ( is_numeric($num1) && is_numeric($num2)){

        if ($operation === "add") {
            echo $num1 + $num2;
        }
        if ($operation === "sous") {
            echo $num1 - $num2;
        }
        if ($operation === "mul") {
            echo $num1 * $num2;
        }
        if ($operation === "div") {
            if ($num2 != 0){
                echo $num1 / $num2;
            } else {
                echo "Division par zéro impossible.";
            }
        }
    } else {
        echo "Merci d'entrer des nombres valides";
    }
    
} else {
    echo "Formulaire invalide, veuillez réessayer";
}