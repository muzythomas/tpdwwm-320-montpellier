<!-- 
produire un formulaire contenant deux champs,
un champ permettant d'indiquer le nombre dont on veut obtenir la table de multiplication
une liste déroulante permettant de selectionner si on veut 10, 25, 50 ou 100 lignes de table 
!-->
<form method="POST" action="">
    <input type="number" name="num">
    <select name="lignes" id="">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
    </select>

    <input type="submit">
</form>

<?php

if (isset($_POST['num']) && isset($_POST["lignes"])){
    $num = $_POST['num'];
    $lignes = $_POST['lignes'];

    if (is_numeric($num) && is_numeric($lignes)){
        for ($i=1; $i <= $lignes; $i++){
            echo $num . "x" . $i . "=" . $num*$i . "<br>";
        }
    } else {
        echo "Veuillez entrer un nombre valide";
    }
} else {
    echo "Erreur formulaire";
}