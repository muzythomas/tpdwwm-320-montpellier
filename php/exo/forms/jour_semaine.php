<form action="" method="post">
    <input name="date" type="date">
    <input type="submit">
</form>
<?php
//ecrire un formulaire récupérant une date 
//a partir de cette date, 
//déterminer en PHP quel jour de la semaine lui correspond
if (isset($_POST['date'])){
    $date = $_POST['date'];
    //on récupère année mois et jour dans la date envoyée
    //on demande à strtotime de convertir notre date en une timestamp
    //puis on demande à date() de récupérer l'information demandée a partir de la timestamp
    $annee = date('y', strtotime($date));
    $mois = date('m', strtotime($date));
    $jour = date('d', strtotime($date));

    //formule récupérée de https://fr.wikibooks.org/wiki/Curiosit%C3%A9s_math%C3%A9matiques/Trouver_le_jour_de_la_semaine_avec_une_date_donn%C3%A9e
    if ($mois >= 3){
        $res = ((23*$mois/9) + $jour + 4 + $annee + ($annee/4) - ($annee/100) + ($annee/400) - 2) % 7;
    } else {
        $res = ((23*$mois/9) + $jour + 4 + $annee + (($annee-1)/4) - (($annee-1)/100) + (($annee-1)/400)) % 7;
    }
    
    //on fait correspondre nos jours avec l'index retrouvé
    $jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    echo $jours[$res];
}