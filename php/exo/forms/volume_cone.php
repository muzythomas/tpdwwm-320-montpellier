<!-- 
Produire un formulaire permettant de récupérer le rayon d'une base et la hauteur d'un cone
et d'en calculer le volume
Ajouter l'option d'arrondir (ou pas) le résultat à l'aide d'une checkbox  

volume d'un cone : 
(pi x rayon² x hauteur)/3
ou en php: 
(pi() * pow(rayon, 2) * hauteur)/3
!-->

<form method="POST" action="volume_cone_traitement.php">
    
    <label for="rayon">Rayon de la base</label>
    <input id="rayon" name="rayon" type="number">
    <label for="hauteur">Hauteur du cone</label>
    <input id="hauteur" name="hauteur" type="number">

    <label for="arrondi">Arrondir ?</label>
    <input id="arrondi" name="arrondi" type="checkbox">

    <input type="submit" value="Calculer Volume">
</form>