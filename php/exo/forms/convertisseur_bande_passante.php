<form method="POST" action="">
    <label>
        Valeur à convertir
        <input name="num" type="number">
    </label>

    <select name="unit1">
        <option value="b/s">b/s</option>
        <option value="kb/s">kb/s</option>
        <option value="Mb/s">Mb/s</option>
        <option value="Gb/s">Gb/s</option>
    </select>
    <select name="unit2">
        <option value="b/s">b/s</option>
        <option value="kb/s">kb/s</option>
        <option value="Mb/s">Mb/s</option>
        <option value="Gb/s">Gb/s</option>
    </select>
    <input type="submit">
</form>

<?php
//implémenter un convertisseur de bande passante
//les ordres de grandeurs pris en compte doivent être
// b/s, kb/s, Mb/s, Gb/s, o/s, ko/s, Mo/s, Go/s
//on doit pouvoir convertir n'importe quel ordre de grandeur en 
//n'importe quel autre

if (isset($_POST['num']) && isset($_POST['unit1']) && isset($_POST['unit2'])){
    $num = $_POST['num'];
    $unit1 = $_POST['unit1'];
    $unit2 = $_POST['unit2'];

    //on prépare nos ratios de conversion en reference a notre unité de base : b/s
    $ratios = ["b/s" => 1, "kb/s" => 1/1e3, "Mb/s" => 1/1e6, "Gb/s" => 1/1e9];
    //on effectue ensuite le calcul en faisant le rapport entre nos ordres de grandeur
    echo $num * ($ratios[$unit2]/$ratios[$unit1]);
}