<?php
//cette classe nous servira à stocker les données venant de la BDD 
class Comment{
    public int $id;
    public string $content;
    public int $createdOn;
    public BlogUser $author;
    public Article $article;

    public function __construct(int $id, string $content, int $createdOn, BlogUser $author, Article $article){
        $this->id = $id;
        $this->content = $content;
        $this->createdOn = $createdOn;
        $this->author = $author;
        $this->article = $article;
    }
}