<?php
//cette classe nous servira à stocker les données venant de la BDD 
class BlogUser{
    public $id;
    public $username;
    public $password;
    public $createdOn;

    public function __construct(int $id, string $username, string $password, int $createdOn){
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->createdOn = $createdOn;
    }

    public function __toString(){
        return $this->username;
    }
}