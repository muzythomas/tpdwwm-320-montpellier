<?php
//cette classe nous servira à stocker les données venant de la BDD 
class Article{
    public int $id;
    public string $title;
    public string $content;
    public int $createdOn;
    public BlogUser $author;

    public function __construct(int $id, string $title, string $content, int $createdOn, BlogUser $author){
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->createdOn = $createdOn;
        $this->author = $author;
    }
}