<?php
//on récupère nos Modèles de données 
require_once('model/user.php');
require_once('model/article.php');
require_once('model/comment.php');

//parce que j'en ai marre d'écrire mon utilisateur et mdp non stop
function getBlogDatabaseHandler(){
    return new DatabaseHandler('blog_concombre', 'blog_concombre', 'DIcMVQKPWNhvdS3h');
}

//Notre DatabaseHandler agit comme un ORM (Objet Relational Mapping) c'est à dire un dispositif qui permet de convertir des données d'une BDD relationnelle en des Objets manipulables plus facilement dans PHP
//Il nous sert à rendre notre code plus lisible en ayant des méthodes permettant le dialogue avec SQL et en renvoyant des objets instanciés à partir de notre Modèle (nos classes Article, BlogUser, Comment)
class DatabaseHandler{
    private string $_dbname;
    private string $_username;
    private string $_password;
    //handler contiendra l'objet PDO qui permet les transactions avec la bdd
    private PDO $_handler; 

    public function __construct(string $dbname, string $username, string $password){
        $this->_dbname = $dbname;
        $this->_username = $username;
        $this->_password = $password;
        //pour nous connecter automatiquement à chaque instanciation de notre surcouche DatabaseHandler
        //on appelle directement connect()
        $this->connect();
    }


    //connect permet de créer une instance PDO permettant de dialoguer avec MySQL et stocke cet objet PDO dans notre propriété _handler
    public function connect(){
        try{
            $dbh = new PDO("mysql:host=localhost;charset=utf8;dbname={$this->_dbname}", $this->_username, $this->_password, [PDO::ATTR_EMULATE_PREPARES => false]);
        } catch (PDOException $e){
            //pour spécifier un code d'erreur lors d'un chargement de page on peut utiliser http_response_code($erreur)
            http_response_code(500);
            die("500 - Internal Server Error");
        }
        $this->_handler = $dbh;
    }

    //GET ALL
    public function getAllFromTable(string $table){
        $stmt = $this->_handler->prepare("SELECT * FROM {$table}");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_OBJ);    
    }

    public function getArticles(){
        $res = $this->getAllFromTable('articles');
        $articles = [];
        //pour chaque article récupéré on instancie un objet de type Article
        foreach($res as $article){
            $author = $this->getUser($article->author);
            array_push($articles, new Article($article->id, $article->title, $article->content, $article->created_on, $author));
        }
        return $articles;
    }

    public function getUsers(){
        return $this->getAllFromTable('users');
    }

    public function getComments(){
        return $this->getAllFromTable('comments');

    }

    //GET BY ID
    public function getById(string $table, int $id){
        $stmt = $this->_handler->prepare("SELECT * FROM {$table} WHERE {$table}.id = :id");
        $stmt->execute(
            [
                ":id" => $id
            ]
        );
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Renvoie une instance de Article si un article est trouvé
     **/ 
    public function getArticle(int $id){
        $res = $this->getById('articles', $id);
        if ($res){
            $author = $this->getUser($res->author);
            return new Article($res->id, $res->title, $res->content, $res->created_on, $author);
        } else {
            return null;
        }
    }

    /**
     * Renvoie une instance de BlogUser si un utilisateur est trouvé
     * Sinon renvoie null
     * @param int $id L'id de l'utilisateur
     */
    public function getUser(int $id){
        $res = $this->getById('users', $id);
        if ($res){

            //on renvoie une instance de notre classe BlogUser
            return new BlogUser($res->id, $res->username, $res->password, $res->created_on);
        } else {
            return null;
        }
    }

    public function getComment(int $id){
        return $this->getById('comments', $id);
    }

    public function getCommentsFromArticle(int $articleId){
        $stmt = $this->_handler->prepare("SELECT * FROM comments WHERE comments.article = :articleId");
        $stmt->execute(
            [
                ":articleId" => $articleId
            ]
        );
        $res = $stmt->fetchAll(PDO::FETCH_OBJ);
        //on prépare un tableua d'objets Comment
        $comments = [];
        //pour chaque ligne de résultat renvoyée par SQL
        foreach($res as $comment){
            //on récupère l'article et l'auteur sous forme de Article et BlogUser
            $article = $this->getArticle($articleId);
            $author = $this->getUser($comment->author);
            //on instancie un Comment avec les infos récupérées de SQL
            $comment = new Comment($comment->id, $comment->content, $comment->created_on, $author, $article);
            //on ajoute le Comment a notre tableau
            array_push($comments, $comment);
        }
        
        //on renvoie notre tableau de commentaires
        return $comments;
    }

    //pour récuperer un utilisateur selon son username, lors de la verif de connexion par exemple
    public function getUserByUsername(string $username){
        $stmt = $this->_handler->prepare("SELECT * FROM users WHERE users.username = :username");
        $stmt->execute(
            [
                ":username" => $username
            ]
        );
        return $stmt->fetch(PDO::FETCH_OBJ);
    }

    //CREATE
    public function createArticle(string $title, string $content, int $author, int $created_on){
        $stmt = $this->_handler->prepare("INSERT INTO articles (title, content, author, created_on) VALUES (:title, :content, :author, :created_on)");
        $stmt->execute([
            ":title" => $title,
            ":content" => $content,
            ":author" => $author,
            ":created_on" => $created_on,
        ]);
    }

    public function createUser(string $username, string $password, int $created_on){
        $stmt = $this->_handler->prepare("INSERT INTO users (username, password, created_on) VALUES (:username, :password, :created_on)");
        $stmt->execute([
            ":username" => $username,
            ":password" => $password,
            ":created_on" => $created_on,
        ]);
    }

    public function createComment(string $content, int $created_on,  BlogUser $author, Article $article){
        $stmt = $this->_handler->prepare("INSERT INTO comments ( content, author, created_on, article) VALUES ( :content, :author, :created_on, :article)");
        $stmt->execute([
            ":content" => $content,
            ":author" => $author->id, //ici SQL nécessite un ID lors du insert into
            ":created_on" => $created_on,
            ":article" => $article->id, //ici SQL nécessite un ID lors du insert into
        ]);
    }
}