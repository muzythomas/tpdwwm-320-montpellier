<?php
session_start();
require_once('../db/db.php');

if (isset($_POST['username']) && isset($_POST['password']) ){
        $username = $_POST['username'];
        $password = $_POST['password'];

        //on récupère un utilisateur selon son username, pour pouvoir vérifier son mot de passe
        $user = getBlogDatabaseHandler()->getUserByUsername($username);
        if ($user){
            //si un utilisateur existe avec cet username
            //on vérifie que le mot de passe entré corresponde au hash dans la base
            if (password_verify($password, $user->password)){
                //on stocke l'id de l'utilisateur pour pouvoir l'utiliser dans notre site
                //et pour pouvoir vérifier qu'un utilisateur soit bien connecté plus tard
                $_SESSION['user_id']  = $user->id;
                //on redirige l'utilisateur à l'accueil
                header('Location: articles.php');
            } else {
                header('Location: signin.php');
            }
        } else {
            header('Location: signin.php');
        }
        
        //puis on redirige à l'accueil
        header('Location: articles.php');
} else {
    die('Formulaire invalide');
}