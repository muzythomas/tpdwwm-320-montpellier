<?php
//ICI toutes les fonctions d'affichage de HTML pour mon blog

function displayHead(string $title)
{
    return
        sprintf("
    <!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>%s - Blog du Concombre</title>
</head>
<body>
    <header>
        <h1>Mon Super Blog</h1>
    </header>
    ", $title);
}

//pour faire un paramètre par défaut, qui rendrait le paramètre possiblement optionnel
//on peut directement le déclarer a n'importe quelle valeur
function displayNav($user = null){
    //selon si un utilisateur est connecté ou pas
    //les actions de notre menu vont changer
    //donc on définit selon si l'utilisateur est réçu ce qui va s'afficher
    if ($user){
        $suiteNav = "<li><a href='signout.php'>Se déconnecter</a> </li>";
        $message = "<h2>Bonjour {$user->username} ! </h2>";
    } else {
        $suiteNav = "<li><a href='signup.php'>S'inscrire</a> </li>
                    <li><a href='signin.php'>Se connecter</a> </li>";
        $message = "";
    }
    //on définit ensuite les parties du menu qui sont obligatoires
    return  
    sprintf("
    <nav>
        <ul>
        <li><a href='articles.php'>Accueil</a> </li>
        <li><a href='article_post.php'>Ecrire</a> </li>
        %s
    </nav>
    %s
    ", $suiteNav, $message); //et on recolle les morceaux à l'aide de sprintf derrière
}

function displayFooter(){
    return "
    </body>
    </html>
    ";
}

function displayArticle($article){
    return sprintf("
    <article>
        <div class='article-head'>
            <a href='article.php?id={$article->id}'><h2>%s</h2></a>
            <span class='article-author'>Par : %s </span>
            <span class='article-date'>Ecrit le: %s</span> 
        </div>
        <div class='article-body'>
            %s
        </div>
    </article>
", $article->title, $article->author, date('d/m/Y H:i:s', $article->createdOn), $article->content);
}

function displayComment($comment){
    return sprintf("
    <div class='comment'>
        <span class='comment-author'>Par : %s</span>
        <span class='comment-date'>Le: %s</span>
        <p>
        %s
        </p>
    </div>
    ", $comment->author, date('d/m/Y H:i:s', $comment->createdOn), $comment->content);
}