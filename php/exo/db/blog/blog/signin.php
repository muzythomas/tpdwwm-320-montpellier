<?php
require_once('display.php');

echo displayHead('Connexion');
echo displayNav();
?>

<form method="POST" action="signin_process.php">
    <label>
        Nom d'utilisateur
        <input type="text" name="username">
    </label>

    <label>
        Mot de passe
        <input type="password" name="password">
    </label>
    <input type="submit">
</form>
<span>
    Pas de compte ? <a href="signup.php">Inscrivez vous</a> !
</span>
<?php
echo displayFooter();