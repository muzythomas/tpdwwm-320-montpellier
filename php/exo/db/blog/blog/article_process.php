<?php
session_start();
require_once('../db/db.php');
//on vérifie que l'utilisateur soit connecté pour écrire un article
if (!isset($_SESSION['user_id'])){
    //si un utilisateur n'est pas connecté
    //on redirige vers la page de connexion
    header('Location: signin.php');
} else {
    $userid = $_SESSION['user_id'];
}

//on vérifie l'intégrité de nos données de formulaire
if (isset($_POST['title']) && isset($_POST['content'])){
    $title = $_POST['title'];
    $content = $_POST['content'];
    
    //on appele notre méthode createArticle dans notre surcouche DatabaseHandler
    //on lui fait passer les informations du formulaire ainsi que de l'utilisateur (auteur), et la timestamp d'envoi (time()) 
    //getBlogDatabaseHandler nous renvoie un objet DatabaseHandler, on peut donc appeler directement createArticle dessus comme si c'était un objet dans une variable
    getBlogDatabaseHandler()->createArticle($title, $content, $userid, time());
    header('Location: articles.php');
}