<?php
require_once('display.php');

echo displayHead('Inscription');
echo displayNav();
?>

<form method="POST" action="signup_process.php">
    <label>
        Nom d'utilisateur
        <input type="text" name="username">
    </label>

    <label>
        Mot de passe
        <input type="password" name="password">
    </label>
    <label>
        Répéter Mot de passe
        <input type="password" name="repeat_password">
    </label>
    <input type="submit">
</form>

<?php
echo displayFooter();