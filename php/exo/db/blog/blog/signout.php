<?php
//Déconnexion
//on récupère la session
session_start();
//on la supprime
session_destroy();
//on vide $_SESSION par sécurité
$_SESSION = [];
//et on redirige vers l'accueil
header('Location: articles.php');