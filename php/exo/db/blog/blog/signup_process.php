<?php
require_once('../db/db.php');

if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['repeat_password'])){
    if ($_POST['password'] == $_POST['repeat_password']){
        $username = $_POST['username'];
        $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

        //on appelle createUser dans notre surcouche DatabaseHandler
        getBlogDatabaseHandler()->createUser($username, $password, time());
        //puis on redirige à l'accueil
        header('Location: articles.php');
    } else {
        die('Mots de passes ne correspondent pas');
    }
} else {
    die('Formulaire invalide');
}