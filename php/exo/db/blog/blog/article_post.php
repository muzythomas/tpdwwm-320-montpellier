<?php
session_start();
require_once('display.php');
require_once('../db/db.php');

//on vérifie que l'utilisateur soit connecté pour écrire un article
if (!isset($_SESSION['user_id'])){
    //si un utilisateur n'est pas connecté
    //on redirige vers la page de connexion
    header('Location: signin.php');
} else {
    $user = getBlogDatabaseHandler()->getUser($_SESSION['user_id']);
}


echo displayHead("Ecrire");
//on affiche notre Nav en fonction de si un utilisateur est connecté ou pas
if (isset($user)){
    echo displayNav($user);
} else {
    echo displayNav();
}
?>
<form method="POST" action="article_process.php">
    <label>
        Titre de l'article
        <input placeholder="Mon Article" type="text" name="title">
    </label>

    <label>
        Corps de l'article
        <textarea placeholder="Votre article" name="content"></textarea>
    </label>
    <input type="submit">
</form>
<?php
echo displayFooter();