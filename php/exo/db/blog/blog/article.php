<?php
session_start();
//.. dans un chemin de fichier signifie "le dossier parent"
require_once('../db/db.php');
require_once('display.php');

//nous renvoie notre DatabaseHandler créé dans notre fichier db.php
$dbh = getBlogDatabaseHandler();
//on vérifie qu'un utilisateur soit connecté
if (isset($_SESSION['user_id'])){
    //si un utilisateur est connecté, on récupère ses informations
    $user = $dbh->getUser($_SESSION['user_id']);
}

echo displayHead('Accueil');

//on affiche notre Nav en fonction de si un utilisateur est connecté ou pas
if (isset($user)){
    echo displayNav($user);
} else {
    echo displayNav();
}
//On récupère le paramètre GET id, contenant l'id de l'article à consulter
if (isset($_GET['id'])){
    //on tente de récupérer l'article dans notre bdd
    $articleId = $_GET['id'];
    $article = $dbh->getArticle($articleId);
}
//si l'article a été trouvé
if ($article){
    //on l'affiche
    echo displayArticle($article);
} else {
    //sinon, on envoie un message d'erreur 404
    http_response_code(404);
    die('Article non trouvé');
}

//si un utilisateur est connecté
if (isset($user)){
    //afficher un formulaire de commentaires
    ?>
        <form method="POST" action="">
            <label>
                Entrez votre réaction, <?=$user?> !
                <input type="text" name="content">
                <input type="submit">
            </label>
        </form>
    <?php

    //si un formulaire de commentaire a été envoyé
    if (isset($_POST['content'])){
        $content = $_POST['content'];
        $dbh->createComment($content, time(), $user, $article);
        header('Location: article.php?id='.$article->id);
    }
}

//affichage des commentaires de l'article
$comments = $dbh->getCommentsFromArticle($article->id);
foreach($comments as $comment){
    echo displayComment($comment);
}
echo displayFooter();
?>