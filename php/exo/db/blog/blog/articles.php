<?php
session_start();
//.. dans un chemin de fichier signifie "le dossier parent"
require_once('../db/db.php');
require_once('display.php');

//nous renvoie notre DatabaseHandler créé dans notre fichier db.php
$dbh = getBlogDatabaseHandler();
//on vérifie qu'un utilisateur soit connecté
if (isset($_SESSION['user_id'])){
    //si un utilisateur est connecté, on récupère ses informations
    $user = $dbh->getUser($_SESSION['user_id']);
}

echo displayHead('Accueil');

//on affiche notre Nav en fonction de si un utilisateur est connecté ou pas
if (isset($user)){
    echo displayNav($user);
} else {
    echo displayNav();
}

//on affiche ensuite les articles
$articles = $dbh->getArticles(); //on les récupère
foreach ($articles as $article) {
    echo displayArticle($article);
    echo "<hr>";
}

echo displayFooter();
?>