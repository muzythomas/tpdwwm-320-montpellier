<?php
session_start();
$_SESSION = []
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Apparts</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
</head>
<body>
    <form action="appart.php?page=1" method="POST">
        <select name="orderby">
            <option value="city">city</option>
            <option value="address">address</option>
            <option value="zip">zip</option>
            <option value="surface">surface</option>
            <option value="rooms">rooms</option>
            <option value="rent">rent</option>
        </select>

        <label>
            Loyer Mini
            <input min="0" type="number" name="rent_min">
        </label>
        <label>
            Loyer Maxi
            <input min="0" type="number" name="rent_max">
        </label>

        <label>
            Surface Mini
            <input min="0" type="number" name="surface_min">
        </label>
        <label>
            Surface Maxi
            <input min="0" type="number" name="surface_max">
        </label>

        Pièces : 
        <label>1<input type="checkbox" name="room_1"></label>
        <label>2<input type="checkbox" name="room_2"></label>
        <label>3<input type="checkbox" name="room_3"></label>
        <label>4<input type="checkbox" name="room_4"></label>
        <label>5<input type="checkbox" name="room_5"></label>
        <label>6<input type="checkbox" name="room_6"></label>
        <label>7<input type="checkbox" name="room_7"></label>
        <label>8<input type="checkbox" name="room_8"></label>
        <label>9<input type="checkbox" name="room_9"></label>
        <label>10<input type="checkbox" name="room_10"></label>

        <label>
            Ville
            <input type="text" name="city">
        </label>
        <label>
            Code Postal
            <input type="text" name="zip">
        </label>

        <input type="submit">
    </form>
</body>
</html>