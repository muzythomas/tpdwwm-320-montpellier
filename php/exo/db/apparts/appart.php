<?php

/**
 * Dans un premier lieu, récupérer et afficher tous les apparts contenus dans la table appart fournie
 * 
 * Ensuite, une fois que vous maitriserez la récupération de données depuis la base avec PDO, créer un formulaire dans appart.html, avec un menu déroulant proposant de trier les résultats en fonction d'une colonne en particulier. 
 * Les colonnes de tri sont :
 * La ville (city)
 * L'adresse (address)
 * Le code postal (zip)
 * La surface (surface)
 * Le nombre de pièces (rooms)
 * Le loyer (rent)
 * Donc juste un menu déroulant, avec un bouton envoyer, qui affichera le résultat selon la colonne selectionnée dans le menu déroulant. 
 * 
 * Etape suivante :
 * Rajouter des champs permettant de filtrer les appartements selon le loyer (loyer mini, loyer maxi)
 * puis,
 * Selon la surface (surface mini, surface maxi)
 * puis le nombre de pièces, 
 * puis enfin par ville, addresse et code postal. 
 * 
 * Rajouter ensuite un système de pagination, avec une possibilité de choisir le nombre de lignes à afficher par page
 */
//on commence notre session pour pouvoir utiliser $_SESSION pour garder nos paramètres de page en page
session_start();

//gestion de la pagination
if (isset($_GET['page'])){
    $page = $_GET['page'];
} else {
    $page = 1;
}
$limit = 25;
//on calcule le décalage des résultats SQL a l'aide du numéro de la page et de la limite de résultats
$offset = ($page - 1) * $limit;

/** RECUPERATION DES VALEURS DE FORMULAIRES */
//pour s'assurer qu'on essaye pas d'effectuer une injection SQL via orderby
//on vérifie que la valeur envoyée par formulaire est bien un nom de colonne
$columns = ['id', 'city', 'address', 'zip', 'surface', 'rooms', 'rent'];
//in_array permet de vérifier si ce qu'on reçoit par POST est bien unn nom de colonne
if (isset($_POST['orderby']) && in_array($_POST['orderby'], $columns)) {
    $orderby = $_POST['orderby'];
} else {
    $orderby = "id";
}
//on stocke/récupère des paramètres de requêtes supplémentaires dans la session
if (isset($_SESSION['orderby'])){
    $orderby = $_SESSION['orderby'];
} else {
    $_SESSION['orderby'] = $orderby;
}

if (isset($_POST['rent_min']) && isset($_POST['rent_max']) && isset($_POST['surface_min']) && isset($_POST['surface_max'])){
    $rentMin = $_POST['rent_min'];
    $rentMax = $_POST['rent_max'];
    $surfaceMin = $_POST['surface_min'];
    $surfaceMax = $_POST['surface_max'];

    //si aucune valeur n'a été fournie pour le loyer minimum, on lui donne une valeur par défaut de 0
    if (empty($rentMin)){
        $rentMin = 0;
    }
    if (empty($surfaceMin)){
        $surfaceMin = 0;
    }

    //si aucune valeur n'a été fournie pour le loyer maximum, on lui donne une valeur par défaut de PHP_INT_MAX
    //PHP_INT_MAX est une constante contenant le nombre entier maximal sur notre version de PHP
    //cela permet d'être sûr qu'aucune valeur ne peut être supérieure à notre maximum par défaut
    //l'idéal serait dans une application plus poussée d'obtenir le loyer/la surface max dans notre bdd
    if (empty($rentMax)){
        $rentMax = PHP_INT_MAX;
    }
    if (empty($surfaceMax)){
        $surfaceMax = PHP_INT_MAX;
    }
}

//on récupère les cases cochées de nombre de pièces
//on prépare un tableau vide
$rooms = [];
//on compte de 1 à 10, 10 étant le maximum de pièces selectionnables sur notre formulaire
for ($i = 1; $i <= 10; $i++){
    //et on vérifie si la checkbox correspondant à l'index à bien été cochée dans notre formulaire
    if (isset($_POST['room_'.$i])){
        //si c'est le cas, on ajoute le nombre de pièce correspondant à notre tableau
        array_push($rooms, $i);
    }
}
if (empty($rooms)){
    $rooms = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
}
//on stocke/récupère des paramètres de requêtes supplémentaires dans la session
if (isset($_SESSION['rooms'])){
    $rooms = $_SESSION['rooms'];
} else {
    $_SESSION['rooms'] = $rooms;
}


if (isset($_POST['city'])){
    //pour pouvoir utiliser SQL LIKE 
    //on utilise des wildcards (jokers) attachés à notre valeur de formulaire
    //pour que LIKE puisse trouver les enregistrements avec une ville contenant la valeur du formulaire
    $city = "%".$_POST['city']."%";
}

if (isset($_POST['zip'])){
    $zip = $_POST['zip'];
    //Si notre code postal n'est pas renseigné
    //on instancie une variable qui nous permettra de contourner le test de code postal dans notre requête SQL
    if (empty($zip)){
        //la requête deviendra ainsi WHERE zip = "" OR 1, OR 1 voulant dire "ou vrai", ce qui valide la condition à tous les coups, annulant ainsi le filtre sur le code postal
        $zipVide = "OR 1";
    } else {
        $zipVide = "";
    }
} else {
    $zipVide= "OR 1";
}

/** CONNEXION A LA BASE DE DONNEES, ET TRANSACTION VIA SQL */
//PDO::ATTR_EMULATE_PREPARES => false permet de ne pas faire semblant de préparer les requêtes
//et donc d'avoir de véritable requêtes préparées
$dbh = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'password', [PDO::ATTR_EMULATE_PREPARES => false]);

//préparation de la requête SQL 
//on peut se permettre de concaténer notre  $orderby directement dans notre requête UNIQUEMENT après s'être assurés qu'il n'est pas autre chose qu'un nom de colonne
//pour tout autre paramètre qui ne serait pas ORDER BY, on peut utiliser le système de paramètres de prepare et execute
//Pour reconstituer un tableau valide en SQL on ne peut pas se conteter de passer un tableau à PDO
//PDO ne gère par les tableaux et collections dans les paramètres d'execute
//et on doit donc convertir notre tableau en une chaine de caractère représentant un tableau SQL valide : (a, b, c) par exemple
//Pour ça, on va utiliser la fonction join() qui permet de coller ensemble les éléments d'un même tableau, en y intercalant un ou plusieurs caractères choisis
$stmt = $dbh->prepare("SELECT * FROM appart WHERE (rent BETWEEN :rent_min AND :rent_max) AND (surface BETWEEN :surface_min AND :surface_max) AND (rooms IN (". join(',', $rooms) .")) AND city LIKE :city AND (zip = :zip $zipVide) ORDER BY $orderby LIMIT :limit OFFSET :offset");
//les parenthèses autour du test du code postal sont indispensables pour ne pas que notre bricolage de OR 1 risque de contourner toute la requête
//execution de la requête SQL

//Ici on enregistre les paramètres de notre requête dans une variable de session
//pour ne pas les perdre à chaque page précedente/suivante
//On vérifie d'abord si les paramètres existent
if (isset($_SESSION['params'])){
    //si les paramètres ont déjà été enregistrés on les récupère
    $queryParams = $_SESSION['params'];
    //et on modifie juste l'offset pour pouvoir afficher les résultats suivants
    $queryParams[':offset'] = $offset;
} else {
    //sinon, on enregistre nos paramètres de requête dans notre session
    $queryParams = [
        ":rent_min" => $rentMin,
        ":rent_max" => $rentMax,
        ":surface_min" => $surfaceMin,
        ":surface_max" => $surfaceMax,
        ":city" => $city,
        ":zip" => $zip,
        ":limit" => $limit,
        ":offset" => $offset
    ];
    $_SESSION['params'] = $queryParams;
}
//on execute comme d'habitude notre requête avec nos paramètres, qu'ils viennent de la session ou pas
$stmt->execute(
    $queryParams
);
//récupération du résultat sous la forme de tableau d'objets
$apparts = $stmt->fetchAll(PDO::FETCH_OBJ);

/** AFFICHAGE DES RESULTATS */
foreach ($apparts as $appart) {
    echo sprintf('Address: %s, Zip: %s, City: %s, Surface: %d, Rooms: %d, Rent: %d <hr>', $appart->address, $appart->zip, $appart->city, $appart->surface, $appart->rooms, $appart->rent);
}

//on affiche les liens pages précédentes ou suivantes
if ($page > 1){
    echo sprintf("<a href='appart.php?page=%d'>page précédente</a>", $page-1);
}
echo sprintf("<a href='appart.php?page=%d'>page suivante</a>", $page+1);