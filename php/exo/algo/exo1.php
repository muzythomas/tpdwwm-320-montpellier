<form method="POST" action="">
    <label>
        Nb de valeurs à génerer
        <input type="number" name="nbvaleurs">
    </label>

    <label>
        Borne minimum
        <input name="min" type="number">
    </label>
    <label> 
        Borne maximum
        <input name="max" type="number">
    </label>

    <input type="submit" value="Go">

</form>

<?php
//Génerer un tableau avec des valeurs aléatoires 
//le nombre de valeurs générées précisées par l'utilisateur
//ainsi que les bornes minimum et maximum de la génération du nombre
//afficher ensuite les valeurs du tableau de la façon suivante
// [n0, n1, ..., nn] (tous les nombres entre crochets séparés de virgules)
if (isset($_POST['nbvaleurs']) && isset($_POST['min']) && isset($_POST['max'])){
    $nbvaleurs = $_POST['nbvaleurs'];
    $min = $_POST['min'];
    $max = $_POST['max'];

    $valeurs = []; //création d'un tableau vide

    if (is_numeric($nbvaleurs) && is_numeric($min) && is_numeric($max)){
        //génération du tableau contenant les valeurs aléatoires
        echo "[";
        for ($i=0; $i < $nbvaleurs; $i++){
            //array_push ajoute la valeur aléatoire a notre tableau $valeurs
            array_push($valeurs, rand($min, $max));
            //on affiche la valeur générée
            echo $valeurs[$i];
            //on empêche une virgule d'être ajoutée après la dernière valeur
            if ($i != $nbvaleurs - 1){
                echo ",";
            }
        }
        echo "]";

    }
}