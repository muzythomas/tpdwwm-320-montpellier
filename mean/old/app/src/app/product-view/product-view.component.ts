import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../models/Product';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  constructor(private ps: ProductService, private route: ActivatedRoute, private location: Location) { }

  product: Product;
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.ps.getById(params.id).subscribe(
        data => this.product = data
      );
    })
  }

  goBack(){
    this.location.back();
  }
}
