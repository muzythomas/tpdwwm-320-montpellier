import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../models/Product';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private ps: ProductService) { }

  public products: Product[];
  ngOnInit(): void {
    this.ps.get().subscribe(data => this.products = data)
  }

}
