import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './models/Product';
@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private url : string= "http://localhost:4000/product";
  constructor(private http: HttpClient) { }

  public get():Observable<Product[]>{
    return this.http.get<Product[]>(this.url);
  }

  public getById(id: string): Observable<Product>{
    return this.http.get<Product>(`${this.url}/${id}`);
  }
}
