# PROJET API STACK MEAN

## Stack MEAN ?

**MEAN** signifie **M**ongoDB **E**xpress **A**ngular et **N**ode, et décrit l'ensemble des technologies utilisées pour mettre en place une application "fullstack". 

### MongoDB 

[MongoDB](https://www.mongodb.com/fr) est une base de données [NoSQL](https://fr.wikipedia.org/wiki/NoSQL) utilisée souvent dans des projets node.js et très populaire dans le monde du développement Web. Elle est surtout utilisée dans des applications où le volume de données traitées est très important. 

Le gain de performance est possible grâce à la façon de MongoDB de gérer les données : non pas de façon relationnelle mais orientée documents. 

### Express 

[Express](https://expressjs.com/fr/) est un framework node permettant de mettre en place un routeur dans une application web de façon simplifiée. 

### Angular

Framework front-end de Google permettant de travailler en MVC.

### Node.js 

[Node.js](https://nodejs.org/en/) est une plateforme de développement javascript facilitant la mise en place d'applications back-end et de gestion de modules applicatifs. Elle est accompagnée du gestionnaire de paquet [npm](https://www.npmjs.com/) (node package manager).


## Mise en place du projet 

Pour initialiser un projet node, il faut utiliser la commande 

```
npm init -y
```

Qui génère un fichier `package.json` à la racine de votre projet. 

Le fichier `package.json` contient des informations sommaires (auteur, nom etc) concernant le projet en vue d'un potentiel partage ou réutilisation (sur npm par exemple).
Il a également pour but de contenir des informations plus techniques comme les dépendences d'un projet, les commandes de script utilisables dans le projet, etc. 

### Installation d'un serveur auto-reload
Pour éviter d'avoir à relancer le serveur à la main à chaque changement dans notre projet 
On peut installer de façon globale (sur notre système) un utilitaire permettant de lancer un script node avec une fonction d'auto-rechargement. 

``` 
npm install -g nodemon
```

### Installation des dépendances 

### MongoDB: 

Pour mettre en place notre api, il nous faut utiliser mongoDB, il faut donc l'installer à partir du [site officiel](https://www.mongodb.com/try/download/community).

Après installation, `mongod.exe` (le service contenant le serveur de base de données) devrait se lancer automatiquement. 

### Sur Windows : 
Sur les systèmes Windows, il faut ensuite ajouter le chemin du client `mongo.exe` aux variables d'environnement. 

Pour accéder aux variables d'environnement, utiliser la combinaison `Windows + R` puis taper `SystemPropertiesAdvanced.exe`.

Ensuite, cliquez sur `Variables D'Environnement`, puis dans `Variables Système`, selectionner `Path` et cliquer sur `Modifier`. 

Ensuite cliquer sur `Parcourir`, et naviguer jusqu'à `C:/Programmes/MongoDB/4.4/Server/bin` et cliquer sur ok. 

Puis validez pour quitter le panneau de gestion de variables d'environnements. 

Désormais, en tapant `mongo` dans un invite de commande, un client Mongodb devrait s'exécuter. 

### Dépendances supplémentaires

Pour notre api, on aura également besoin d'un moyen de lire les corps de requêtes HTTP, un moyen de gérer efficacement les headers CORS, ainsi qu'un ORM  - ou dans ce cas là ODM - pour dialoguer efficacement avec notre base de données.

On aura également besoin d'un routeur (express).

On peut installer tout ça en une commande : 
``` 
npm install express cors body-parser mongoose --save
```

### .gitignore
Si votre projet est suivi sous git, il est recommandé d'exclure le dossier `node_modules` contenant les extension de notre projet à l'aide d'un fichier `.gitignore`.
En créant un fichier nommé `.gitignore` à la racine de notre projet et en y écrivant 

```
node_modules
```

git ignorera ce dossier et tout son contenu. 

## Développement du serveur 
Notre serveur basé sur express nécessite très peu de configuration avant d'être lancé : 


```js
//server.js
const express = require('express');
const app = express(); //on instancie Express
const port = 4000; //on définit un port d'écoute pour notre serveur 
const server = app.listen(port, () => { console.log('serveur lancé sur le port ' + port) })
```

en lançant la commande 
```
node server
```
ou
```
nodemon server
```
si `nodemon` est installé. 

on recevait donc dans la console :

```
serveur lancé sur le port 4000
```

Cependant, notre serveur n'est pas programmé pour faire quoi que ce soit, et en essayant d'accéder à `localhost:4000` le navigateur nous renvoie `cannot GET /`. 

Il faut donc paramétrer des routes pour Express

### Créer un fichier de routes pour donner à Express 

On crée donc un fichier visant à contenir des routes pour paramétrer notre serveur. 

```js
//helloworld.route.js
const express = require('express');
const app = express();
//pour définir des routes dans express on va appeler le composant Router de express
const router = express.Router();

//pour déclarer une route on utilise la méthode route du router
//on définit une route selon un chemin 
//puis une méthode
//puis la fonction qui s'exécutera lorsque la route aura reçu une requête 
//dans la fonction à exécuter, on reçoit deux paramètres : la requête HTTP et la réponse HTTP
router.route("/").get( (req, res) => {
    //on utilise l'objet réponse (res ici) pour envoyer nos données au format json
    res.json({message: 'Hello, world !'});
})

//on export le module pour qu'on puisse l'utiliser dans toute l'application
module.exports = router;
```

Une fois notre fichier de routes créé, on doit demander à Express dans notre `server.js` d'utiliser lesdites routes : 

```js
const express = require('express');

//on importe notre module de routes
const helloRoutes = require('./routes/helloworld.route');

const app = express();
const port = 4000;

//on indique à notre app express d'utiliser nos routes
app.use('/', helloRoutes);

const server = app.listen(port, () => { console.log('serveur lancé sur le port ' + port) })
```

En relançant 
```
node server
```
on peut ensuite accéder à `localhost:4000` et recevoir au format json : 
```json
{"message":"Hello, world !"}
```


## Connexion à la base de données MongoDB à l'aide de Mongoose

Pour entamer une connexion à MongoDB via Mongoose il faut d'abord importer Mongoose, puis effectuer une requête de connexion sur une collection MongoDB grâce à `mongoose.connect()`.


```js
//on importe notre ODM mongoose
const mongoose = require('mongoose');
//on lance la connexion à la bdd via mongoose
//on dicte les instructions à exécuter en cas de succès ou d'erreur 
// les options useNewUrlParser et useUnifiedTopology mise à true permettent d'éviter des erreurs de deprecation
mongoose.connect('mongodb://localhost:27017/ecommerce', { useNewUrlParser: true, useUnifiedTopology: true }).then(
    //si tout se passe bien 
    () => console.log('db connected'),
    //en cas d'erreur
    err => console.error(`db error ${err}`)
);
```


## Lier des routes Express à des actions Mongoose 

On avait déjà paramétré une route à l'aide d'express dans un fichier à part, et fourni cette route à notre serveur dans le `server.js`. 
Pour coder notre API, il faut désormais implémenter des actions en base de données qui se déclencheront lors d'une requête sur une de ces routes. 

### Paramétrer un Modèle de données Mongoose 

Pour pouvoir effectuer des actions sur une ressource, il faut d'abord paramétrer un modèle de données à l'aide de Mongoose. 

Pour une ressource Product, on crée un fichier `Product.js` qui servira à préciser dans quelle collection MongoDB la ressource sera enregistrée, ainsi que indiquer quelles seront ses propriétés. 

```js
//Product.js
const mongoose = require('mongoose');
//on récupère le composant Schema de mongoose pour générer un schema de données 
const Schema = mongoose.Schema; 

//on définit désormais notre schema pour notre collection product
const product = new Schema(
    {
        //on définit d'abord nos propriétés
        name: {
            type: String
        }, 
        description: {
            type: String
        },
        price: {
            type: Number
        }
    }, {
        //on définit dans quelle collection on stockera ce schéma
        collection: 'product'
    }
);

//on exporte ensuite le modele généré par mongoose pour utilisation dans notre serveur
//mongoose.model récupère le schema ainsi que le futur nom du modèle pour créer le modèle de données
module.exports = mongoose.model('product', product);
```

Notre modèle de Product ainsi exporté nous servira à effectuer des actions en base dans nos callbacks de routes. 

### Actions en base de données dans les routes Express

Pour manipuler notre ressource Product, on peut créer un fichier de routes `product.route.js`. 

```js
// sont définies nos routes concernant la manipulation de nos entités product
//on récupère express et son router
const express = require('express');
const router = express.Router();

//on récupère également le model product pour pouvoir faire des requêtes mongodb
let Product = require('../models/Product');

//on définit ensuite nos routes
//la première permettant de renvoyer tous nos products en cas de récéption d'une requête GET sur notre serveur 
router.route('/').get((req, res) => {
    //une fois la requête reçue on cherche tous les product en bdd 
    //et on les renvoie au format json
    //Product.find() permet d'aller chercher toutes les représentations de product
    //et renvoie un objet contenant de potentielles erreurs
    //ainsi qu'un objet contenant de potentiels résultats
    //Product.find() attends comme paramètre une fonction de callback qui définira quoi faire au retour des résultats
    Product.find( (err, products) => {
        //en cas d'erreur
        if (err) {
            res.status(500).json({ message: `Error retrieving products : ${err}`});
        }
        //si on a bien reçu les produits
        else {
            //on renvoie une réponse http contenant les produits au format json
            res.json(products);
        }
    });
});
```

Ici, notre route permettant de récupérer notre collection de products entière est très simple : on se contente de matcher sur des requêtes HTTP GET et de renvoyer le résultat de `Product.find()`, qui est la requête de mongoose vers la base de données.

Pour la création de ressources : 
```js
//on crée une route permettant l'ajout de products
//la route est donc paramétrée pour HTTP POST
router.route("/").post((req, res) => {
    //on récupère le contenu de la requête pour créer notre product
    //req.body renvoie le corps de la requête contenant la représentation json du product à ajouter
    //new Product() permet de créer un nouvel objet selon le model Product de mongoose
    const product = new Product(req.body);
    //on enregistre ensuite le product nouvellement créé dans la bdd
    //save() renvoie un objet Promise, et save().then() permet de gérer le cas où save() a réussi 
    product.save()
    .then(
        //on définit quoi faire en cas de réussite
        product => res.json(product)
    )
    //si une erreur arrive on le gère dans le .catch() de la promise
    .catch(
        err => res.json({message: `Unable to save product to database : ${err}`})
    );
});
```

Pour pouvoir lire `req.body` au format json, il faut que Express soit équipé du module `body-parser`, ainsi dans notre `server.js` il faut qu'on demande à notre application express d'utiliser ledit module : 

```js
//server.js
//on charge bodyParser qui permettra la lecture des corps de requête HTTP
const bodyParser = require('body-parser');
//on indique à express d'utiliser bodyParser pour interpreter le json dans les requêtes
app.use(bodyParser.json());
```

Lorsqu'on utilise `object.save()` de mongoose, la méthode renvoie une `Promise`, il faut donc traiter le résultat à l'aide des méthodes `then()` (qui traite le résultat normal) et `catch()` (qui traite les erreurs).


## Gérer les entêtes CORS
En utilisant le module [cors](https://www.npmjs.com/package/cors) on peut paramétrer les entêtes CORS de nos réponses.

On se contente ici de charger le module `cors` et le passer à express à l'aide de `use()`: 
```js
//on charge le module cors qui permettra les requêtes Cross Origin
const cors = require('cors');
//on charge le module cors dans express
app.use(cors())); //les paramètres de l'en tête peuvent être changés ici lors de l'appel de cors()
```

