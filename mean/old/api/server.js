//pour mettre en place notre serveur et le faire écouter sur un port
//il faut utiliser express
//Pour importer un module Node, on utilise require
//Ici, on charge le module express contenu dans node_modules dans une constante nommée express
const express = require("express");
//on charge bodyParser qui permettra la lecture des corps de requête HTTP
const bodyParser = require("body-parser");
//on charge le module cors qui permettra les requêtes Cross Origin
const cors = require("cors");
//on importe nos routes
const helloRoutes = require("./routes/helloworld.route");
const productRoutes = require("./routes/product.route");
const categoryRoutes = require("./routes/category.route");
//on importe notre ODM mongoose
const mongoose = require("mongoose");
//on lance la connexion à la bdd via mongoose
//on dicte les instructions à exécuter en cas de succès ou d'erreur
// les options useNewUrlParser et useUnifiedTopology mise à true permettent d'éviter des erreurs de deprecation
mongoose
    .connect("mongodb://localhost:27017/ecommerce", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    })
    .then(
        //si tout se passe bien
        () => console.log("db connected"),
        //en cas d'erreur
        (err) => console.error(`db error ${err}`)
    );

//on appelle ensuite express pour générer notre serveur
const app = express();
const port = 4000; //on définit un port d'écoute pour notre serveur

//on indique à express d'utiliser bodyParser pour interpreter le json dans les requêtes
app.use(bodyParser.json());
//on indique à express d'utiliser le module cors
app.use(cors());
//on indique à notre app express d'utiliser nos routes
app.use("/", helloRoutes);
app.use("/product", productRoutes);
app.use("/category", categoryRoutes);

//on demande ensuite au serveur d'écouter sur le port défini
const server = app.listen(port, () => {
    console.log("serveur lancé sur le port " + port);
});
