const mongoose = require('mongoose');
//on récupère le composant Schema de mongoose pour générer un schema de données 
const Schema = mongoose.Schema; 

//on définit désormais notre schema pour notre collection product
const product = new Schema(
    {
        //on définit d'abord nos propriétés
        name: {
            type: String
        }, 
        description: {
            type: String
        },
        price: {
            type: Number
        },
        category : {
            type: String
        }
    }, {
        //on définit dans quelle collection on stockera ce schéma
        collection: 'product'
    }
);

//on exporte ensuite le modele généré par mongoose pour utilisation dans notre serveur
module.exports = mongoose.model('product', product);