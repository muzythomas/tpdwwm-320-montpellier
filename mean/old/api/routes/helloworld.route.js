//ce fichier sert à définir des routes qui pourront être importées dans Express pour notre serveur
//pour cela, il faut donc récupérer une instance de express
const express = require('express');
const app = express();
//pour définir des routes dans express on va appeler le composant Router de express
const router = express.Router();


//pour déclarer une route on utilise la méthode route du router
//on définit une route selon un chemin 
//puis une méthode
//puis la fonction qui s'exécutera lorsque la route aura reçu une requête 
//dans la fonction à exécuter, on reçoit deux paramètres : la requête HTTP et la réponse HTTP
router.route("/").get( (req, res) => {
    //on utilise l'objet réponse (res ici) pour envoyer nos données au format json
    res.json({message: 'Hello, ddd !'});
})

//pour rendre disponible notre module de routes aux autres parties de l'application, 
//il faut l'exporter
//module.exports permet de rendre disponible des données dans une application node
module.exports = router;