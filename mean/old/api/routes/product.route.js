// sont définies nos routes concernant la manipulation de nos entités product
//on récupère express et son router
const express = require("express");
const router = express.Router();

//on récupère également le model product pour pouvoir faire des requêtes mongodb
let Product = require("../models/Product");
let Category = require("../models/Category");

//on définit ensuite nos routes
//la première permettant de renvoyer tous nos products en cas de récéption d'une requête GET sur notre serveur
router.route("/").get((req, res) => {
    //une fois la requête reçue on cherche tous les product en bdd
    //et on les renvoie au format json
    //Product.find() permet d'aller chercher toutes les représentations de product
    //et renvoie un objet contenant de potentielles erreurs
    //ainsi qu'un objet contenant de potentiels résultats
    //Product.find() attends comme paramètre une fonction de callback qui définira quoi faire au retour des résultats
    Product.find((err, products) => {
        //en cas d'erreur
        if (err) {
            res.status(500).json({
                message: `Error retrieving products : ${err}`,
            });
        }
        //si on a bien reçu les produits
        else {
            //on renvoie une réponse http contenant les produits au format json
            res.json(products);
        }
    });
});

//on crée une route permettant l'ajout de products
//la route est donc paramétrée pour HTTP POST
router.route("/").post((req, res) => {
    //on récupère le contenu de la requête pour créer notre product
    //req.body renvoie le corps de la requête contenant la représentation json du product à ajouter
    //new Product() permet de créer un nouvel objet selon le model Product de mongoose
    const product = new Product(req.body);
    //on enregistre ensuite le product nouvellement créé dans la bdd
    //save() renvoie un objet Promise, et save().then() permet de gérer le cas où save() a réussi
    product
        .save()
        .then(
            //on définit quoi faire en cas de réussite
            (product) => {
                //on va chercher la catégorie spécifiée dans la requête
                //et y ranger notre product
                //on cherche la catégorie via son nom qu'on récupère dans le corps de la requête
                Category.findOne( {name: product.category }, (err, category) => {
                    if (category){
                        //on range notre product dans le tableau de products de la catégorie trouvée
                        category.products.push(product);
                        category.save();
                        res.json(product)
                    }
                })
            }
        )
        //si une erreur arrive on le gère dans le .catch() de la promise
        .catch((err) =>
            res.json({ message: `Unable to save product to database : ${err}` })
        );
});

//pour aller chercher un product en particulier, on peut utiliser un paramètre comme id par exemple
router.route("/:id").get((req, res) => {
    //pour récupérer le paramètre id de la requête on utilise req.params
    const id = req.params.id;
    //on cherche ensuite la ressource associée en bdd
    Product.findById(id, (err, product) => {
        //si une erreur survient
        if (err) {
            //on envoie un message d'erreur en console
            //ici on part du principe que l'erreur viendra du serveur
            //mais on devrait d'abord vérifier en temps normal au lieu de supposer
            //TODO : filtrer correctement le type d'erreur
            res.status(500).json({
                message: `Error retrieving product : ${err}`,
            });
        } else {
            //si on a récupéré un product
            if (product) {
                res.json(product);
            } else {
                res.status(404).json({ message: `Product ${id} not found` });
            }
        }
    });
});

router.route("/:id").delete((req, res) => {
    //on récupère le paramètre id dans la route
    const id = req.params.id;
    //on utilise ensuite findByIdAndDelete pour supprimer la ressource
    Product.findByIdAndDelete(id, (err, product) => {
        if (err) {
            res.status(500).json({
                message: `Error deleting product ${id} : ${err}`,
            });
        } else {
            if (product) {
                res.json({ message: `Product ${id} successfuly deleted` });
            } else {
                res.status(404).json({ message: `Product ${id} not found` });
            }
        }
    });
});

router.route("/:id").put((req, res) => {
    const id = req.params.id;
    Product.findByIdAndUpdate(id, req.body, {new: true} ,(err, product) => {
        if (err) {
            res.status(500).json({
                message: `Error updating product ${id} : ${err}`
            })
        } else {
            if (product){
                res.json(product)
            } else {
                res.status(404).json({message: `Product ${id} not found`});
            }
        }
    })
});

//on exporte ensuite nos routes
module.exports = router;
