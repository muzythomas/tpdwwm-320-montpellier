const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const slugify = require("slugify");

const pizza = new Schema(
    {
        name: { type: String },
        slug: { type: String },
        base: { type: Schema.Types.ObjectId, ref: "pizzaBase" },
        ingredients: [{ type: Schema.Types.ObjectId, ref: "ingredient" }],
    },
    {
        collection: "pizzas",
    }
);

pizza.pre("save", async function () {
    this.slug = slugify(this.name);
});

module.exports = mongoose.model("pizza", pizza);
