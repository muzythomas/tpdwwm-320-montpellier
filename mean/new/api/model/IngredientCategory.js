const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const slugify = require("slugify");

const ingredientCategory = new Schema(
    {
        name: { type: String },
        slug: { type: String },
        ingredients: [{ type: Schema.Types.ObjectId, ref: "ingredient" }],
    },
    { collection: "ingredientCategories" }
);

ingredientCategory.pre("save", async function () {
    this.slug = slugify(this.name);
});

module.exports = mongoose.model("ingredientCategory", ingredientCategory);
