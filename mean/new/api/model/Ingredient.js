const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const slugify = require("slugify");

const ingredient = new Schema(
    {
        name: { type: String },
        slug: { type: String },
        category: { type: Schema.Types.ObjectId, ref: "ingredientCategory" },
    },
    { collection: "ingredients" }
);

ingredient.pre("save", async function () {
    this.slug = slugify(this.name);
});

module.exports = mongoose.model("ingredient", ingredient);
