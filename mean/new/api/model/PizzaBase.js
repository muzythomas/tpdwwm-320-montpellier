const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const slugify = require("slugify");

const pizzaBase = new Schema(
    {
        name: { type: String },
        slug: { type: String },
    },
    { collection: "pizzaBases" }
);

//function() au lieu de () => parce que () => ne possède pas son propre this et conserve le contexte global
pizzaBase.pre("save", async function () {
    this.slug = slugify(this.name);
});

module.exports = mongoose.model("pizzaBase", pizzaBase);
