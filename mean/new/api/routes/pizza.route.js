const express = require("express");
const router = express.Router();

let Pizza = require("../model/pizza");
let Ingredient = require("../model/ingredient");

router.route("/").get((req, res) => {
    Pizza.find()
        //Populate nous permet de remplir notre tableau d'ingrédients avec plus d'informations que seulement l'objectId, il nous permet ici d'également récupérer le nom de chaque ingrédient pour qu'il soit accessible dans le tableau
        //Path définit l'endroit dans l'objet (ici Pizza) que l'on doit "populate" (remplir)
        //select définit les propriétés de l'objet à remplir (ici Ingrédient) que l'on veut récupérer
        .populate({
            path: "ingredients",
            //on populate un niveau de plus, pour remplir les catégories de chaque ingrédient
            populate: { path: "category", select: "name slug" },
            select: "-__v",
        })
        .populate({ path: "base", select: "name slug" })
        .then((pizze) => res.json(pizze))
        .catch((err) =>
            res.json({ message: `erreur en récupérant les pizze : ${err}` })
        );
});

router.route("/").post((req, res) => {
    const newPizza = new Pizza(req.body);
    newPizza
        .save()
        .then((pizza) => {
            //pour renvoyer en json l'objet déjà populate, on peut l'appeler après le save
            pizza
                .populate({
                    path: "ingredients",
                    //on populate un niveau de plus, pour remplir les catégories de chaque ingrédient
                    populate: { path: "category", select: "name slug" },
                    select: "-__v",
                })
                .populate({ path: "base", select: "name slug" }, () => {
                    //puis utiliser le callback pour envoyer la réponse désormais remplie
                    res.json(pizza);
                });
        })
        .catch((err) => {
            res.status(500).json({
                message: `impossible d'enregistrer la pizza : ${err}`,
            });
        });
});

router.route("/:id").get((req, res) => {
    const id = req.params.id;
    Pizza.findById(id)
        .populate({
            path: "ingredients",
            //on populate un niveau de plus, pour remplir les catégories de chaque ingrédient
            populate: { path: "category", select: "name slug" },
            select: "-__v",
        })
        .populate({ path: "base", select: "name slug" })
        .then((pizza) => {
            if (pizza) {
                res.json(pizza);
            } else {
                res.status(404).json({ message: `Pizza ${id} non trouvée` });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération pizza impossible : ${err}`,
            });
        });
});

router.route("/:id").delete((req, res) => {
    const id = req.params.id;
    //findbyanddelete trouve un objet en base et le supprime
    Pizza.findByIdAndDelete(id)
        .then((deleted) =>
            //on nous renvoie également l'objet qui a été supprimé
            res.json({ message: `Pizza ${deleted.id} supprimé` })
        )
        .catch((err) => {
            res.status(500).json({
                message: `Erreur suppression Pizza impossible : ${err}`,
            });
        });
});

router.route("/:id").put((req, res) => {
    const id = req.params.id;
    const pizzaJson = req.body;
    //finByIdAndUpdate trouve un objet et le met à jour selon ce qu'on lui passe en paramètre
    //le paramètre {new: true} est une option permettant de renvoyer non pas l'objet qui a été trouvé mais bien le résultat de la modification
    Pizza.findByIdAndUpdate(id, pizzaJson, {
        new: true,
        useFindAndModify: false,
    })
        .populate({
            path: "ingredients",
            //on populate un niveau de plus, pour remplir les catégories de chaque ingrédient
            populate: { path: "category", select: "name slug" },
            select: "-__v",
        })
        .populate({ path: "base", select: "name slug" })
        .then((pizza) => {
            if (pizza) {
                res.json(pizza);
            } else {
                res.status(404).json({
                    message: `Pizza ${id} non trouvée `,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur modification Pizza impossible : ${err}`,
            });
        });
});
module.exports = router;
