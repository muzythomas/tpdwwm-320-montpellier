const express = require("express");
const router = express.Router();

let PizzaBase = require("../model/pizzaBase");

router.route("/").get((req, res) => {
    PizzaBase.find()
        .then((pizzaBases) => res.json(pizzaBases))
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération des bases : ${err}`,
            });
        });
});

router.route("/").post((req, res) => {
    const pizzaBase = new PizzaBase(req.body);
    pizzaBase
        .save()
        .then((pizzaBase) => {
            res.json(pizzaBase);
        })
        .catch((err) => {
            res.status(500).json({
                message: `erreur enregistrement pizzaBase : ${err}`,
            });
        });
});

router.route("/:id").get((req, res) => {
    const id = req.params.id;
    PizzaBase.findById(id)
        .then((pizzaBase) => {
            if (pizzaBase) {
                res.json(pizzaBase);
            } else {
                //si on reçoit null c'est que l'objet n'est pas trouvé
                res.status(404).json({
                    message: `pizzaBase ${id} non trouvée`,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération base : ${err}`,
            });
        });
});

router.route("/:id").delete((req, res) => {
    const id = req.params.id;
    PizzaBase.findByIdAndDelete(id)
        .then((deleted) => {
            res.json({ message: `Base ${deleted.slug} supprimée` });
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur suppression base impossible : ${err}`,
            });
        });
});

router.route("/:id").put((req, res) => {
    const id = req.params.id;
    const body = req.body;
    PizzaBase.findByIdAndUpdate(id, body, { new: true })
        .then((pizzaBase) => {
            if (pizzaBase) {
                res.json(pizzaBase);
            } else {
                res.status(404).json({
                    message: `PizzaBase ${id} non trouvée `,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur modification base impossible : ${err}`,
            });
        });
});

module.exports = router;
