const express = require("express");
const router = express.Router();

let Ingredient = require("../model/ingredient");
let IngredientCategory = require("../model/ingredientCategory");

router.route("/").get((req, res) => {
    Ingredient.find()
        .populate({ path: "category", select: "name slug" })
        .then((ingredients) => res.json(ingredients))

        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération des ingredients : ${err}`,
            });
        });
});

router.route("/").post((req, res) => {
    //pour récupérer les données nécessaires à la création de notre ingredient
    //on doit lire le corps de la requête contenant le json
    //pour ça on a installé et utilisé body-parser au niveau de notre application express
    const ingredient = new Ingredient(req.body); //on passe le corps de la requête au constructeur du modele mongoose

    //on peut ensuite tenter de l'enregistrer en bdd
    ingredient
        .save()
        .then((ingredient) => {
            //ajout de l'ingrédient dans la collection d'ingrédients de la catégorie
            IngredientCategory.findOneAndUpdate(
                { _id: ingredient.category },
                { $push: { ingredients: ingredient } }
            );
            res.json(ingredient);
        })
        .catch((err) => {
            res.status(500).json({
                message: `erreur enregistrement ingredient : ${err}`,
            });
        });
});

router.route("/:id").get((req, res) => {
    const id = req.params.id;
    //findById permet de retrouver un objet par son objectId
    Ingredient.findById(id)
        .populate({ path: "category", select: "name slug" })
        .then((ingredient) => {
            //Dans le cas ou find s'exécute proprement, il faut tout de même vérifier si l'objectId correspond à un objet existant
            if (ingredient) {
                res.json(ingredient);
            } else {
                //si on reçoit null c'est que l'objet n'est pas trouvé
                res.status(404).json({
                    message: `Ingredient ${id} non trouvé`,
                });
            }
        })
        .catch((err) => {
            //catch s'exécutera si find ne se lance pas pour une raison x ou y (ex: objectId au format invalide)
            res.status(500).json({
                message: `Erreur récupération ingrédient : ${err}`,
            });
        });
});

router.route("/:id").delete((req, res) => {
    const id = req.params.id;
    //findbyanddelete trouve un objet en base et le supprime
    Ingredient.findByIdAndDelete(id)
        .then((deleted) =>
            //on nous renvoie également l'objet qui a été supprimé
            res.json({ message: `Ingredient ${deleted.id} supprimé` })
        )
        .catch((err) => {
            res.status(500).json({
                message: `Erreur suppression ingredient impossible : ${err}`,
            });
        });
});

router.route("/:id").put((req, res) => {
    const id = req.params.id;
    const body = req.body;
    //finByIdAndUpdate trouve un objet et le met à jour selon ce qu'on lui passe en paramètre
    //le paramètre {new: true} est une option permettant de renvoyer non pas l'objet qui a été trouvé mais bien le résultat de la modification
    Ingredient.findByIdAndUpdate(id, body, { new: true })
        .then((ingredient) => {
            if (ingredient) {
                res.json(ingredient);
            } else {
                res.status(404).json({
                    message: `Ingredient ${id} non trouvé `,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur modification ingredient impossible : ${err}`,
            });
        });
});

module.exports = router;
