const express = require("express");
const router = express.Router();

let IngredientCategory = require("../model/ingredientCategory");

router.route("/").get((req, res) => {
    IngredientCategory.find()
        .populate({ path: "ingredients", select: "name" })
        .then((ingredientCategory) => res.json(ingredientCategory))
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération des bases : ${err}`,
            });
        });
});

router.route("/").post((req, res) => {
    const ingredientCategory = new IngredientCategory(req.body);
    ingredientCategory
        .save()
        .then((ingredientCategory) => {
            res.json(ingredientCategory);
        })
        .catch((err) => {
            res.status(500).json({
                message: `erreur enregistrement ingredientCategory : ${err}`,
            });
        });
});

router.route("/:id").get((req, res) => {
    const id = req.params.id;
    IngredientCategory.findById(id)
        .populate({ path: "ingredients", select: "name" })
        .then((ingredientCategory) => {
            if (ingredientCategory) {
                res.json(ingredientCategory);
            } else {
                //si on reçoit null c'est que l'objet n'est pas trouvé
                res.status(404).json({
                    message: `ingredientCategory ${id} non trouvée`,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération ingredientCategory : ${err}`,
            });
        });
});

router.route("/:id").delete((req, res) => {
    const id = req.params.id;
    IngredientCategory.findByIdAndDelete(id)
        .then((deleted) => {
            res.json({ message: `Base ${deleted.slug} supprimée` });
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur suppression catégorie impossible : ${err}`,
            });
        });
});

router.route("/:id").put((req, res) => {
    const id = req.params.id;
    const body = req.body;
    IngredientCategory.findByIdAndUpdate(id, body, { new: true })
        .then((ingredientCategory) => {
            if (ingredientCategory) {
                res.json(ingredientCategory);
            } else {
                res.status(404).json({
                    message: `IngredientCategory ${id} non trouvée `,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur modification IngredientCategory impossible : ${err}`,
            });
        });
});

module.exports = router;
