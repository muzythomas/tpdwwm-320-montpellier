const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const port = 4000;

//on utilise mongoose pour se connecter a mongodb
mongoose.connect(
    //la connection a notre base de données test se fait en précisant /test dans notre url de connexion
    "mongodb://localhost:27017/pizzeria",
    //ces options sont recommandées par mongodb pour cause de deprecation
    { useNewUrlParser: true, useUnifiedTopology: true },
    //on définit notre message en cas de réussite
    () => console.log("connexion a la bdd reussie"),
    //et en cas d'erreur
    (err) => console.error(err)
);
app.use(bodyParser.json());
app.use(cors());

const pizzaRoute = require("./routes/pizza.route");
app.use("/pizza", pizzaRoute);
const ingredientRoute = require("./routes/ingredient.route");
app.use("/ingredient", ingredientRoute);

const pizzaBaseRoute = require("./routes/pizzaBase.route");
app.use("/pizzaBase", pizzaBaseRoute);

const ingredientCategoryRoute = require("./routes/ingredientCategory.route");
app.use("/ingredientCategory", ingredientCategoryRoute);

const server = app.listen(port, () => {
    console.log("Server listening on port " + port);
});
