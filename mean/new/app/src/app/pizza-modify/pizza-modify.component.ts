import {
    Component,
    EventEmitter,
    Input,
    OnInit,
    OnDestroy,
    Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable, of, Subscription } from 'rxjs';
import { Ingredient } from '../models/Ingredient';
import { Pizza } from '../models/Pizza';
import { PizzaBase } from '../models/PizzaBase';
import { IngredientService } from '../services/ingredient.service';
import { PizzaBaseService } from '../services/pizza-base.service';
import { PizzaService } from '../services/pizza.service';

@Component({
    selector: 'app-pizza-modify',
    templateUrl: './pizza-modify.component.html',
    styleUrls: [
        '../pizza-view/pizza-view.component.css',
        './pizza-modify.component.css',
    ],
})
export class PizzaModifyComponent implements OnInit, OnDestroy {
    //@Input permet de faire passer un paramètre lors du montage de notre component dans une autre page
    //cela nous permet d'appeler ce component avec <app-pizza-modify [id]="id"></app-pizza-modify>
    // ou <app-pizza-modify [pizza]="objetPizza"></app-pizza-modify>
    @Input() id?: string;
    @Input() pizza?: Pizza;

    //Ouput nous permet de faire passer une donnée au component branchant pizza-modify
    //un EventEmitter est un moyen d'envoyer un event auquel un component pourra réagir
    @Output() edited = new EventEmitter<Pizza>();

    public pizzaForm?: FormGroup;
    public ingredients: Ingredient[] = [];
    public formIngredients: Ingredient[] = [];
    public pizzaBases: PizzaBase[] = [];

    private ingredientSubscription?: Subscription;
    private pizzaSubscription?: Subscription;
    private pizzaBaseSubscription?: Subscription;

    constructor(
        private pizzaService: PizzaService,
        private ingredientService: IngredientService,
        private pizzaBaseService: PizzaBaseService,
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(): void {
        //si on a pas reçu l'objet en entier
        this.pizzaSubscription = this.getPizza().subscribe((pizza) => {
            this.pizza = pizza;
            this.pizzaForm = this.initForm(pizza);
        });

        this.ingredientSubscription = this.ingredientService
            .get()
            .subscribe((ingredients) => {
                this.ingredients = ingredients;
            });
        this.pizzaBaseSubscription = this.pizzaBaseService
            .get()
            .subscribe((pizzaBases: PizzaBase[]) => {
                this.pizzaBases = pizzaBases;
            });
    }

    ngOnDestroy(): void {
        if (this.ingredientSubscription) {
            this.ingredientSubscription.unsubscribe();
        }
        if (this.pizzaSubscription) {
            this.pizzaSubscription.unsubscribe();
        }
        if (this.pizzaBaseSubscription) {
            this.pizzaBaseSubscription.unsubscribe();
        }
    }

    getPizza(): Observable<Pizza> {
        if (!this.pizza) {
            if (this.id) {
                //on va le chercher via notre service grâce à son id
                return this.pizzaService.getById(this.id);
            }
        }
        return of(this.pizza!);
    }

    addIngredient(id: string) {
        const ingredient = this.ingredients.find(
            (i: Ingredient) => i._id === id
        );
        if (ingredient) {
            this.formIngredients.push(ingredient);
            console.log(this.formIngredients);
        }
    }

    removeIngredient(toRemove: Ingredient) {
        this.formIngredients = this.formIngredients.filter(
            (i) => i._id !== toRemove._id
        );
    }

    initForm(pizza: Pizza): FormGroup {
        this.formIngredients = pizza.ingredients;
        //on prépare notre formulaire à l'aide de notre formBuilder
        return this.formBuilder.group({
            //on précise chaque champ de notre formulaire
            name: [pizza.name],
            base: [pizza.base._id],
        });
    }

    edit(): void {
        if (this.pizzaForm) {
            //pour ajouter notre pizza on récupère les valeurs de formulaire
            const newPizza = this.pizzaForm.value;
            newPizza._id = this.pizza!._id;
            //ajout de chaque ingrédient dans notre pizza
            newPizza.ingredients = [];
            this.formIngredients.forEach((ingredient) => {
                newPizza.ingredients.push(ingredient._id);
            });

            this.pizzaService.edit(newPizza).subscribe((pizza) => {
                //on envoie notre evenement pour stopper le mode d'édition
                this.edited.emit(pizza);
            });
        }
    }
}
