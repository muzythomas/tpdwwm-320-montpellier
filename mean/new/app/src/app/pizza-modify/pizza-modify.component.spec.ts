import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaModifyComponent } from './pizza-modify.component';

describe('PizzaModifyComponent', () => {
  let component: PizzaModifyComponent;
  let fixture: ComponentFixture<PizzaModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
