import { Component, OnDestroy, OnInit } from '@angular/core';
import { Pizza } from '../models/Pizza';
import { PizzaService } from '../services/pizza.service';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-pizza',
    templateUrl: './pizza.component.html',
    styleUrls: ['./pizza.component.css'],
})
export class PizzaComponent implements OnInit, OnDestroy {
    constructor(private pizzaService: PizzaService) {}

    //on prépare un tableau liant une pizza et un mode d'édition
    public pizze: { pizza: Pizza; editMode: boolean }[] = [];
    public pizzeResults: { pizza: Pizza; editMode: boolean }[] = [];
    public createMode: boolean = false;

    public pizzaSearchControlSub?: Subscription;
    public pizzaSearchControl: FormControl = new FormControl(['']);

    private pizzaSubscription?: Subscription;
    ngOnInit(): void {
        this.pizzaSubscription = this.pizzaService
            .get()
            .subscribe((pizze: Pizza[]) => {
                this.pizze = pizze.map((pizza) => {
                    return { pizza: pizza, editMode: false };
                });
                this.pizzeResults = Array.from(this.pizze);
            });

        this.pizzaSearchControlSub = this.pizzaSearchControl.valueChanges
            //.pipe() permet de personnaliser notre observable
            //debounceTime() permet de préciser un temps avant l'envoi de la prochaine valeur par l'observable
            //ce qui veut dire ici qu'à chaque appui de touche on redémarre le compte de 300ms avant d'envoyer la donnée
            //distinctUntilChanged permet de s'assurer qu'une donnée ne soit pas envoyée si elle est identique à la dernière donnée envoyée
            .pipe(debounceTime(250), distinctUntilChanged())
            .subscribe((value) => {
                //a chaque fois qu'une valeur est envoyée, on fait notre recherche
                //grâce à debounceTime() et distinctUntilChanged() on s'assure de ne pas faire de recherche inutile
                this.search(value);
            });
    }

    inString(needle: string, haystack: string) {
        return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
    }

    search(searchTerm: string) {
        this.pizzeResults = this.pizze.filter((p) => {
            //si on trouve une correspondance via le nom de la pizza
            let found = this.inString(searchTerm, p.pizza.name);
            if (found) {
                //on renvoie true, qui affichera cette pizza dans les résultats
                return found;
            }

            //même chose dans le nom de la base
            found = this.inString(searchTerm, p.pizza.base.name);
            if (found) {
                //on renvoie true, qui affichera cette pizza dans les résultats
                return found;
            }

            //sinon on cherche une correspondance dans les noms des ingrédients
            //find() permet de retourner le premier élément d'un tableau qui valide un certain prédicat (une condition)
            if (
                p.pizza.ingredients.find((i) =>
                    this.inString(searchTerm, i.name)
                )
            ) {
                found = true;
            }

            //on renvoie si oui ou non une correspondance des ingrédients a été trouvée
            return found;
        });
    }

    //permet de rafraichir l'affichage des résultats en prenant compte si une recherche a déjà été effectuée
    refreshSearch() {
        if (this.pizzaSearchControl.touched) {
            this.search(this.pizzaSearchControl.value);
        } else {
            this.pizzeResults = this.pizze;
        }
    }

    //ngOnDestroy permet d'effectuer un traitement avant la destruction du component
    //il est utilisé pour faire un peu de "nettoyage" pour aider le "garbage collector" de javascript
    //ici, on unsubscribe des observables pour libérer la mémoire et ne pas avoir des observables "zombies"
    ngOnDestroy(): void {
        if (this.pizzaSubscription) {
            this.pizzaSubscription.unsubscribe();
        }

        if (this.pizzaSearchControlSub) {
            this.pizzaSearchControlSub.unsubscribe();
        }
    }

    //permet de réagir à l'appui de bouton dans notre boite de dialogue dialogComponent
    onDeleteConfirmation(confirmation: boolean, toDelete: Pizza) {
        if (confirmation) {
            this.pizzaService.delete(toDelete).subscribe((data) => {
                //on met à jour notre tableau de pizze avec un tableau dans lequel on ne garde pas la pizza à supprimer, ce qui a pour effet de supprimer la pizza du tableau
                this.pizze = this.pizze.filter(
                    (p) => p.pizza._id != toDelete._id
                );
                //on met à jour également nos résultats de recherche sur lesquels l'affichage est basé
                this.refreshSearch();
            });
        } else {
            this.pizzaToDelete = undefined;
        }
    }

    public pizzaToDelete?: Pizza;
    delete(toDelete: Pizza) {
        //en indiquant quelle pizza on compte supprimer, on peut afficher la boite de dialogue correspondante
        this.pizzaToDelete = toDelete;
    }

    toggleEdit(pizza: any) {
        //on inverse le booléen gérant le mode d'édition de la pizza
        pizza.editMode = !pizza.editMode;
    }

    onEdit(editedPizza: Pizza, index: number) {
        this.pizze.splice(index, 1, { pizza: editedPizza, editMode: false });
        this.refreshSearch();
    }

    onNewPizza(newPizza: Pizza) {
        this.pizze.push({ pizza: newPizza, editMode: false });
        this.refreshSearch();
    }

    toggleCreateMode() {
        this.createMode = !this.createMode;
    }
}
