import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../models/Ingredient';
import { IngredientService } from '../services/ingredient.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css'],
})
export class IngredientComponent implements OnInit {
  //on injecte notre service dans notre component
  constructor(private ingredientService: IngredientService) {}

  //on prépare un tableau dans lequel stocker nos ingrédients
  public ingredients: Ingredient[] = [];
  ngOnInit(): void {
    //ngOnInit s'exécute à l'initialisation du component
    //on souscrit au résultat de notre méthode get du service
    this.ingredientService.get().subscribe((ingredients) => {
      //une fois le résultat reçu on le range dans notre propriété
      this.ingredients = ingredients;
    });
  }

  //pour ajouter un nouvel ingrédient on a besoin de récupérer son nom
  add(name: string) {
    //on fait ensuite appel au service
    this.ingredientService
      //pour effectuer la requête post pour nous
      .add({ name: name } as Ingredient)
      //et on attend qu'il nous renvoie le résultat
      .subscribe((ingredient) => {
        //pour l'ajouter dans notre liste d'ingrédients de façon à ne pas avoir à recharger la liste via une requête
        this.ingredients.push(ingredient);
        console.log(ingredient);
      });
  }
}
