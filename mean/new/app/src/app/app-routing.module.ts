import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngredientComponent } from './ingredient/ingredient.component';
import { PizzaComponent } from './pizza/pizza.component';

const routes: Routes = [
  { path: 'ingredient', component: IngredientComponent },
  { path: 'pizza', component: PizzaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
