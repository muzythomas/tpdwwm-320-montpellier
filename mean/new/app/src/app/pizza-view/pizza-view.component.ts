import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Pizza } from '../models/Pizza';
import { PizzaService } from '../services/pizza.service';

@Component({
    selector: 'app-pizza-view',
    templateUrl: './pizza-view.component.html',
    styleUrls: ['./pizza-view.component.css'],
})
export class PizzaViewComponent implements OnInit, OnDestroy {
    constructor(private pizzaService: PizzaService) {}

    //@Input permet de faire passer un paramètre lors du montage de notre component dans une autre page
    //cela nous permet d'appeler ce component avec <app-pizza-view [id]="id"></app-pizza-view>
    // ou <app-pizza-view [pizza]="objetPizza"></app-pizza-view>
    @Input() id?: string;
    @Input() pizza?: Pizza;

    private pizzaSubscription?: Subscription;
    ngOnInit(): void {
        //si on a pas reçu l'objet en entier
        if (!this.pizza) {
            if (this.id) {
                //on va le chercher via notre service grâce à son id
                this.pizzaSubscription = this.pizzaService
                    .getById(this.id)
                    .subscribe((pizza) => {
                        this.pizza = pizza;
                    });
            }
        }
    }

    ngOnDestroy(): void {
        if (this.pizzaSubscription) {
            this.pizzaSubscription.unsubscribe();
        }
    }

    //for image testing purposes
    randomImage(): string {
        return `pizza${Math.floor(Math.random() * 5) + 1}.png`;
    }
}
