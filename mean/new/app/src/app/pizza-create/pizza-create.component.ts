import {
    Component,
    EventEmitter,
    OnDestroy,
    OnInit,
    Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Ingredient } from '../models/Ingredient';
import { Pizza } from '../models/Pizza';
import { PizzaBase } from '../models/PizzaBase';
import { IngredientService } from '../services/ingredient.service';
import { PizzaBaseService } from '../services/pizza-base.service';
import { PizzaService } from '../services/pizza.service';

@Component({
    selector: 'app-pizza-create',
    templateUrl: './pizza-create.component.html',
    styleUrls: ['./pizza-create.component.css'],
})
export class PizzaCreateComponent implements OnInit, OnDestroy {
    //un FormGroup (= un formulaire) contient des FormControl (champs de formulaires)
    public formAdd?: FormGroup;
    public formIngredients: Ingredient[] = [];
    public pizzaBases: PizzaBase[] = [];
    public ingredients: Ingredient[] = [];

    private ingredientSubscription?: Subscription;
    private pizzaBaseSubscription?: Subscription;
    private pizzaSubscription?: Subscription;

    @Output() newPizza = new EventEmitter<Pizza>();

    constructor(
        private pizzaService: PizzaService,
        private ingredientService: IngredientService,
        private pizzaBaseService: PizzaBaseService,
        //on a besoin de formBuilder pour créer des formulaires
        private formBuilder: FormBuilder
    ) {}

    ngOnInit(): void {
        this.ingredientSubscription = this.ingredientService
            .get()
            .subscribe((ingredients: Ingredient[]) => {
                this.ingredients = ingredients;
            });
        this.pizzaBaseSubscription = this.pizzaBaseService
            .get()
            .subscribe((pizzaBases: PizzaBase[]) => {
                this.pizzaBases = pizzaBases;
            });

        //a l'init du component on crée notre formulaire
        this.formAdd = this.initForm();
    }
    ngOnDestroy(): void {
        if (this.ingredientSubscription) {
            this.ingredientSubscription.unsubscribe();
        }

        if (this.pizzaSubscription) {
            this.pizzaSubscription.unsubscribe();
        }
        if (this.pizzaBaseSubscription) {
            this.pizzaBaseSubscription.unsubscribe();
        }
    }

    initForm(): FormGroup {
        //on prépare notre formulaire à l'aide de notre formBuilder
        return this.formBuilder.group({
            //on précise chaque champ de notre formulaire
            name: [''],
            base: ['---Choisir une Base---'],
        });
    }

    add(): void {
        if (this.formAdd) {
            //pour ajouter notre pizza on récupère les valeurs de formulaire
            const newPizza = this.formAdd.value;
            //ajout de chaque id d'ingrédient dans notre pizza
            newPizza.ingredients = [];
            this.formIngredients.forEach((ingredient) => {
                newPizza.ingredients.push(ingredient._id);
            });
            //on envoie la pizza
            this.pizzaSubscription = this.pizzaService
                .add(newPizza)
                .subscribe((pizza) => {
                    console.log(pizza);
                    this.newPizza.emit(pizza);
                });
        }
    }

    newIngredient(id: string) {
        const ingredient = this.ingredients.find(
            (i: Ingredient) => i._id === id
        );
        if (ingredient) {
            this.formIngredients.push(ingredient);
        }
    }

    removeIngredient(toRemove: Ingredient) {
        this.formIngredients = this.formIngredients.filter(
            (i) => i._id !== toRemove._id
        );
    }
}
