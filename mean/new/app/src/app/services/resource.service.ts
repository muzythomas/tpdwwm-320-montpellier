import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Resource } from '../models/Resource';
import { environment } from './../../environments/environment';

export abstract class ResourceService<T extends Resource> {
    private url: string = `${environment.api.url}:${environment.api.port}`;

    constructor(private http: HttpClient, private endPoint: string) {}

    public get(): Observable<T[]> {
        return this.http.get<T[]>(`${this.url}/${this.endPoint}`);
    }

    public getById(id: string): Observable<T> {
        return this.http.get<T>(`${this.url}/${this.endPoint}/${id}`);
    }

    public add(toAdd: T): Observable<T> {
        return this.http.post<T>(`${this.url}/${this.endPoint}`, toAdd);
    }

    public delete(toDelete: T): Observable<any> {
        return this.http.delete<any>(
            `${this.url}/${this.endPoint}/${toDelete._id}`
        );
    }

    public edit(toEdit: T): Observable<T> {
        return this.http.put<T>(
            `${this.url}/${this.endPoint}/${toEdit._id}`,
            toEdit
        );
    }
}
