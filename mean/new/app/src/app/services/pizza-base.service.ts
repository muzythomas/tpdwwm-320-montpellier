import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PizzaBase } from '../models/PizzaBase';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class PizzaBaseService extends ResourceService<PizzaBase> {
    constructor(http: HttpClient) {
        super(http, 'pizzaBase');
    }
}
