import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IngredientCategory } from '../models/IngredientCategory';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class IngredientCategoryService extends ResourceService<IngredientCategory> {
    constructor(http: HttpClient) {
        super(http, 'ingredientCategory');
    }
}
