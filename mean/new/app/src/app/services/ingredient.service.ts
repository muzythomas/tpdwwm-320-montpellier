import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ingredient } from '../models/Ingredient';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class IngredientService extends ResourceService<Ingredient> {
    constructor(http: HttpClient) {
        super(http, 'ingredient');
    }
}
