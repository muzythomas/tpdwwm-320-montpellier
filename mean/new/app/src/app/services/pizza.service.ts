import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pizza } from '../models/Pizza';
import { ResourceService } from './resource.service';

@Injectable({
    providedIn: 'root',
})
export class PizzaService extends ResourceService<Pizza> {
    constructor(http: HttpClient) {
        super(http, 'pizza');
    }
}
