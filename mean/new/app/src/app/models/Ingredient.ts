import { IngredientCategory } from './IngredientCategory';
import { Resource } from './Resource';

export interface Ingredient extends Resource {
    name: string;
    slug: string;
    category: IngredientCategory;
}
