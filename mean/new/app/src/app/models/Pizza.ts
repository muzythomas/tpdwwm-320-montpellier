import { Ingredient } from './Ingredient';
import { PizzaBase } from './PizzaBase';
import { Resource } from './Resource';

export interface Pizza extends Resource {
    size: number;
    name: string;
    base: PizzaBase;
    ingredients: Ingredient[];
}
