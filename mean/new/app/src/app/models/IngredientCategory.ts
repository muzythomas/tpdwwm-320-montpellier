import { Ingredient } from './Ingredient';
import { Resource } from './Resource';

export interface IngredientCategory extends Resource {
    ingredients: Ingredient[];
    name: string;
    slug: string;
}
