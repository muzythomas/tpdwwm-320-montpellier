import { Resource } from './Resource';

export interface PizzaBase extends Resource {
    name: string;
    slug: string;
}
