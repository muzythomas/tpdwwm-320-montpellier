import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PizzaComponent } from './pizza/pizza.component';
import { IngredientComponent } from './ingredient/ingredient.component';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { PizzaViewComponent } from './pizza-view/pizza-view.component';
import { PizzaModifyComponent } from './pizza-modify/pizza-modify.component';
import { PizzaCreateComponent } from './pizza-create/pizza-create.component';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
    declarations: [
        AppComponent,
        PizzaComponent,
        IngredientComponent,
        PizzaViewComponent,
        PizzaModifyComponent,
        PizzaCreateComponent,
        DialogComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
