import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.css'],
})
export class DialogComponent implements OnInit {
    @Input() message: string = 'Are you sure ?';
    @Input() action: string = 'Confirm';

    @Output() confirmation: EventEmitter<boolean> = new EventEmitter();
    constructor() {}

    ngOnInit(): void {}

    confirm() {
        this.confirmation.emit(true);
    }

    cancel() {
        this.confirmation.emit(false);
    }
}
