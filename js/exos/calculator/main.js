
document.addEventListener('DOMContentLoaded', () => {
    let calculatorScreen = document.getElementById('screen');
    let numbers = document.getElementById('numbers');
    let operations = document.getElementById('operations');

    //on prépare des variables pour stocker les opérandes de notre calcul
    let a = null;
    let op = null;
    let b = null;
    //permet d'indiquer si un calcul vient de finir pour assiter notre fonction DEL 
    let finCalcul = false;

    //permet de savoir si on chaine les calculs ou pas
    //de façon à pouvoir calculer chaque opération avant d'en utiliser une autre
    let premierCalcul = true;

    //on met nos event listener sur tous les elements de number
    numbers.childNodes.forEach(numberKey => {
        numberKey.addEventListener('click', (event) => {
            let number = event.target.getAttribute('keyData');
            calculatorScreen.value += number;
            //
            finCalcul = false;
        })
    })
    operations.childNodes.forEach(operationkey => {
        operationkey.addEventListener('click', (event) => {
            //on récupère la valeur de la touche appuyée dans notre attribut personnalise keyData
            let operation = event.target.getAttribute('keyData');
            //si l'opération demandée est un calcul
            if (operation === OPERATIONS.ADD || operation === OPERATIONS.DIV || operation === OPERATIONS.SUB || operation === OPERATIONS.MUL){
                if (premierCalcul){
                    //on récupère l'opérande 1
                    a = calculatorScreen.value;
                    //on stocke l'opération
                    op = operation;
                    //on affiche l'opération à l'écran
                    calculatorScreen.value += operation;
                    premierCalcul = false;
                } else {
                    //si on enchaine les opérations, on doit calculer le résultat de la première, et l'utiliser comme opérande pour la seconde
                    //substring permet de récuperer une chaine dans une chaine
                    //ici on veut récuperer la chaine qui commence après le caractère de l'opération, jusqu'à la fin
                    //c'est à dire si on a "33+45" on veut récuperer 45
                    //on demande donc à substring de couper après le +, jusqu'à la fin de la chaine
                    b = calculatorScreen.value.substring(a.length + op.length);
                    a = calcul(parseInt(a), parseInt(b), op);
                    op = operation;
                    calculatorScreen.value = a + operation;
                }
            }   
            //si l'opération demandée est le résultat
            if (operation === OPERATIONS.EQ){
                //on récupère la deuxième opérande
                b = calculatorScreen.value.substring(a.length + op.length);
                //et on affiche le résultat à l'écran
                calculatorScreen.value = calcul(parseInt(a), parseInt(b), op);
                //on signale la fin du calcul
                finCalcul = true;
            }
            //si on demande de clear
            if (operation === OPERATIONS.CE){
                //on remet tout à zéro
                a = null;
                b = null;
                op = null;
                calculatorScreen.value = "";
            }
            //si on demande de supprimer le dernier caractère
            if (operation === OPERATIONS.DEL){
                if (finCalcul){
                    calculatorScreen.value = "";
                } else {
                    //on supprime le dernier caractère à l'aide de slice(0, -1) qui fait une copie de la string sans le dernier caractère 
                    calculatorScreen.value = calculatorScreen.value.slice(0, -1); 
                }
                //si le = est utilisé, on "réinitialise" la chaine de calculs
                premierCalcul = true;
            }
        })
    })
});

const OPERATIONS = {
    DIV :"÷" ,
    MUL: "×",
    ADD: "+",
    SUB: "−", 
    ERR: "ERR",
    EQ : "=",
    CE : "CE",
    DEL : "DEL"
}

function calcul(x, y, operation){
    let res = 0;
    switch(operation){
        case OPERATIONS.ADD: 
            res = x + y;
            break;
        case OPERATIONS.MUL:
            res = x * y;
            break;
        case OPERATIONS.DIV:
            if (y === 0){
                return OPERATIONS.ERR;
            }
            res = x / y;
            break;
        case OPERATIONS.SUB:
            res = x - y;
    }
    return res;
}