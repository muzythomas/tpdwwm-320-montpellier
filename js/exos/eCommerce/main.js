let productData = data.slice(0, 50);
//on prépare un tableau pour stocker les données de notre panier
let basketData = [];
let priceFormat = new Intl.NumberFormat("fr-FR", {
    style: "currency",
    currency: "EUR",
});

//une fois la page chargée
document.addEventListener("DOMContentLoaded", () => {
    //on affiche nos produits
    displayProducts(productData);
    //au chargement on récupère les données du dernier panier stockées en JSON 
    //JSON.parse les transforme en objets à nouveau
    basketData = JSON.parse(localStorage.getItem('basket'));
    displayBasket(basketData);
});

//notre fonction d'affichage de produits nécessite un tableau de produits
function displayProducts(products) {
    let productsFragment = document.createDocumentFragment();
    document.getElementById("products").innerHTML = "";
    products.forEach((product) => {
        let productCard = document.createElement("div");
        let productName = document.createElement("span");
        let productPrice = document.createElement("span");
        let productImage = document.createElement("img");

        let productAddToBasket = document.createElement("button");

        let productQuantity = document.createElement("input");
        productQuantity.type = "number";
        productQuantity.value = 1;

        productName.textContent = product.name;
        productPrice.textContent = priceFormat.format(product.price);
        productImage.src = product.image;
        productImage.alt = `${product.name} picture`;

        productAddToBasket.textContent = "+";
        productAddToBasket.addEventListener("click", () => {
            //on ajoute un produit a notre panier
            //on doit convertir la valeur de quantité en nombre entier car les champs input renvoient du texte
            addToBasket(product, parseInt(productQuantity.value));
            //une fois fait, on affiche le panier
            displayBasket(basketData);
        });

        productCard.classList.add("product-card");
        productName.classList.add("product-name");
        productImage.classList.add("product-img");
        productPrice.classList.add("product-price");

        productCard.appendChild(productImage);
        productCard.appendChild(productName);
        productCard.appendChild(productQuantity);
        productCard.appendChild(productPrice);
        productCard.appendChild(productAddToBasket);
        productsFragment.appendChild(productCard);
    });
    document.getElementById("products").appendChild(productsFragment);
}

function displayBasket(basket) {
    let basketDiv = document.getElementById("basket");
    //on vide le panier avant de le redessiner
    basketDiv.innerHTML = "";
    let basketFragment = document.createDocumentFragment();

    let basketTotalValue = 0;
    let basketTotal = document.createElement("span");
    basket.forEach((basketRow) => {
        let basketRowDiv = document.createElement("div");
        let basketRowName = document.createElement("span");
        let basketRowQuantity = document.createElement("span");
        let basketRowPrice = document.createElement("span");
        let basketRemoveOne = document.createElement("button");
        let basketAddOne = document.createElement("button");
        let basketRemoveRow = document.createElement("button");
        
        basketRemoveOne.textContent = "-";
        basketRemoveOne.addEventListener("click", () => {
            removeOneFromBasket(basketRow.product);
            displayBasket(basketData);
        });

        basketAddOne.textContent = "+";
        basketAddOne.addEventListener("click", () => {
            addOneToBasket(basketRow.product);
            displayBasket(basketData);
        });

        basketRemoveRow.textContent = "🗑";
        basketRemoveRow.addEventListener("click", () => {
            removeRowFromBasket(basketRow.product);
            displayBasket(basketData);
        })

        basketRowName.textContent = basketRow.product.name;
        basketRowQuantity.textContent = "x" + basketRow.quantity;
        basketRowPrice.textContent = priceFormat.format(
            basketRow.product.price * basketRow.quantity
        );
        basketTotalValue += basketRow.product.price * basketRow.quantity;

        basketRowDiv.classList.add("basket-row");
        basketRowName.classList.add("basket-row-name");
        basketRowQuantity.classList.add("basket-row-quantity");
        basketRowPrice.classList.add("basket-row-price");

        basketRowDiv.appendChild(basketRowName);
        basketRowDiv.appendChild(basketRemoveOne);
        basketRowDiv.appendChild(basketRowQuantity);
        basketRowDiv.appendChild(basketAddOne);
        basketRowDiv.appendChild(basketRowPrice);
        basketRowDiv.appendChild(basketRemoveRow);

        basketFragment.appendChild(basketRowDiv);
    });
    basketTotal.textContent = `Total : ${priceFormat.format(basketTotalValue)}`;
    basketFragment.appendChild(basketTotal);
    basketDiv.appendChild(basketFragment);
}

function addOneToBasket(product) {
    addToBasket(product, 1);
}

function addToBasket(product, quantity) {
    //pour ajouter un produit dans notre panier, on vérifie d'abord qu'il n'existe pas déjà dans le panier
    let basketRow = null;
    //basketData.find permet de trouver un element dans notre tableau basketData
    //row => row.product.id === product.id signifie qu'on cherche une ligne dans notre tableau
    //dont l'id de produit correspond au produit qu'on essaye d'ajouter
    if ((basketRow = basketData.find((row) => row.product.id === product.id))) {
        //si c'est le cas, on augmente juste la quantité
        basketRow.quantity += quantity;
    } else {
        //sinon, on l'insère avec sa quantité dans le panier
        basketData.push({ product: product, quantity: quantity });
    }
    //a chaque fois qu'on change le panier, on stocke dans le stockage local
    localStorage.setItem('basket', JSON.stringify(basketData));
}

function removeOneFromBasket(product) {
    let basketRow = null;
    //si on trouve un produit
    if ((basketRow = basketData.find((row) => row.product.id === product.id))) {
        //on reduit juste la quantité
        basketRow.quantity--;
        if (basketRow.quantity === 0) {
            //basketData.splice() permet de supprimer notre ligne
            basketData.splice(basketData.indexOf(basketRow), 1);
        }
    }
    localStorage.setItem('basket', JSON.stringify(basketData));
}

function removeRowFromBasket(product) {
    let basketRow = null;
    //si on trouve un produit
    if ((basketRow = basketData.find((row) => row.product.id === product.id))) {
        //basketData.splice() permet de supprimer notre ligne
        basketData.splice(basketData.indexOf(basketRow), 1);
    }
    localStorage.setItem('basket', JSON.stringify(basketData));
}
