# Interface E-Commerce, panier de produits 

Réaliser une interface permettant de parcourir une liste de produits (contenus dans `products.json`).
L'interface permettra d'ajouter des produits dans un panier, affiché quelque part sur la page. 
Le panier sera constitué d'une liste de produits avec quantité, et total de prix.

Un produit contient les propriété suivantes :
* `id`
* `name`
* `price`
* `image`
