document.addEventListener("DOMContentLoaded", () => {
    const cityInput = document.getElementById("city");

    document.getElementById("go").addEventListener("click", () => {
        getWeather(cityInput.value);
    });
});

function getWeather(cityName) {
    //on prépare notre requête
    const request = new XMLHttpRequest();
    const url = `http://api.openweathermap.org/data/2.5/weather?lang=fr&units=metric&q=${cityName}&appid=08b5de14418a73031ecee7ce184e71cb`;
    request.open("GET", url);
    //on prévoit ce qu'on devra exécuter à l'arrivée de la réponse
    request.addEventListener("load", () => {
        const response = JSON.parse(request.response);
        console.log("xhr", response);
        //on stocke notre météo en cache pour réutilisation future
        localStorage.setItem(
            "weather",
            JSON.stringify({ time: Date.now(), response })
        );
        //on affiche la météo
        displayWeather(response);
    });

    //si on a une donnée en cache
    if (localStorage.getItem("weather")) {
        const cachedWeather = JSON.parse(localStorage.getItem("weather"));
        //si notre donnée mise en cache a + de 10min d'ancienneté
        //OU si le nom ne correspond pas
        if (Date.now() >= cachedWeather.time + 600000 || cachedWeather.response.name !== cityName) {
            //on la met à jour en envoyant la requête
            request.send();
        } else {
            //sinon on l'utilise pour afficher la météo
            console.log("cache", cachedWeather);
            displayWeather(cachedWeather.response);
        }
    } else {
        //si aucune donnée n'est cachée on va la chercher via l'api
        request.send();
    }
}

function displayWeather(data) {
    let weatherReport = document.getElementById("weatherReport");
    weatherReport.innerHTML = "";
    let weather = document.createElement("div");
    weather.textContent = data.main.temp + "°C";

    let icon = document.createElement("img");
    icon.src = `http://openweathermap.org/img/wn/${data.weather[0].icon}@2x.png`;
    icon.alt = data.weather[0].description;

    weatherReport.appendChild(weather);
    weatherReport.appendChild(icon);
}
