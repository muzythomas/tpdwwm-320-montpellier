# Récupération Asynchrone de Données Utilisateurs

À l'aide de l'API (Application Programming Interface) [randomuser.me](https://randomuser.me/) et de XMLHttpRequest, mettre en place une interface qui contiendra des informations d'utilisateurs générés aléatoirement. 

Cette interface permettra également de selectionner la nationalité dans un menu déroulant ainsi que le genre d'utilisateur à l'aide d'une checkbox, avec rafraichissement des données instantané. 

Les informations à afficher pour chaque utilisateur sont :
* La photo
* Le nom
* L'adresse
* Le numéro de téléphone
* La date de naissance