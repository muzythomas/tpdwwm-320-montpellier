let users = data;

//on attend que le document soit entièrement chargé
document.addEventListener("DOMContentLoaded", () => {
    //gestion du champ de recherche :
    let searchInput = document.getElementById("userSearch");
    //a chaque fois qu'on tape dans le champ de recherche
    //on utilise keyup et non keydown, car au moment ou keydown est lancé le champ input n'a pas encore changé
    searchInput.addEventListener("keyup", (event) => {
        //on récupère les termes de notre recherche et on les normalise en minuscle
        let search = event.target.value.toLowerCase();
        let filteredUsers = [];
        users.forEach((user) => {
            let userFullName = `${user.first_name} ${user.last_name}`.toLowerCase();
            //ensuite pour chaque utilisateur on vérifie qu'il y ait correspondance avec nom ou prénom normalisés également
            if (
                user.first_name.toLowerCase().startsWith(search) ||
                user.last_name.toLowerCase().startsWith(search) ||
                userFullName.startsWith(search)
            ) {
                filteredUsers.push(user);
            }
        });
        displayUsers(filteredUsers);
    });

    displayUsers(users);
});

function displayUsers(userList) {
    let userContainer = document.getElementById("container");
    //pour que les utilisateurs précédents soient retirés du document avant de redessiner
    //on vide le userContainer
    userContainer.innerHTML = "";

    //on crée un fragment pour optimiser le dessin de nos cards
    //le fragment nous permet de dessiner toutes nos cards à l'écran d'un coup au lieu de les faire un par un
    let userFragment = document.createDocumentFragment();

    userList.forEach((user) => {
        //on concatène first_name et last_name pour faire un nom complet
        let userFullName = `${user.first_name} ${user.last_name}`;

        //on crée une div visant à réceptionner toutes les informations sur l'user
        let userDiv = document.createElement("div");
        userDiv.classList.add("userCard");

        //on crée ensuite différents éléments pour chaque propriété de l'user
        let userNameSpan = document.createElement("span");
        userNameSpan.textContent = userFullName;
        userNameSpan.classList.add("userName");

        let userEmailSpan = document.createElement("span");
        userEmailSpan.textContent = user.email;
        userEmailSpan.classList.add("userEmail");

        let userGenderSpan = document.createElement("span");
        //on définit une icone pour le genre en fonction de la donnée trouvée dans l'user
        if (user.gender == "Male") {
            userGenderSpan.textContent = "♂";
            userGenderSpan.classList.add("male");
        } else {
            userGenderSpan.textContent = "♀";
            userGenderSpan.classList.add("female");
        }
        userGenderSpan.classList.add("userGender");

        //pour une image on doit générer un élément img
        let userAvatarImg = document.createElement("img");
        //et la src et l'alt se modifient comme n'importe quel attribut
        userAvatarImg.src = user.avatar;
        //alt est l'attribut qui définit ce qui s'affichera si jamais l'image ne s'affichait pas
        //pour le alt on veut utiliser "User's avatar"
        userAvatarImg.alt = `${userFullName}'s avatar`;
        userAvatarImg.classList.add("userAvatar");

        //on ajoute enfin nos éléments dans notre userDiv
        userDiv.appendChild(userAvatarImg);
        userDiv.appendChild(userNameSpan);
        userDiv.appendChild(userEmailSpan);
        userDiv.appendChild(userGenderSpan);

        //puis on ajoute notre div dans notre fragment au lieu de container directement
        userFragment.appendChild(userDiv);
    });
    //et enfin on ajoute notre fragment entier contenant toutes nos cards dans notre container
    userContainer.appendChild(userFragment);
}
