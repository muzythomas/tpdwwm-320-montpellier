# Liste d'Utilisateurs JSON

A partir d'un fichier JSON contenant des informations d'utilisateurs, établir une interface affichant les utilisateurs, chacun dans une `card`, avec leurs informations :

Un user a comme propriétés : 
* `id`
* `first_name`
* `last_name` 
* `email`
* `gender`
* `avatar`

Pour récupérer des données d'un fichier JSON il suffit de l'importer comme un script js. 
Dans le fichier MOCK_DATA.json fourni, les données sont stockées dans une variable `data`. 

