# Interface Clavier Aléatoire

Mettre en place une interface de clavier dont la disposition de touche est aléatoire et change à chaque chargement de page de façon à augmenter la sécurité (typiquement comme sur les interfaces de connexion d'une banque). Pour augmenter la sécurité, des cases vides pourront être