# ToDoList.js

Implémenter une simple Liste de Tâches (To Do List) en javascript et HTML.

Une To Do List se compose d'un champ d'input pour indiquer l'intitulé de la tâche à effectuer, et une liste de tâches auxquelles se rajoutent la tâche indiquée dans le champ d'input. 

On peut ensuite rajouter un bouton sur chaque tâche pour supprimer la tâche, et possiblement un bouton pour afficher la tâche comme "complétée". 