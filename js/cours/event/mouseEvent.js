//Les évènements du DOM sont des notifications du navigateur lorsque quelque choe d'utile intervient sur la page
//Chaque évènement est représentet par un objet d'interface Event contenant des propriétés permettant d'obtenir des informations supplémentaires sur l'évènement produit
//Un évènement peut être un clic, un scroll, un chargement d'un certain contenu, etc... et permet ainsi de gérer l'interaction de l'utilisateur avec l'interface d'un site web (entre autres)

//addEventListener permet d'ajouter un "gestionnaire d'évènements" qui, pour un évènement spécifique, pourra executer une fonction 
//la syntaxe () => {} permet de définir une fonction dite "anonyme" (qui n'a pas de nom) et qui sera appelée automatiquement lors de la résolution de l'évènement spécifié
//dans cette fonction anonyme on peut y faire passer un objet Event qui contiendra des informations sur l'évènement inspecté

//Par exemple pour détecter le chargement d'une page on peut utiliser l'évènement load
//Pour détecter le chargement complet du DOM on peut utiliser DOMContentLoaded
//cette détection d'évènement peut permettre entre autres de déclencher notre script au "bon moment"
//par exemple, le problème courant du script js qui s'exécute avant que le body soit chargé peut être réglé de la façon suivante : 

console.log("Avant DOMContendLoaded", document.getElementById('coco'));

document.addEventListener('DOMContentLoaded', ()  => {
    //le code écrit ici ne sera executé que lorsque le Contenu du DOM sera entièrement chargé
    console.log("Après DOMContentLoaded", document.getElementById('coco'));
    //on peut ainsi écrire tout notre code dans cette fonction si notre script dépend des éléments du DOM pour fonctionner

    //pour gérer un évènement de clic on peut utiliser la syntaxe suivante 
    let aCliquer = document.getElementById('acliquer');

    //on récupère notre <ul> d'id list
    let list = document.getElementById('list');
    let count = 0; //on initialise un compteur à 0
    //on ajoute un gestionnaire d'évènement de click sur notre element
    //on peut faire passer l'evenement capturé dans un objet de type Event
    //ou dans le cas de click de type MouseEvent https://developer.mozilla.org/fr/docs/Web/API/MouseEvent
    //qui possède des propriétés spécifiques et des informations supplémentaires
    aCliquer.addEventListener('click', (event) => {
        console.log('Clic sur bouton', event);

        //on crée un element de list <li>
        let listElement = document.createElement('li');
        //on lui attribue le texte avec notre compteur 
        listElement.textContent= `test ${++count}`; // ${} permet d'insérer une variable dans une chaine avec des backquote `` sans concaténer
        //on ajoute ensuite ce <li> dans notre <ul> en utilisant appendChild  
        liste.appendChild(listElement);

        let formField = document.getElementById('formfield');
        console.log(formField.value) //value permet de récuperer la valeur en texte d'un champ de formulaire au moment de l'évènement
    });


    //cet evenement gère les mouvements de la souris sur l'entiereté du document
    document.addEventListener('mousemove', (event) => {


        //l'objet de type MouseEvent contient par exemple x ou y qui donnent la position en abscisse et ordonnée de notre curseur au moment de l'apparition de l'évènement
        if (event.x > 300){
            console.log('coucou', event.x)
        }
    })
});    

