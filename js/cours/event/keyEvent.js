document.addEventListener("keydown", (event) => {
    //enregistre un évènement de touche de clavier lorsqu'on appuie dessus
    //console.log(event); //un event de type keydown contient des propriétés comme keyCode qui indiquent quelle touche à été appuyée
});
document.addEventListener("keyup", (event) => {
    //enregistre un évènement de touche de clavier lorsqu'on la relache
    //console.log(event);  //un event de type keyup contient des propriétés comme keyCode qui indiquent quelle touche à été relachée
});



document.addEventListener('DOMContentLoaded', () => {
    let textInput = document.getElementById('textInput');
    let passwordInput = document.getElementById('passwordInput');
    let infoTextInput = document.getElementById('infoTextInput');
    let infoPassword = document.getElementById('infoPassword');
    let passwordEye = document.getElementById('eye');

    //l'évènement change marche sur les input, select et textarea et notifie le gestionnaire d'évènement
    //lorsque la valeur d'un de ces champs (que ce soit textuel ou case à cocher etc) change 
    textInput.addEventListener('change', (event) => {

        //a chaque changement de notre champ input on vérifie que notre texte corresponde à un motif
        //string.match() permet d'appliquer une regex sur une chaine de caractères
        //ici la regex [a-z0-9]+ correspond à n'importe quelle string contenant des caractères minuscules ou des chiffres 
        let match = event.target.value.match("[a-z0-9]+");
        //si la string matchée est celle qui se trouve dans le champ input
        //c'est que le motif est respecté
        if (match[0] === match.input) {
            //alors on change notre affichage d'info bulle
            infoTextInput.textContent = "good";
            infoTextInput.classList.add('good')
            infoTextInput.classList.remove('bad')
        } else {
            infoTextInput.textContent = "bad";
            infoTextInput.classList.add('bad')
            infoTextInput.classList.remove('good')
        }


    });

    //si on voulait gérer l'affichage d'une information sur le mot de passe en temps réel, change 
    //ne serait pas approprié car n'envoyant l'event qu'à la fin du changement de valeur de notre champ
    //pour avoir les informations en temps réel, il faut utiliser un évènement qui serait lancé à chaque touche appuyée
    //keydown semble approprié pour cette utilisation
    passwordInput.addEventListener('keydown', (event) => {
        let value = event.target.value;
        if (value.length > 7){
            //bidouillage : 
            //on supprime les class en donnant une chaine vide en tant que className
            infoPassword.className = "";
            //puis on donne la classe qui nous interesse
            infoPassword.classList.add('password-ok')
        }
        if (value.length > 11){
            infoPassword.className = "";
            infoPassword.classList.add('password-good')
        }
        if (value.length < 8) {
            infoPassword.className = "";
            infoPassword.classList.add('password-bad')
        }
    });

    passwordEye.addEventListener("mousedown", (event) => {
        passwordInput.type = "text";
        passwordEye.src = "images/eye-solid.svg"
    })
    passwordEye.addEventListener("mouseup", (event) => {
        passwordInput.type = "password";
        passwordEye.src = "images/eye-slash-solid.svg"
    })
})