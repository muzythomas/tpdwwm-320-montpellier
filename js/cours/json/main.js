//Le Javascript Object Notation (JSON) est un format standard utilisé pour représenter des données structurées de façon semblabe aux objets Javascript. Il est utilisé pour structurer et transmettre des données sur des sites ou applications web. 
//La notation JSON étant très courante dans le développement web, il est facile de transférer des données même vers ou venant d'applications utilisant des technologies différentes, et de le faire via HTTP le JSON étant du texte. 

let user = {
    name : "John",
    dob : "1955-06-25",
    address: {
        city: "Genève",
        number: 45,
        street: "Rue de la Croix",
        country: "CH"
    }, 
    cars : 
    [
        {
            brand: 'Daewoo',
            model: 'A chier'
        }, 
        {
            brand: 'Lada',
            model: '333'
        },
        {
            brand: 'Scooter Gonflable',
            model: 'Inexistant'
        }
    ]
}

//pour serialiser un objet en texte formaté en JSON on utilise JSON.stringify
let userJson = JSON.stringify(user);

//pour deserialiser (texte vers objet) un texte formaté en JSON on utilise JSON.parse
let newUser = JSON.parse(userJson);

