//Javascript (ou ECMAScript le nom de la spécification officielle) est un langage de script utilisé dans le web pour manipuler communément le document HTML et les intéractions avec l'utilistaeur
//Il est désormais également utilisé via des plateformes comme NodeJs pour gérer les interactions dnas le back-end, mais son utilité principale reste tout de même dans la gestion du front-end
//La capacité de javascript à gérer l'immédiat et les réactions aux évènements le rendent le candidat favori pour rendre une interface dynamique sur un site web 

//En javascript une variable se déclare à l'aide du mot clé let
//let est l'équivalent de "soit", comme dans une phrase comme "soit x valant 2"
let x = 2;

//un tableau est déclaré comme d'hab 
let courses = ["Ravioli", "Cochonaille", "Whisky"];

//pour accéder à une case de tableau c'est une véritable surprise on utilise
console.log(courses[0]); //renvoie "Ravioli"
//console.log() permet d'afficher quelque chose dans la console du navigateur 

console.log(courses[43]); //renvoie undefined et non une erreur


//pour insérer un ou plusieurs éléments à la fin d'un tableau on utilise array.push
courses.push("Sauce Tomate", "Huile d'olive");
console.log(courses[3]);
console.log(courses[4]);

//array.pop() permet de retirer et renvoyer le dernier élement d'un tableau
console.log(courses.pop());
console.log(courses);

//array.shift() permet de retirer et renvoyer le premier element d'un tableau 
console.log(courses.shift());
console.log(courses);


//Note : en javascript les tableaux sont immutable, donc à l'exception d'opérations primitives comme pop, chaque opération de tableau complexe (shift, splice, slice etc) vont renvoyer une copie du tableau au lieu du tableau originel 

// https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide

//rappels sur l'égalité 
// en javascript il existe l'égalité faible == qui effectue la conversion des deux éléments avant d'effectuer la comparaison
// l'égalité stricte === qui effectue la comparaison sans conversion et donc renverra false si les types sont différents 
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Les_diff%C3%A9rents_tests_d_%C3%A9galit%C3%A9#Un_mod%C3%A8le_pour_mieux_comprendre

//le reste du cours a déjà été vu en algo hésitez pas à jeter un coup d'oeil 