//AJAX(Asynchronous Javascript And XML) n'est pas une technologie, mais un principe de programmation qui préconise l'utilisation de requêtes asynchrones de façon à pouvoir faire transiter des données entre un serveur et un client, et ainsi à ne pas paralyser l'interface/obliger à un rafraichissement de la page à chaque nouvelle donnée 

//il existe de nombreuses technologies permettant d'appliquer ce genre de principes
//l'une d'entre elles est la suivante : 
//pour envoyer une requête HTTP Asynchrone on peut utiliser XMLHttpRequest
//pour créer une XHR on utilise
let request = new XMLHttpRequest();
//pour définir la méthode utilisée et l'adresse de destination, on utilise open
request.open('GET', 'https://randomuser.me/api/');
//étant donné que la requête est envoyée sur le réseau, il est impossible de récupérer les informations de façon instantanée, et il faut attendre la réponse en différé
request.send()
//si on tente de récupérer la réponse directement, elle sera vide, car pas encore arrivée au moment de l'exécution du code
console.log("récupération synchrone", request.response)

//il faut donc utiliser un eventListener
//en utilisant load on indique qu'on veut exécuter un callback lorsque la reponse est chargée
request.addEventListener('load', (event) => {
    //on peut ensuite accéder a la réponse de cette façon par exemple
    console.log("récupération asynchrone", JSON.parse(event.target.response));
    //avec cette réponse, on peut ensuite manipuler l'interface en fonction des données reçues 
})