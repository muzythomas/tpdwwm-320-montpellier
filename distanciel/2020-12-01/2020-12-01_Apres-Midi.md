# Après-Midi du 1er Décembre 2020

## Conception d'un petit jeu vidéo multijoueur indeterminé avec `canvas` et `socket.io`

Entreprise sacrément naïve mais ô combien stimulante qui nous attend là.

La merveilleuse idée qui a été énoncée de "voir ce que ça prendrait de faire un jeu multijoueur en HTML et JS avec socket.io et HTMLCanvas " possède de nombreuses implications aussi douloureuses les unes que les autres.

Cependant, notre curiosité et naïveté triomphent nettement face au gouffre qui nous sépare de l'objectif final, et nous empruntons avec ravissement ce sentier qui causera notre perte.

Après cette introduction dramatique, voyons voir ce que nous avons tenté d'implémenter.

### Conception de notre application

Note importante: nous n'avons **aucune idée** du jeu que nous sommes en train d'implémenter. Ce qui en plus d'être extrêmement débile est extrêmement bizarre et contraignant. Qu'à cela ne tienne, on fait un jeu en 2D qui sera peut être un jeu de plate-forme, peut être de combat, peut-être un _top down rpg_, allez savoir...

La décision se prendra sur le dos de la technique ce qui n'est jamais incroyable niveau gestion de projet.

Pour ce que l'on **sait** qu'on aura à faire :

Il faut qu'on ait un serveur de jeu qui garde une trace de toutes les positions des joueurs en temps réel, et qui reçoive les informations que les joueurs ont bougé au fur et à mesure que les commandes arrivent du côté du client.

Donc si le `joueur1` se déplace à droite, il faut que le serveur soit au courant pour que `joueur1` bouge également chez `joueur2`, `joueur3` et `joueurn`.
..

D'une façon plus globale, le serveur va donc conserver un état d'application qui serait le `gameState`, et ce `gameState` devra être partagé entre tous les clients de l'application et être mis à jour à chaque fois que nécessaire ([divulgâchage](https://fr.wiktionary.org/wiki/divulg%C3%A2cher): tout le temps).

Mais commençons par le début.

### Dessiner quelque chose sur le canvas

Pour afficher quelque chose à l'écran, il faut pouvoir dessiner. Pour pouvoir dessiner, il faut une toile, et cette toile il s'agit de l'élément html `canvas`.

La [documentation de l'api canvas](https://developer.mozilla.org/fr/docs/Web/API/Canvas_API) va donc nous indiquer comment dessiner une simple forme sur notre page.

On se fait une page html, dans laquelle on insère un élément `<canvas>` qu'on manipulera dans un script en js.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>au secours</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
  </head>

  <body>
    <canvas id="gameCanvas" height="300" width="300"> </canvas>
  </body>
</html>
```

à l'aide ça va être trop long vous voulez pas juste regarder la vidéo ?

Si quelqu'un lit ça qu'il ou elle m'envoie un message privé et dans ce cas là je ferai ptet une explication détailée. Ca aura l'avantage de me prouver que j'écris pas ça pour que ce ne soit jamais lu.

A+
