# Après-Midi du 19 Novembre 2020

## Mise en place d'une application Angular communiquant avec une API

Dans l'établissement d'une application _full-stack_ la réalisation de l'`API` n'est bien évidemment que la première partie. Il faut ensuite proposer une interface rendant possible la manipulation des données de notre application au travers des appels de l'`API` mise en place.

Il est donc coutumier d'utiliser un _framework Front-End_ qui aide à gérer ces aspects du développement, de la gestion de l'interface graphique jusqu'aux appels vers ladite `API` et la gestion de ses résultats.

### Petit mot sur le Typescript

Le développement de notre application se fait à l'aide d'angular et sera donc en TypeScript. Le TypeScript, comme nous l'avons déjà vu auparavant, est une _surcouche_ de javascript permettant d'utiliser un _typage statique_ et de nouvelles structures de développement, le tout en gardant les capacités et les méthodes du javascript.

Le code TypeScript possède donc des capacités supplémentaires par rapport au javascript et permet de développer _plus sainement_. La rigueur que permet le typage statique à la place du dynamique permet de détecter certaines erreurs (grâce à l'interpréteur) en amont de l'exécution, ce que javascript ne permet que peu dû à sa façon de traiter les types de données (_dynamiquement_).

Le temps et la clarté gagnées grâce à l'utilisation du TypeScript sont d'une immense valeur et il faut savoir s'entraîner à l'écrire et le lire, et à réfléchir de façon à toujours savoir quel type de données on doit manipuler à chaque instant.

_note: Il n'est pas obligatoire de suivre les règles du typescript, surtout dans un projet angular, et il peut également uniquement servir d'outil ponctuel, cependant je vous recommande fortement de vous habituer à utiliser ses principes._

### Nouveau Projet Angular

Angular est donc le framework choisi ici. Il est un framework complet et non une bibliothèque d'interface (comme le seraient `react` et `vuejs` par exemple) et possède de nombreuses aides à la mise en place d'une
_single page application_, de la gestion des évènements utilisateurs jusqu'au routage, en passant par un client http complet.

Pour créer un projet Angular, il faut d'abord passer par sa `cli` (**C**ommand **L**ine **I**nterface) que l'on peut récupérer/mettre à jour via npm :

```
npm install -g @angular/cli
```

Grâce à `angular-cli` on peut désormais créer un nouveau projet angular à l'aide de, par exemple :

```
ng new app
```

Qui nous crée notre dossier d'application complet, cependant il faut auparavant réponde à certaines questions :

-   Désire-t-on utiliser le mode strict de TypeScript -> _à vous de choisir, je vous recommande oui_
-   Utiliser le routeur d'angular ? -> _dans notre cas oui_
-   Quel moteur de style ? -> _css pour nous_

### Mise en place des services angular pour communication avec l'API

Pour communiquer avec l'API on doit envoyer des requêtes HTTP à notre serveur.

Pour ce faire, on va utiliser `HttpClient` fourni avec angular.
Il faut déclarer son utilisation dans le fichier `app.module.ts` :

```ts
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  //...
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  //...
})
```

On ajoute ainsi dans le tableau `imports` notre `HttpClientModule` trouvé dans `@angular/common/http`.

Il est ainsi rendu disponible au travers de notre module `App` dans notre application angular.

On peut maintenant créer un fichier de service qui servira à communiquer avec notre api.

#### Pourquoi un service séparé ?

Cela fait partie des bonnes habitudes de placer ce genre de composants d'applications réutilisable et indépendant dans un service de façon à pouvoir le réutiliser facilement dans plusieurs parties de notre application et ce sans avoir à dupliquer du code.

#### Création et écriture du service :

On peut utiliser le `cli` pour créer notre service dans un nouveau dossier `services` :

```
ng g s services/ingredient
```

`ng` **`g`**`enerate` **`s`**`ervice` `services/ingredient` nous crée donc un fichier `ingredient.service.ts` dans un dossier `services` de notre application.

Ce service est une classe très simple rendue disponible dans l'application grâce à la mention `@Injectable` à la tête du fichier, permettant ainsi d'utiliser l'_injection de dépendance_ pour utiliser ce service.

```ts
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Ingredient } from "../models/Ingredient";

@Injectable({
    providedIn: "root",
})
export class IngredientService {
    //on définit notre url source
    private url: string = "http://localhost:4000/ingredient";

    //on injecte notre httpClient permettant d'effectuer des requêtes dans notre construct eur
    constructor(private http: HttpClient) {}

    //on définit nos requêtes à destination de l'api
    public get(): Observable<Ingredient[]> {
        //le résultat est sous forme d'observable permettant de détecter quand la donnée revient
        return this.http.get<Ingredient[]>(this.url);
    }

    public add(newIngredient: Ingredient): Observable<Ingredient> {
        return this.http.post<Ingredient>(this.url, newIngredient);
    }
}
```

L'utilisation d'`HttpClient` permet d'exécuter des requêtes HTTP à destination de notre `api` et de recevoir le résultat (la réponse) _encapsulée_ dans un `Observable`.

#### Rxjs et Observable

L'`Observable` est une structure de donnée permettant la gestion de données asynchrones facilement et est disponible dans la bibliothèque javascript `rxjs`.

Un `Observable` permet l'_inscription_ à un flux de donnée **asynchrone** de façon à être notifié lorsqu'une nouvelle donnée est reçue au travers de ce flux.
Ici on s'en sert pour avoir la possibilité de réagir au moment de la récéption de la réponse HTTP, un peu à la manière de l'objet `Promise` disponible dans javascript nativement.

La différence principale avec une `Promise` est ce qu'une promesse ne concerne qu'une donnée ponctuelle (ce qui fonctionnerait parfaitement bien dans le cas présent) alors qu'un `Observable`, bien plus puissant, permet la réception de plusieurs données à la file et de pouvoir les traiter les unes après les autres.

#### Utilisation de notre service dans un `component`

Il ne nous reste plus qu'à utiliser le service dans un `component` d'application pour le tester.

On crée donc un composant à l'aide de `ng g c ingredient` de façon à créer un dossier `ingredient` contenant notre `ingredient.component.html` et `ingredient.component.ts`.

Ce composant d'application doit être accessible au niveau de celle-ci, il faut donc paramétrer notre routeur pour qu'une route d'application monte notre composant sur la page.

Dans notre `app.routing.module` on définit donc une nouvelle route permettant d'accéder à notre composant :

```ts
//...
import { IngredientComponent } from "./ingredient/ingredient.component";

const routes: Routes = [{ path: "ingredient", component: IngredientComponent }];
//...
```

Pour que notre application puisse afficher le résultat du routeur, il faut également qu'on précise dans notre `app.component.html`:

```html
<router-outlet></router-outlet>
```

Ainsi, quelque soit le `component` que renverra le routeur à son interprétation de l'url, il s'affichera à cet endroit.

Au tour de notre `ingredient.component.ts` maintenant. Il faut que ce composant nous affiche la liste de nos ingrédients reçue via notre `api` grâce à notre service `IngredientService` :

```ts
//ingredient.component.ts
import { Component, OnInit } from "@angular/core";
import { Ingredient } from "../models/Ingredient";
import { IngredientService } from "../services/ingredient.service";

@Component({
    selector: "app-ingredient",
    templateUrl: "./ingredient.component.html",
    styleUrls: ["./ingredient.component.css"],
})
export class IngredientComponent implements OnInit {
    //on injecte notre service dans notre component
    constructor(private ingredientService: IngredientService) {}

    //on prépare un tableau dans lequel stocker nos ingrédients
    public ingredients: Ingredient[] = [];
    ngOnInit(): void {
        //ngOnInit s'exécute à l'initialisation du component
        //on souscrit au résultat de notre méthode get du service
        this.ingredientService.get().subscribe((ingredients) => {
            //une fois le résultat reçu on le range dans notre propriété
            this.ingredients = ingredients;
        });
    }
}
```

Il ne nous reste plus qu'à afficher ça sur la page `ingredient.component.html`:

```html
<div *ngIf="ingredients">
    <div *ngFor="let ingredient of ingredients">{{ingredient.name}}</div>
</div>
```

On lance notre serveur d'application angular à l'aide de `ng s` et on admire le résultat lorsqu'on essaye d'atteindre `localhost:4200/ingredient`.

La liste de nos ingrédients s'affiche, c'est magnifique.

####

#### Ajout d'un nouvel ingrédient :

Pour ajouter un nouvel ingrédient dans notre base, il faut qu'on puisse envoyer une requête `POST` à notre api sur `/ingredient`.

On implémente donc cette requête dans notre `IngredientService` :

```ts
//...
public add(newIngredient: Ingredient): Observable<Ingredient> {
  return this.http.post<Ingredient>(this.url, newIngredient);
}
//...
```

Il faut ensuite l'utiliser dans notre `IngredientComponent`.

On prépare un champ d'input, ainsi qu'un bouton, qui déclenchera au moment du clic une méthode appelant notre service :

```html
<!-- ingredient.component.html -->
<div>
    Ajouter ingrédient :

    <label>
        nom:
        <input type="text" #ingredientName />
    </label>
    <button (click)="add(ingredientName.value)">Créer</button>
</div>
```

```ts
//ingredient.component.ts


//...
//pour ajouter un nouvel ingrédient on a besoin de récupérer son nom
  add(name: string) {
    //on fait ensuite appel au service
    this.ingredientService
      //pour effectuer la requête post pour nous
      .add({ name: name } as Ingredient)
      //et on attend qu'il nous renvoie le résultat
      .subscribe((ingredient) => {
        //pour l'ajouter dans notre liste d'ingrédients de façon à ne pas avoir à recharger la liste via une requête
        this.ingredients.push(ingredient);
        console.log(ingredient);
      });
  }
```

En tapant un nom d'ingrédient et en appuyant sur `Créer`, celui ci est envoyé à notre `api` via notre `IngredientService`. L'`api` l'enregistre dans la base et renvoie le résultat, qui est ensuite inséré dans notre tableau d'ingrédients de façon à mettre à jour la liste automatiquement.

### Conclusion et suite

Notre comportement en rapport avec l'api est cantonné dans un service, ce qui permet au composant de ne pas avoir à se soucier des questions de requêtes http ou autre. Le composant ne se charge que d'afficher les données.

A l'avenir, il faudrait même séparer celui qui affiche les ressources de celui qui reçoit les données pour créer de nouvelles ressources, de cette façon le code sera plus facile à naviguer et à maintenir.

Il reste bien évidemment tout un tas de choses à faire, comme par exemple implémenter les autres requêtes (`delete` et `put`) à destinatino de notre `api` dans notre `service`. Il faut également implémenter les mêmes interactions pour les `pizze`, qui ont la difficulté d'avoir à gérer un nombre variable d'ingrédients, qu'il faudra peut être même pouvoir créer à la volée pour permettre à l'utilisateur de gérer sa liste de pizze avec plus de facilités.
