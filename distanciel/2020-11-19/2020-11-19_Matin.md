# Matinée du 19 Novembre 2020

## Rappels :

Voir aussi [le récapitulatif de la dernière fois](https://gitlab.com/muzythomas/tpdwwm-320-montpellier/-/blob/master/mean/old/api/recap.md).

### Mise en place d'une simple API HTTP à l'aide de `node` `express` et `mongoDB`

Pour mettre en place une API HTTP (genre `REST`) il faut qu'on puisse implémenter un serveur qui écoute les requêtes HTTP et réagisse en fonction du type de requête et de leur destination.

Pour nous mettre à coder il faut d'abord qu'on se crée un projet node et qu'on télécharge ses dépendances :

```
mkdir api
cd api
npm init -y
npm install --save express body-parser mongoose
```

### `express` pour créer notre infrastructure web

Express possède tout un tas de méthodes permettant de mettre en place une infrastructure web facilement, et la première étape est donc de créer un serveur qui pourra écouter sur un certain port en attente de requêtes entrantes.

### Un serveur à l'écoute

Dans un fichier `server.js` :

```js
const express = require("express");
const app = express();

//permet soit d'utiliser une variable d'environnement soit une valeur par défaut
const port = process.env.PORT || 4000;
const server = app.listen(port, () => {
    console.log(`Server listening on http://localhost://${port}`);
});
```

En lançant notre script à l'aide de

```
node server
```

on a donc comme résultat

```
Server listening on http://localhost:4000
```

### Des routes pour définir des actions

Pour que notre API soit fonctionnelle il faut que notre serveur réagisse à certaines requêtes, et réagisse différemment selon la ressource demandée ou la méthode (`GET POST PUT DELETE`...) utilisée.

Il faut donc que l'on définisse des routes. Une route est une liaison entre une url et une méthode, et une action.
Pour définir une route, on peut utiliser le composant `router` contenu dans `express`.

De façon à organiser notre projet correctement, on va ranger nos routes dans des fichiers séparés dans un dossier `routes` et demander à notre serveur de les prendre en compte :

```js
//fichier routes/bonjour.route.js

const express = require("express");
const router = express.Router();

router.route("/").get((req, res) => {
    //req est la requête entrante
    //res est la réponse qui sera envoyée
    //pour définir une réponse au format json on peut faire :
    res.json({ message: `Coucou` });
});
```

Dans notre fichier on récupère donc le routeur d'express pour y définir une route.

Cette route indique que pour une requête `GET` sur la ressource `/` de notre serveur, on renvoie un objet `json` suivant :

```json
{
    "message": "Coucou"
}
```

Intégrons cette route à notre serveur dans notre `server.js` :

```js
//...

const bonjourRoute = require("./routes/bonjour.route");
app.use("/bonjour", bonjourRoute);
const server = app.listen(port, () => {
    console.log(`Server listening on http://localhost://${port}`);
});
```

Maintenant en relançant notre serveur et en allant sur `localhost:4000/bonjour` on reçoit notre réponse en `json`.

## L'application Pizzeria

En cours on a décidé de mettre en place une API de gestion d'un menu de pizzeria, permettant donc de gérer les Pizze elles même et leurs ingrédients, pour l'instant en tout cas.

La mise en place du serveur étant déjà faite, on décide donc de mettre en place nos schemas de base de données à l'aide de l'ORM `Mongoose`.

### Les `Schema` et `Model` de Mongoose

Pour représenter une donnée dans notre application et ce en conjonction avec la base de données, `mongoose` nous permet de définir un objet de type `Schema` qui permet d'indiquer les différents champs d'un objet dans notre collection mongodb.

On a donc placé un dossier `models` dans notre projet pour contenir des fichiers représentant nos différentes ressources, pour l'instant `Pizza` et `Ingredient`, une `Pizza` possédant plusieurs ingrédients.

#### Notre Schema d'ingrédient :

Pour l'instant très simple, simplement un nom d'ingrédient qui servira à afficher sur le menu :

```js
//models/Ingredient.js
//on importe mongoose et l'objet Schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//on crée un nouvel ingrédient
const ingredient = new Schema(
    {
        //champ name de type string
        name: { type: String },
    },
    //la collection se nommera ingredients au niveau de mongodb
    { collection: "ingredients" }
);

//on exporte le "model" (un DAO) pour pouvoir l'utiliser dans le reste de l'application
module.exports = mongoose.model("ingredient", ingredient);
```

#### Schema de Pizza :

Pour les pizze, le modèle est légèrement plus complexe, puisqu'un pizza doit contenir une liste d'ingrédients (ici un tableau donc) :

```js
//models/Pizza.js
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pizza = new Schema(
    {
        size: { type: Number },
        name: { type: String },
        base: { type: String },
        //ici on définit un tableau d'ingrédients, qui sont représentés par leur ObjectID et font référence au Schema `ingredient` de nos ingrédients
        ingredients: [{ type: Schema.Types.ObjectId, ref: "ingredient" }],
    },
    {
        collection: "pizze",
    }
);

module.exports = mongoose.model("pizza", pizza);
```

Le tableau `ingredients` contiendra donc des `ObjectId` mongodb pour chaque ingrédient dont la pizza est composée. La `ref` est une référence au schema `ingredient` que l'on a créé au dessus.

### Utilisation de nos modèles de données dans nos routes

Il faut désormais qu'on code le comportement de nos routes en utilisant nos modèles de données pour accéder/modifier les ingrédients et pizze dans notre base mongodb.

#### Récupérer des ingrédients :

Pour récupérer nos ingrédients dans notre API on utilisera des requêtes avec la méthode `GET` (récupérer).

Pour nos ingrédients, dans notre fichier de route `ingredient.route.js` :

```js
//routes/ingredient.route.js
const express = require("express");
const router = express.Router();

//on récupère notre modele mongoose
let Ingredient = require("../model/ingredient");

//si on reçoit une requête get sur /
router.route("/").get((req, res) => {
    //on va chercher tous les ingrédients dans notre collection
    Ingredient.find()
        //find() renvoie une promesse (Promise) de Document[] (notre tableau de résultat)
        //then définit le comportement si la requête aboutit normalement :
        .then((ingredients) => res.json(ingredients))
        //catch permet d'attrapper une éventuelle erreur
        .catch((err) => {
            res.status(500).json({
                message: `Erreur récupération des ingredients : ${err}`,
            });
        });
});

router.route("/:id").get((req, res) => {
    const id = req.params.id;
    //findById permet de retrouver un objet par son objectId
    Ingredient.findById(id)
        .then((ingredient) => {
            //Dans le cas ou find s'exécute proprement, il faut tout de même vérifier si l'objectId correspond à un objet existant
            if (ingredient) {
                res.json(ingredient);
            } else {
                //si on reçoit null c'est que l'objet n'est pas trouvé
                res.status(404).json({
                    message: `Ingredient ${id} non trouvé`,
                });
            }
        })
        .catch((err) => {
            //catch s'exécutera si find ne se lance pas pour une raison x ou y (ex: objectId au format invalide)
            res.status(500).json({
                message: `Erreur récupération ingrédient : ${err}`,
            });
        });
});
```

Pour la première route matchant `/` en `GET`, on se contente de tout récupérer à l'aide de `find()` et on affiche tout.

Dans la seconde, on récupère un élément via son `id` au format `ObjectId`, on utilise donc `findById()`. Dans ce cas, il est possible qu'aucun objet ne soit trouvé selon cet `id`, on se prépare donc à renvoyer une erreur `404 Not Found`.

#### Ajouter des ingrédients

Ajouter une nouvelle ressource demande à ce qu'on précise dans la requête un corps au format `json`. Ce corps devra être lu lors du traitement de la route et inséré dans la base de données en tant que nouvelle ressource.

Pour lire le corps de la requête (`body`) au format `json`, on utilise le module `body-parser` dans notre application express, au niveau de notre `server.js` :

```js
//server.js
//..

const bodyParser = require("body-parser");
app.use(bodyParser.json());

//...
```

Dans notre fichier de route on peut ensuite définir une nouvelle route matchant une requête de méthode `POST` :

```js
router.route("/").post((req, res) => {
    //pour récupérer les données nécessaires à la création de notre ingredient
    //on doit lire le corps de la requête contenant le json
    //pour ça on a installé et utilisé body-parser au niveau de notre application express
    const ingredient = new Ingredient(req.body); //on passe le corps de la requête au constructeur du modele mongoose

    //on peut ensuite tenter de l'enregistrer en bdd
    ingredient
        .save()
        .then((ingredient) => {
            res.json({ message: `ingredient stocké : ${ingredient}` });
        })
        .catch((err) => {
            res.status(500).json({
                message: `erreur enregistrement ingredient : ${err}`,
            });
        });
});
```

#### Modification et suppression des ingrédients

Pour supprimer un ingrédient de la base, il faut qu'on prépare une route matchant sur `DELETE`, avec un paramètre `id` permettant de déterminer quelle ressource supprimer :

```js
router.route("/:id").delete((req, res) => {
    const id = req.params.id;
    //findbyanddelete trouve un objet en base et le supprime
    Ingredient.findByIdAndDelete(id)
        .then((deleted) =>
            //on nous renvoie également l'objet qui a été supprimé
            res.json({ message: `Ingredient ${deleted.id} supprimé` })
        )
        .catch((err) => {
            res.status(500).json({
                message: `Erreur suppression ingredient impossible : ${err}`,
            });
        });
});
```

Pour la modification d'une ressource cependant, il faut qu'on attende une requête `PUT` (qui permet de remplacer une ressource entièrement).

Pourquoi `PUT` et pas `PATCH` ? [Cet article](https://blog.octo.com/should-i-put-or-should-i-patch/) disponible en français explique bien les intérêts d'utiliser chaque méthode (et son implémentation) en détail et pour plusieurs cas de figure.

Notre route ressemble donc à ça :

```js
router.route("/:id").put((req, res) => {
    const id = req.params.id;
    const body = req.body;
    //finByIdAndUpdate trouve un objet et le met à jour selon ce qu'on lui passe en paramètre
    //le paramètre {new: true} est une option permettant de renvoyer non pas l'objet qui a été trouvé mais bien le résultat de la modification
    Ingredient.findByIdAndUpdate(id, body, { new: true })
        .then((ingredient) => {
            if (ingredient) {
                res.json(ingredient);
            } else {
                res.status(404).json({
                    message: `Ingredient ${id} non trouvé `,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                message: `Erreur modification ingredient impossible : ${err}`,
            });
        });
});
```

`findByIdAndUpdate` permet de trouver l'ingrédient selon son `id` et le mettre à jour selon le deuxième paramètre proposé, qui doit être une représentation de l'objet. Cette représentation peut être partielle, ce qui rend l'utilisation de `PUT` ou `PATCH` parfaitement possible avec `findByIdAndUpdate`.

#### Qu'en est il de nos pizze ?

Les pizze quand à elles ont un cas particulier, c'est que une pizza contient plusieurs ingrédients qui sont eux même des ressources gérées dans notre base. Au moment de les ajouter (`POST`) il faut donc préciser en json un tableau de ressources.

Il ne serait pas pratique, même si totalement possible, de devoir préciser le contenu de chaque ingrédient à chaque fois, et on doit donc les ajouter à l'aide d'une référence.

C'est pour cela que dans notre `Schema` on avait défini la propriété suivante :

```js
//model/Pizza.js
//...
    ingredients: [{ type: Schema.Types.ObjectId, ref: "ingredient" }],
//...
```

Il faut donc qu'on envoie un tableau d'`ObjectId` lors de notre requête pour que ceux-ci soient acceptés.

Mais comment faire si on désire envoyer non pas un tableau d'`ObjectId` mais un tableau de noms d'ingrédients ?

_nb: dans l'application finale on utilisera probablement des ids, c'est purement pour l'exercice qu'on s'impose ce genre de contraintes_

Dans ce cas de figure il faut donc d'abord qu'on aille chercher les objets eux même à partir de leur nom dans la base de données :

```js
//routes/pizza.route.js
router.route("/").post((req, res) => {
    //on récupère les données json de la nouvelle pizza
    const pizzaJson = req.body;
    //on cherche les ingrédients envoyés dans un tableau de nom
    Ingredient.find({ name: { $in: pizzaJson.ingredients } }).then((result) => {
        //on remplace ensuite le tableau d'ingrédients par le tableau d'objets récupérés de la bdd
        pizzaJson.ingredients = result;
        //puis on enregistre la pizza en base
        const newPizza = new Pizza(pizzaJson);
        newPizza
            .save()
            .then((pizza) => {
                res.json({ message: `pizza stockée : ${pizza}` });
            })
            .catch((err) => {
                res.status(500).json({
                    message: `impossible d'enregistrer la pizza : ${err}`,
                });
            });
    });
});
```

#### Récupération des pizze

Après avoir créé quelques pizze avec notre nouvelle route, on peut désormais aller en chercher. La même chose qu'avec les ingrédients, on crée une route qui matche sur `GET` et qui effectue un `find` dans mongodb.

Et lorsqu'on reçoit nos pizze, elles ressemblent à ça :

```json
{
    "_id": "5fb62ad2b0b5ec4a684c95b0",
    "ingredients": ["5fb3cc77c25c8362005a8f2a", "5fb3ccb23277a6390c3cdc11"],
    "size": 20,
    "name": "napoletana",
    "base": "tomate",
    "__v": 0
}
```

Pas de gros problème jusqu'ici, les ingredients sont bien représentés par des `ObjetId`, et il serait facile de les récupérer un à un.

Cependant, dans le cadre de notre application il sera fréquent de récupérer une `Pizza` et le nom de chaque ingrédient directement, dans le cas de la confection d'un menu par exemple.

Ne serait-il donc pas plus pratique de récupérer directement les noms de chaque ingrédient lors de la récupération de la `Pizza`, pour ne pas avoir a multiplier les requêtes ?

#### Utilisation de populate :

Notre `find` pour nos pizze va donc légèrement changer pour intégrer une nouvelle méthode qui permettra de récupérer les objets avec leurs propriétés, au lieu de juste leur `id` :

```js
//routes/pizza.route.js
router.route("/").get((req, res) => {
    Pizza.find()
        //Populate nous permet de remplir notre tableau d'ingrédients avec plus d'informations que seulement l'objectId, il nous permet ici d'également récupérer le nom de chaque ingrédient pour qu'il soit accessible dans le tableau
        //Path définit l'endroit dans l'objet (ici Pizza) que l'on doit "populate" (remplir)
        //select définit les propriétés de l'objet à remplir (ici Ingrédient) que l'on veut récupérer
        .populate({ path: "ingredients", select: "name" })
        .then((pizze) => res.json(pizze))
        .catch((err) =>
            res.json({ message: `erreur en récupérant les pizze : ${err}` })
        );
});
```

On peut appliquer la même méthode `populate` à `findById` également sans problème.

## A suivre

Pour la suite, il faudra mettre en place une application front très simple qui effectuera les requêtes vers notre serveur. Notre serveur ayant son comportement défini via les `routes` mises en place, sera capable de répondre et donc effectuer les traitements `CRUD` basiques de notre application.

Il sera possible d'implémenter des routes plus complexes plus tard, aisin qu'une meilleure gestion des erreurs, de façon à pouvoir faciliter le développement de notre application.
