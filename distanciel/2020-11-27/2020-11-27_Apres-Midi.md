# Après-Midi du 27 Novembre 2020

## Mise en place d'un composant _Dialogue de Confirmation_

Pour pouvoir alerter un utilisateur sur une action à venir, et s'assurer de sa volonté d'effectuer une action avec des conséquences importantes (une suppression par exemple), on va mettre en place une _boîte de dialogue_.

### Création de notre composant

Le sempiternel

```
ng g c dialog
```

nous génère notre composant. Ensuite on met en place quelque chose de très simple pour le style, un texte et deux boutons.

On va faire en sorte de pouvoir récupérer certaines informations pour rendre notre boîte de dialogue assez _modulable_, comme par exemple quel message on souhaite faire passer ou quelle action sera celle à confirmer (suppression, création) etc...

Pour pouvoir faire remonter l'information de si on a cliqué sur la _confirmation_ ou l'_annulation_ on met en place un simple `EventEmitter` qui fera remonter un `boolean`.

```ts
//dialog.component.ts
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
    selector: "app-dialog",
    templateUrl: "./dialog.component.html",
    styleUrls: ["./dialog.component.css"],
})
export class DialogComponent implements OnInit {
    //valeurs de personnalisation de notre boîte de dialogue
    @Input() message: string = "Are you sure ?";
    @Input() action: string = "confirm";

    @Output() confirmation: EventEmitter<boolean> = new EventEmitter();
    constructor() {}

    ngOnInit(): void {}

    confirm() {
        this.confirmation.emit(true);
    }

    cancel() {
        this.confirmation.emit(false);
    }
}
```

```html
<!-- dialog.component.html -->
<div class="dialog-box">
    <div class="message">{{message}} ?</div>
    <div class="actions">
        <button (click)="confirm()" type="button" class="btn-{{action}}">
            {{action | titlecase}}
        </button>
        <button (click)="cancel()" type="button">Cancel</button>
    </div>
</div>
```

Notre composant est maintenant utilisable dans un composant parent :

```html
<!-- pizza.component.html -->
<app-dialog
    message="Voulez vous supprimer {{pizza.pizza.name}} ?"
    action="delete"
>
</app-dialog>
```

_note: `message="chaine de caractères {{expression}}"` est possible, tout comme `[message]="expression"` est possible pour faire passer nos données à notre `app-dialog`_

### Réagir à un appui de bouton

On met donc en place une méthode nous permettant de réagir au `boolean` que nous enverra notre `EventEmitter` :

```html
<!-- pizza.component.html -->

<app-dialog
    message="Voulez vous supprimer {{pizza.pizza.name}} ?"
    action="delete"
    (confirmation)="onDeleteConfirmation($event, pizza.pizza)"
>
</app-dialog>
```

```ts
//pizza.component.ts
//permet de réagir à l'appui de bouton dans notre boite de dialogue dialogComponent
onDeleteConfirmation(confirmation: boolean, toDelete: Pizza) {
    if (confirmation) {
        this.pizzaService.delete(toDelete).subscribe((data) => {
            //on met à jour notre tableau de pizze avec un tableau dans lequel on ne garde pas la pizza à supprimer, ce qui a pour effet de supprimer la pizza du tableau
            this.pizze = this.pizze.filter(
                (p) => p.pizza._id != toDelete._id
            );
            //on met à jour également nos résultats de recherche sur lesquels l'affichage est basé
            this.refreshSearch();
        });
    }
}
```

Si on a reçu la confirmation, on peut donc supprimer notre Pizza.

### Gérer l'affichage dynamique

Il faut désormais que la boîte de dialogue apparaisse quand on clique sur le bouton supprimer de la Pizza, et qu'elle disparaisse si on appuie sur _annuler_ dans notre boîte de dialogue.

Pour ça on rajoute une clause `*ngIf` sur notre `DialogComponent` qui s'activera seulement si notre pizza est concernée.

La pizza concernée sera enregistrée grâce à notre méthode `delete()` déjà présente, dont la logique a été déplacée dans notre `onDeleteConfirmation`.

```ts
//pizza.component.ts

public pizzaToDelete?: Pizza;
    delete(toDelete: Pizza) {
        //en indiquant quelle pizza on compte supprimer, on peut afficher la boite de dialogue correspondante
        this.pizzaToDelete = toDelete;
}
```

En indiquant quelle pizza on veut supprimer, on peut la comparer à la pizza dans la boucle, et donc l'utiliser pour choisir d'afficher ou non notre dialogue.

```html
<!-- pizza.component.html -->
<app-dialog
    message="Voulez vous supprimer {{pizza.pizza.name}} ?"
    action="delete"
    *ngIf="pizzaToDelete == pizza.pizza"
    (confirmation)="onDeleteConfirmation($event, pizza.pizza)"
>
</app-dialog>
```

Maintenant il faut que la propriété `pizzaToDelete` revienne à `undefined` dans le cas où on cliquerait sur _annuler_ :

```ts
onDeleteConfirmation(confirmation: boolean, toDelete: Pizza) {
    if (confirmation) {
        this.pizzaService.delete(toDelete).subscribe((data) => {
            //on met à jour notre tableau de pizze avec un tableau dans lequel on ne garde pas la pizza à supprimer, ce qui a pour effet de supprimer la pizza du tableau
            this.pizze = this.pizze.filter(
                (p) => p.pizza._id != toDelete._id
            );
            //on met à jour également nos résultats de recherche sur lesquels l'affichage est basé
            this.refreshSearch();
        });
    } else {
        //pour faire disparaitre la boite de dialogue
        this.pizzaToDelete = undefined;
    }
}
```

### Donner une apparence de boîte de dialogue

Il faut désormais lui donner l'apparence de sa fonction. C'est à dire qu'il faut qu'elle soit _surimposée_ au contenu de la page, centrée, et bien visible. Il faut que le _focus_ soit sur la boîte de dialogue, de façon à bien indiquer que ce choix est important et doit être fait _maintenant_.

Dans le css de notre `DialogComponent` donc :

```css
.dialog-box {
    border-radius: 2px;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    background-color: white;
    height: 200px;
    width: 300px;

    font-family: sans-serif;
    color: var(--color-black);
}
```

C'est loin d'être parfait, c'est même plutôt moche, mais c'est juste pour le début. Déjà ça ressemble à une boîte.

Mais par contre elle se trouve dans la page au lieu d'au dessus du reste, et n'est pas au milieu de la page. Comment faire ?

#### `:host` pour cibler le composant lui même

Dans angular en css on peut utiliser le selecteur `:host`, similaire à `:root` il permet de cibler l'élément dans lequel notre composant possède son _rendu_.

On va donc se servir de ça pour faire en sorte de l'agrandir à la taille de la page, et utiliser `flexbox` pour centrer notre boîte de dialogue:

```css
:host {
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
}
```

Génial. Par contre la position n'est pas bonne, il faudrait que ça fasse toute la page à partir du _coin supérieur gauche_ (ou l'_origine_) de la page !

```css
:host {
    position: absolute; /* donne une position absolue et donc ne perturbe plus les autres éléments et leurs positions*/
    top: 0;
    left: 0;

    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
}
```

Quand je scroll la boîte ne suit pas !

```css
:host {
    position: fixed; /* donne une position absolue par rapport au contexte de l'écran et non de la page */
    top: 0;
    left: 0;

    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
}
```

Petit bonus, on va faire en sorte de rendre le contenu derrière moins visible pour bien laisser le "projecteur" sur notre dialogue :

```css
:host {
    position: fixed; /* donne une position absolue par rapport au contexte de l'écran et non de la page */
    top: 0;
    left: 0;

    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;

    background-color: rgba(0, 0, 0, 0.3);
}
```

### _Nota Bene_

Bien évidemment ce composant n'est pas parfait. Il n'est pas facilement réutilisable, et n'est pas très _angularien_ dans sa conception.

Pour bien faire, il faudrait peut être utiliser un service et des observables, ou ce genre de choses très _angulariennes_ pour le coup ;)

Mais pour l'instant ça marche.

Ne pas oublier, et là c'est le moment des locutions _bâteau_, que **le mieux est l'ennemi du bien**.

Et surtout que **l'optimisation prématurée est mauvaise**.

Certes je vous rabache les oreilles avec le besoin d'optimiser, de rendre le code propre, modulable, réutilisable.... Mais comment déterminer exactement à quel moment entreprendre ce travail titanesque de rendre un projet réutilisable ?

Il n'existera jamais de vraie réponse. Le besoin est tellement variable de projet en projet, de journées en journées, qu'il serait zêlé et possiblement stupide de tenter de mesurer le moment opportun pour entreprendre ce genre d'optimisations et autres _refactorisations_.

L'important est de tenter de _ressentir_ l'arriver d'un potentiel "point de rupture" du projet en quelque sorte, et l'anticiper de peu. Cela vient avec l'expérience, beaucoup de travail, et une fois qu'on a cette capacité on la perd très _très_ vite.

Rappelez vous donc que votre métier est tout de même de répondre à un besoin, et si vous passez votre temps à faire des composants réutilisables mais à ne jamais implémenter la solution à ce besoin, vous faites mal votre métier.

Gardez les yeux sur l'_objectif_ en quelque sorte !
