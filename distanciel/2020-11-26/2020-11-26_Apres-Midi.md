# Apres-Midi du 26 Novembre 2020

## 1 - Généralisation de nos services

Dans le cadre de notre application Pizzeria, nous avons quelques services qui se chargent de réunir certaines méthodes, elles même visant à effectuer les requêtes `http` nécessaires à la récupération des données de notre `api`.

Cependant, toutes ces méthodes, et donc ces services, sont quasi identiques, à la différence que les requêtes vont sur un autre _endpoint_ (une autre destination) de notre api, et que les ressources récupérées sont de types différents.

Cela nous a amené à avoir 4 services dont le code se répète, ce qui n'est forcément pas très joli.

### Création d'un service _générique_

Pour résoudre ce problème, on va donc créer un service _abstrait_ qui réunira l'implémentation de toutes ces méthodes `CRUD` tout en étant _type agnostic_, c'est à dire en laissant un _"trou"_ là ou le type et l'_endpoint_ devraient être renseignés, de façon à pouvoir l'utiliser autant pour les `Pizza`s que les `Ingredient`s juste en héritant de ce service.

Ce service générique aura pour but d'améliorer la maintanabilité de nos services et même de nous permettre d'en développer d'autres plus rapidement si jamais le besoin de le faire surgissait dans notre projet.

#### Création d'une classe abstraite

On crée donc notre service comme d'habitude, mais cette fois en lui donnant un nom légèrement plus _générique_, comme `ResourceService` par exemple :

```
ng g s services/resource
```

Ce qui donne :

```ts
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: "root",
})
export class ResourceService {
    constructor() {}
}
```

On va le rendre _abstrait_, c'est à dire qu'il n'aura pas pour vocation d'être instancié par lui même mais cellement d'être _hérité_. On se débarasse en même temps de la déclaration `@Injectable` vu qu'il ne sera jamais rendu disponible en tant que _service injectable_ au niveau d'angular par lui même, de par son _abstraction_.

```ts
export abstract class ResourceService {
    constructor() {}
}
```

#### Implémentation des méthodes génériques

Il faut maintenant qu'on offre à `ResourceService` les moyens d'effectuer des requêtes `HTTP`, puis qu'on lui implémente les méthodes ayant pour but de les effectuer. On récupère donc `HttpClient`, et on implémente notre `CRUD`:

```ts
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export abstract class ResourceService {
    private url: string = `http://localhost:4000`;

    constructor(private http: HttpClient) {}

    public get(): Observable {
        return this.http.get(`${this.url}`);
    }

    public getById(id: string) {
        return this.http.get(`${this.url}/${id}`);
    }

    public add(toAdd) {
        return this.http.post(`${this.url}`, toAdd);
    }

    public delete(toDelete) {
        return this.http.delete(`${this.url}/${toDelete._id}`);
    }

    public edit(toEdit) {
        return this.http.put(`${this.url}/${toEdit._id}`, toEdit);
    }
}
```

Superbe ! Juste un souci, on est en `typescript` ici et on veut/doit donc profiter de ces capacités à préciser le type de chaque donnée à l'oeuvre. Cependant, quel type met-on, étant donné que le service doit accepter un coup `Pizza`, un coup `Ingredient`, ou n'importe quelle autre ressource, et ce sans broncher ?

#### `T`, le type générique

En programmation, les génériques sont un moyen de développer des fonctions et composants utilisables avec une variété de types différents.

Pour les utiliser en `typescript` on utilise une `variable de type`, qu'on appellera `T` par convention.

Un exemple de cette utilisation est visible sur la [documentation de typescript](https://www.typescriptlang.org/docs/handbook/generics.html) (oui ça existe). Y est démontrée l'utilisation d'un type générique dans le cadre d'une définition de fonction.

Par exemple une fonction pourrait se déclarer soit avec un type précis, soit avec le _type mélangé_ `any` :

```ts
function identity(arg: number): number {
    return arg;
}
//ou bien
function identity(arg: any): any {
    return arg;
}
```

Dans le premier cas il n'est pas possible de donner autre chose qu'un nombre, et dans le second cas le type précis demandé est perdu dans les méandres de `any`. Donc comment faire pour qu'on puisse définir proprement notre fonction et pourtant utiliser le type qu'on veut ?

```ts
function identity<T>(arg: T): T {
    return arg;
}

let output = identity<string>("myString");
//       ^ = let output: string
```

Ici notre fonction a été déclarée avec des chevrons `< >` contenant la variable de type `T`. Cette même variable peut ensuite être utilisée comme type dans les paramètres d'entrée ainsi que dans les paramètres de retour, et dans tout le corps de la fonction.

Ensuite, on appelle la fonction en précisant le type qui sera utilisé (ici `string`) et on peut utiliser la fonction comme prévue.

_note: dans le cas d'un type "scalaire" comme `string` ou `number`, typescript peut deviner le type de la donnée utilisée sans le préciser à l'aide de `<Type>`. Cependant, pour des types plus complexes cette "inférence" ne serait pas possible_.

#### Utiliser `T` dans notre service

On va donc utiliser cette _variable de type_ pour généraliser notre service. On lui précise donc au niveau de la déclaration de classe que celle-ci accepte un type variable qu'on appellera `T`, et on utilise ensuite ce `T` dans toutes nos méthodes :

```ts
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export abstract class ResourceService<T> {
    private url: string = `http://localhost:4000`;

    constructor(private http: HttpClient) {}

    public get(): Observable<T[]> {
        return this.http.get<T[]>(`${this.url}`);
    }

    public getById(id: string): Observable<T> {
        return this.http.get<T>(`${this.url}/${id}`);
    }

    public add(toAdd: T): Observable<T> {
        return this.http.post<T>(`${this.url}`, toAdd);
    }

    public delete(toDelete: T): Observable<any> {
        return this.http.delete<any>(`${this.url}/${toDelete._id}`);
    }

    public edit(toEdit: T): Observable<T> {
        return this.http.put<T>(`${this.url}/${toEdit._id}`, toEdit);
    }
}
```

Imaginons maintenant que l'on utilise ce `ResourceService` pour en hériter, il faudra préciser le type voulu en utilisant `< >`, pour nos `Pizza` cela donnerait donc :

```ts
export class PizzaService extends ResourceService<Pizza> {
    //...
}
```

En s'instanciant, le `PizzaService` fera donc appel à une représentation de `ResourceService` avec `T = Pizza`.

#### Quid de notre `endpoint` ?

Il est vrai qu'on a oublié de préciser sur quelle destination nos requêtes iront taper dans le cadre de nos implémentations de services. Il faut donc que le `ResourceService` accepte un paramètre qui permettrait de le définir :

```ts
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export abstract class ResourceService<T> {
    private url: string = `http://localhost:4000`;

    constructor(private http: HttpClient, private endPoint: string) {}

    public get(): Observable<T[]> {
        return this.http.get<T[]>(`${this.url}/${this.endPoint}`);
    }

    public getById(id: string): Observable<T> {
        return this.http.get<T>(`${this.url}/${this.endPoint}/${id}`);
    }

    public add(toAdd: T): Observable<T> {
        return this.http.post<T>(`${this.url}/${this.endPoint}`, toAdd);
    }

    public delete(toDelete: T): Observable<any> {
        return this.http.delete<any>(
            `${this.url}/${this.endPoint}/${toDelete._id}`
        );
    }

    public edit(toEdit: T): Observable<T> {
        return this.http.put<T>(
            `${this.url}/${this.endPoint}/${toEdit._id}`,
            toEdit
        );
    }
}
```

Tout comme `HttpClient`, `endPoint` devient un paramètre dont la valeur devra être définie dans le constructeur de nos services _enfants_.

#### Rassurer Typescript

Impossible à montrer dans ce document, mais dans notre implémentation actuelle quelque chose cloche toujours.

TypeScript n'a aucun moyen de savoir ce que contiennent les potentiels types de données que l'on range habilement et sans distinction dans `T`. Il panique donc au moment de devoir chercher dans des objets de ce type `T`, comme par exemple lorsqu'on demande :

```ts
public edit(toEdit: T): Observable<T> {
    return this.http.put<T>(
        `${this.url}/${this.endPoint}/${toEdit._id}`, toEdit
        //            T._id, connaît pas !  <──╯
    );
}
```

Il faut donc que l'on précise à typescript que, quoi qu'il y ait dans `T`, cela contiendra une propriété `_id`.

#### Une _interface mère_ pour nos resources.

Nous allons créer un ancêtre commun à toutes nos ressources (`Pizza` et consorts) qui contiendra un `_id`, et on précisera à typescript que `T` sera une représentation d'un type _dans la lignée_ de cet ancêtre commun.

Rien de plus simple, on crée un fichier `models/Resource.ts` qui constituera notre _ressource générique_ :

```ts
export interface Resource {
    _id: string;
}
```

et on demande ensuite à toutes nos ressources d'en hériter, exemple avec `Ingredient` :

```ts
import { IngredientCategory } from "./IngredientCategory";
import { Resource } from "./Resource";
//                              ╭──> on hérite de l'interface Resource
export interface Ingredient extends Resource {
    //_id est contenu dans Resource et n'a donc plus besoin d'être ici
    name: string;
    slug: string;
    category: IngredientCategory;
}
```

On précise enfin à notre `ResourceService` que notre `T` sera un descendant de `Resource` :

```ts
//...
export abstract class ResourceService<T extends Resource> {
    //      T sera toujours une Resource  <──╯

    public edit(toEdit: T): Observable<T> {
        return this.http.put<T>(
            `${this.url}/${this.endPoint}/${toEdit._id}`,
            toEdit //    T._id, je connais !  <──╯
        );
    }
}
```

#### Implémentation de nos services

Il ne nous reste plus qu'à implémenter nos services en héritant de ce `ResourceService`. Exemple avec `PizzaService` :

```ts
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Pizza } from "../models/Pizza";
import { ResourceService } from "./resource.service";

@Injectable({
    providedIn: "root",
})
export class PizzaService extends ResourceService<Pizza> {
    constructor(http: HttpClient) {
        super(http, "pizza"); //super() nous sert à appeler le constructeur de ResourceService, à la manière de parent::__construct en php
    }
}
```

Et c'est tout. Nos services fonctionnent et on aura plus qu'à faire ça la prochaine fois qu'on voudra en implémenter un nouveau !

#### Utilisation de l'environnement

L'url de notre api est encore définie "à la main" dans notre `ResourceService`, et cela n'est pas assez _modulaire_ à notre goût n'est-ce pas ? Il faudrait retrouver ces informations d'_environnement_ dans un stockage plus _mutualisé_, pour faciliter l'implémentation d'un futur service par exemple, et la modification de ces données si cela était nécessaire.

Pour ça on peut définir ces données dans le fichier `environment.ts` se trouvant dans le dossier `environments` à la racine de notre dossier `src` :

```
└── src
    ├── app
    ┊    └── services
    ┊        └── resource.service.ts
    ┊
    └── environments
        └── environment.ts
```

```ts
//environments/environment.ts
export const environment = {
    production: false,
    //on peut y définir des paramètres de ce genre
    api: {
        url: "http://localhost",
        port: "4000",
    },
};
```

Pour les utiliser dans notre service on se contente ensuite d'importer cette constante et de lire à l'intérieur :

```ts
//...
import { environment } from "./../../environments/environment";

export abstract class ResourceService<T extends Resource> {
    private url: string = `${environment.api.url}:${environment.api.port}`;
    //...
}
```

## 2 - Champ de recherche de `PizzaComponent`

Nous avons commencé à impleménter un très simple, presque simpliste, champ de recherche pour filtrer nos `Pizza` par leur nom.

On a donc créé un champ `input` avec un évènement `(keyup)` permettant de filtrer nos pizze :

```html
<input (keyup)="search()" [(ngModel)]="pizzaSearch" />
```

_note: ngModel nécessite de rajouter `FormsModule` dans notre `app.module.ts`_

```ts
public pizzaSearch: string = '';
public pizzeResults: { pizza: Pizza; editMode: boolean }[] = [];

ngOnInit(): void {
    this.pizzaSubscription = this.pizzaService
        .get()
        .subscribe((pizze: Pizza[]) => {
            this.pizze = pizze.map((pizza) => {
                return { pizza: pizza, editMode: false };
            });

            //on remplit notre tableau de résultat avec toutes les pizze au début
            this.pizzeResults = Array.from(this.pizze);
        });
}

search() {
    this.pizzeResults = this.pizze.filter((pizza) => {
        return (
            //on filtre selon le nom a chaque appui de touche
            pizza.pizza.name
                .toLowerCase()
                .indexOf(this.pizzaSearch.toLowerCase()) !== -1
        );
    });
}
```

Après une modification de notre template pour que les pizze soient affichées à partir de `pizzeResults`, notre recherche fonctionne !

Mais qu'est ce que c'est que cette histoire de `indexOf` ?

C'est tout simplement une façon légèrement primitive mais parfaitement fonctionnelle d'implémenter une _recherche partielle_.

`indexOf` permet de renvoyer l'`index` auquel est trouvé un élément dans un _itérable_. C'est à dire qu'en demandant :

```js
"Salut les amis".indexOf("lu");
```

le résultat serait `2`.

Il a une particularité cependant, c'est que lorsqu'il ne trouve pas l'élément, il renvoie `-1`.

Ce qui est parfait pour nous ! On a qu'à éliminer (à l'aide de `filter`) toutes les pizze dont le nom ne contient pas le terme de recherche, et qui donc renverront `-1` à notre `indexOf`.

Du coup cela donne :

```
    ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐
sur ╎ Regina, Capriciosa, Napoletana, Quattro Stagione ╎,
    └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘
recherche :
┌────────────┐
│ gi         │
└────────────┘

résultats :

─ Quattro Sta[gi]one
- Re[gi]na
```

Là ou le `toLowerCase` est utile c'est qu'il permet de trouver un résultat quelque soit la casse.

```
    ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐
sur ╎ Regina, Capriciosa, Napoletana, Quattro Stagione ╎,
    └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘
recherche :
┌────────────┐
│ r          │
└────────────┘

résultats :

- [R]egina
- Cap[r]iciosa
- Quatt[r]o Stagione
```
