# Après-Midi du 23 Novembre 2020

## Mise en place d'un formulaire de modification de Pizza

Pour modifier nos `Pizza` dans notre application angular, il faut pouvoir utiliser un formulaire similaire à celui établi pour la création de nos entités.

### Un composant pour la modification

Afin de tenter de rendre plus propre notre application, et dans un même temps nous rapprocher de ce que angular _souhaite_ nous faire faire avec ses composants, on va créer un nouveau component permettant d'y mettre le formulaire d'édition ainsi que sa logique d'exécution :

```
ng g c pizza-modify
```

Dedans, on y copie notre formulaire de création et sa logique :

```html
//pizza-modify.component.html
<form [formGroup]="pizzaForm" (ngSubmit)="edit()">
    <label>
        Nom:
        <input formControlName="name" type="text" />
    </label>
    <label>
        size :
        <input formControlName="size" type="text" />
    </label>
    <label>
        base :
        <input formControlName="base" type="text" />
    </label>
    <label>
        ingredients :
        <select
            #ingredientSelect
            (change)="newIngredient(ingredientSelect.value)"
        >
            <option
                *ngFor="let ingredient of ingredients"
                [value]="ingredient._id"
            >
                {{ingredient.name}}
            </option>
        </select>
        <div *ngFor="let ingredient of formIngredients">
            {{ingredient.name}}
            <button type="button" (click)="removeIngredient(ingredient)">
                -
            </button>
        </div>
    </label>
    <button type="submit">Valider Changements</button>
</form>
```

Ce copier-coller nous indique également qu'il serait possible de créer un formulaire réutilisable dans un composant et d'utiliser celui-ci à la place.
Ce sera pour une autre fois. Pour l'instant il faut déjà que ça marche comme ça.

Au tour de la logique du composant maintenant.

Dans l'idée, il faudrait que ce composant soit utilisable par notre `pizza.component` de façon à pouvoir le _brancher_ selon notre bon vouloir. On va donc préparer notre `pizza-modify.component` à accepter une `Pizza` en entrée, ou bien un `ObjectId` identifiant une `Pizza`, lui permettant ainsi d'en récupérer une via notre service.

```ts
//pizza-modify.component.ts
import { Component, Input, OnInit } from '@angular/core';
import { PizzaService } from '../services/pizza.service';
import { Pizza } from '../models/Pizza';

export class PizzaModifyComponent implements OnInit {
  constructor(
    private pizzaService: PizzaService
  ) {}

  //@Input permet de faire passer un paramètre lors du montage de notre component dans une autre page
  //cela nous permet d'appeler ce component avec <app-pizza-modify [id]="id"></app-pizza-modify>
  // ou <app-pizza-modify [pizza]="objetPizza"></app-pizza-modify>
  @Input() id?: string;
  @Input() pizza?: Pizza;

  ngOnInit(): void {
    //si on a pas reçu l'objet en entier
    this.getPizza().subscribe((pizza) => {
      this.pizza = pizza;
    });
  }

  getPizza(): Observable<Pizza> {
    if (!this.pizza) {
      if (this.id) {
        //on va le chercher via notre service grâce à son id
        return this.pizzaService.getById(this.id);
      }
    }
    return of(this.pizza!);
  }
```

Ici, les clauses `@Input()` permettent d'indiquer à angular que ce composant accepte des paramètres lors de son _branchement_. De cette façon, un _composant parent_ pourra lui passer un objet `Pizza` ou un `ObjectId` pour l'initialiser.

Il nous suffit pour ça de préciser ces paramètres lors de l'appel du component dans le component parent.

### Initialisation du `FormGroup` et envoi de la modification

La différence majeure entre le formulaire de création et celui d'édition est que celui d'édition sera pré-rempli avec les données de la Pizza à modifier :

```ts
//pizza-modify.component.ts
initForm(pizza: Pizza): FormGroup {
    //on remplit le tableau d'ingrédients avec les ingrédients récupérés de la pizza
    this.formIngredients = pizza.ingredients;
    //on prépare notre formulaire à l'aide de notre formBuilder
    return this.formBuilder.group({
      //on précise chaque champ de notre formulaire
      size: [pizza.size],
      name: [pizza.name],
      base: [pizza.base],
    });
  }
```

Il ne nous reste plus qu'un moyen pour envoyer les modifications, il faut donc implémenter la méthode `edit()` qu'on a prévu d'utiliser dans le formulaire au moment de `submit` :

```ts
//pizza-modify.component.ts
edit(): void {
    if (this.pizzaForm) {
      //pour ajouter notre pizza on récupère les valeurs de formulaire
      const newPizza = this.pizzaForm.value;
      newPizza._id = this.pizza!._id;
      //ajout de chaque nom d'ingrédient dans notre pizza
      newPizza.ingredients = [];
      this.formIngredients.forEach((ingredient) => {
        newPizza.ingredients.push(ingredient.name);
      });

      this.pizzaService.edit(newPizza).subscribe((pizza) => {
        console.log(pizza);
      });
    }
  }
```

Et donc également implémenter la méthode `edit` dans notre service :

```ts
//pizza.service.ts
public edit(toEdit: Pizza): Observable<Pizza> {
    return this.http.put<Pizza>(`${this.url}/${toEdit._id}`, toEdit);
  }
```

## Un composant vue pour nos `Pizza`

De la même façon qu'on a déplacé la logique de modification dans un composant, il faudrait également déplacer la logique de consultation d'une Pizza dans un autre composant. On pourra toujours afficher toutes les instances de `Pizza` sur notre page, et on pourra également mettre en place un _mode d'édition_ à la pression d'un bouton, de façon à pouvoir passer de la consultation à l'édition simplement.

### Création du composant et utilisation dans le `pizza.component` :

On crée notre component

```
ng g c pizza-view
```

et comme pour `pizza-modify` on lui permet de recevoir des paramètres (de quoi afficher une pizza) :

```ts
//pizza-view.component.ts
import { Component, Input, OnInit } from "@angular/core";
import { Pizza } from "../models/Pizza";
import { PizzaService } from "../services/pizza.service";

@Component({
    selector: "app-pizza-view",
    templateUrl: "./pizza-view.component.html",
    styleUrls: ["./pizza-view.component.css"],
})
export class PizzaViewComponent implements OnInit {
    constructor(private pizzaService: PizzaService) {}

    //@Input permet de faire passer un paramètre lors du montage de notre component dans une autre page
    //cela nous permet d'appeler ce component avec <app-pizza-view [id]="id"></app-pizza-view>
    // ou <app-pizza-view [pizza]="objetPizza"></app-pizza-view>
    @Input() id?: string;
    @Input() pizza?: Pizza;
    ngOnInit(): void {
        //si on a pas reçu l'objet en entier
        if (!this.pizza) {
            if (this.id) {
                //on va le chercher via notre service grâce à son id
                this.pizzaService
                    .getById(this.id)
                    .subscribe((pizza) => (this.pizza = pizza));
            }
        }
    }
}
```

```html
<!-- pizza-view.component.html -->
<p>{{pizza.name}}</p>

<p>{{pizza.size}}</p>
<p>{{pizza.base}}</p>

<h2>Ingredients :</h2>
<ul>
    <li *ngFor="let ingredient of pizza.ingredients">{{ingredient.name}}</li>
</ul>
```

## Intégration de nos composants enfants dans notre `pizza.component`

On va désormais demander à notre composant d'utiliser ces nouveaux composants _enfants_.

```html
<div *ngIf="pizze">
    <div *ngFor="let pizza of pizze">
        <button (click)="delete(pizza)" type="button">supprimer pizza</button>

        <app-pizza-view [pizza]="pizza"></app-pizza-view>
    </div>
</div>
```

Cependant, il nous faut un moyen de passer de la `pizza-view` à `pizza-modify` d'un simple bouton.

### Association d'un booléen à chaque `Pizza`

Pour pouvoir déterminer si une instance de pizza est en mode édition ou pas, il faut qu'on puisse associer à chaque objet dans notre tableau un booléen, qu'on appelera `editMode`, qui sera à `false` par défaut, et passera à `true` si on désire éditer.

Implémenter ça demande qu'on modifie légèrement notre façon de stocker nos `Pizza`, il faut qu'on les stocke non pas dans un `Pizza[]`, mais dans un tableau d'objet plus complexe contenant

```ts
{
    pizza: Pizza,
    editMode: boolean
}
```

Donc on modifie notre propriété `pizze`, et on modifie son initialisation pour que le tableau contienne des `Pizza` et des `boolean` :

```ts
//pizza.component.ts
//...
public pizze: { pizza: Pizza; editMode: boolean }[] = [];
//...
ngOnInit(): void {
    this.pizzaService.get().subscribe((pizze: Pizza[]) => {
      this.pizze = pizze.map((pizza) => {
        return { pizza: pizza, editMode: false };
      });
    });
    //...
}
```

Cela nous force à changer quelques façons de faire, comme par exemple dans nos boucles et dans notre manipulation du tableau au moment d'une création de pizza, mais cela nous permet également de gérer facilement l'affichage ou non du composant `pizza-modify`:

```html
<div *ngFor="let pizza of pizze">
    <button (click)="delete(pizza.pizza)" type="button">supprimer pizza</button>
    <!-- toggleEdit inverse juste la valeur de pizza.editMode-->
    <button type="button" (click)="toggleEdit(pizza)">edit</button>
    <!-- on peut utiliser un if/else ici pour déterminer quoi afficher en fonction du booléen editMode-->
    <div *ngIf="pizza.editMode; else pizzaView">
        <!-- ici on accède donc à la pizza grâce à pizza.pizza-->
        <app-pizza-modify [pizza]="pizza.pizza"></app-pizza-modify>
    </div>
    <!-- ng-template permet d'utiliser un bloc else nommé #pizzaView-->
    <ng-template #pizzaView>
        <app-pizza-view [pizza]="pizza.pizza"></app-pizza-view>
    </ng-template>
</div>
```

Désormais, l'appui du bouton nous fera passer de l'affichage au formulaire, et vice-versa.
Chaque _composant enfant_ reçoit les données via son paramètre `[pizza]` et gère son fonctionnement en interne.

## La suite

Il faut maintenant pouvoir faire en sorte de notifier le composant parent qu'une modification a été prise en compte dans le `pizza-modify.component`. En effet, en ayant placé la logique de modification dans le composant enfant, on empêche le parent de savoir _par défaut_ ce que l'enfant fait. Il faut donc trouver un moyen de faire remonter l'information, de façon à ce que, par exemple, la liste en local soit mise à jour sans avoir à refaire une requête. Ou encore que le mode d'édition, géré par le parent, puisse être désactivé automatiquement une fois la modification prise en compte.

Il faudra également tenter de _refactoriser_ le code un peu plus, et transformer, par exemple, notre formulaire en un composant réutilisable.
