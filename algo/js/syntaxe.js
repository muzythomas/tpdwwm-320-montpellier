//VARIABLES
//une variable est un espace de stockage dans lequel on peut assigner une valeur à un nom
//en javascript la déclaration d'une variable se fait à l'aide du mot clé "let"

let nombre = 10; //ici on définit une variable nommée nombre contenant la valeur 10
//en javascript les données sont typées dynamiquement, c'est à dire que 
//javascript définit le type de la donnée selon la donnée stockée
//en c on devrait définir int nombre = 10; désignant un nombre entier
let message = "coucou"; //ici on définit une variable nommée message contenant une chaine de caractères "coucou". une chaîne se trouve toujours entre guillemets ou apostrophes
//pour utiliser les valeurs stockées dans des variables il suffit d'appeler leur nom
let a = 5;
let b = 3;
let c = a + b; // c est égal à 8

//CONDITIONS
//un branchement conditionnel en javascript se fait à l'aide d'un if la plupart du temps 
//une condition se fait sur une comparaison ou n'importe quel opération renvoyant un booléen
//un booléen étant true ou false, 1 ou 0
//pour effectuer des comparaisons le javascript possède différents opérateurs
/*
== égalité
!= différence
> superiorité stricte 
>= supériorité ou égalité 
< inferiorité stricte
<= inferiorité ou égalité
=== égalité stricte
!== différence stricte
*/
let age = 12
if ( age < 18 ) {
    //revenez plus tard
} else {
    //régalez vous
    if ( age > 21) {
        //buvez à ma santé
    }
}

/*
Booléens :
&& et 
true && true === true
true && false === false
false && true === false
false && false === false
true && true && true && false == false

|| ou 
true || true === true
true || false === true
false || false || false || true === true
false || false === false

! non
!true === false
!false === true
!(true && false) === true
!(true || false) === false 
*/

// le switch/case permet de tester la valeur d'une variable
//et d'effectuer une opération en fonction de cas prédéfinis
let legume = "courgette";
switch (legume) {
    case "Courgette":
        //faites une ratatouille
        break;
    case "Tomates":
        //faites une pizza
        break; 
    default: 
        //comportement par défault 
}

// LES BOUCLES
//une boucle permet de répéter un jeu d'instructions selon une certaine condition
//qui est réévaluée à chaque execution d'un tour de boucle 
let i = 1
while (i <= 100){
    console.log('tour numéro ', i)
    i++
}
//le do while permet de s'assurer l'execution d'au moins un jeu d'instruction
do {
    //instructions
} while (i <= 100)

//la boucle for permet d'effectuer des opérations répétées comme le while
//mais la syntaxe permet de définir une condition de départ, une condition d'arrêt
//ainsi que l'opération à effectuer à chaque fin de boucle 
// for (déclaration de départ; condition d'arrêt; changement) {}
for (let i = 1; i <= 100; i++){
    console.log('tour numéro ', i)
    if (i%2 == 0){
        console.log('pair')
    } else {
        console.log('impair')
    }
}

//FONCTIONS
//une fonction est un jeu d'instructions réutilisable qui peut prendre des données en entrée
//pour effectuer un traitement, et renvoyer un résultat
//une procédure est une fonction effectuant un traitement mais n'envoyant de resultat
//la définition d'une fonction se fait via le mot clé function
function direBonjour(){
    console.log('bonjour');
}
//pour appeler une fonction on utilise ensuite le nom défini lors de sa déclaration
direBonjour();

//pour passer des données à une fonction sous forme de paramètres, on les définit ainsi que leur nom entre parenthèses après le nom de la fonction
function addition(a, b){
    //pour renvoyer un résultat on utilise le mot clé return
    return a+b
}
addition(1,2) //renverra 3