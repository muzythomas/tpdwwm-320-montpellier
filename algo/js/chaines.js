//une chaine de caractères est similaire à un tableau de caractères
//on définit généralement une chaine comme du texte contenu entre "" / ''
let exemple = "Coucou";
let exemple2 = 'aujourd\'hui'; //obligés d'utiliser un \ pour échapper le '

//pour concaténer des chaines ensemble on utilise +
let chaine = "ceci est " + 1 + " chaine" //=== "ceci est 1 chaine"

//pour inclure des variables on peut utiliser la concétation ou autre chose

let concat = "Comment ça va " + exemple2 //renvoie "Comment ça va aujourd'hui"
//en utilisant des `` on peut intégrer une variable dans le texte à l'aide de ${variable}
let exemple3 = `Comment ça va ${exemple2} les amis` //renvoie "Comment ça va aujourd'hui les amis"

//pour sauter une ligne dans une chaine on utilise le caractère newline \n
let sautdeligne = "je saute\n une ligne"

//pour changer la casse (maj/minuscule)
//on peut utiliser toUpperCase() et toLowerCase() pour renvoyer des copies transformées
let petitbonjour = "bonjour";
petitbonjour.toUpperCase() //renvoie "BONJOUR"

let groscoucou = "COUCOU";
groscoucou.toLowerCase() // renvoie "coucou"

//la comparaison de chaines prend en compte la casse
let comp = "toto"
comp === "toto" //true
comp === "Toto" //false

//les chaines sont représentées comme des tableaux de caractères
//ce qui veut dire qu'on peut accéder à un caractère en particulier à l'aide d'un indice
//             0  1  2  3  4  5  6
//            [S, a, l, u, t,  , l,  ...]
let phrase = "Salut les copains, comment ça va ?"
phrase[0] // S
phrase[10] //c
phrase[phrase.length - 1] //dernier caractère => "?"

//parcourir une chaine se fait donc de la même façon qu'un tableau
for (let i = 0; i < phrase.length; i++){
    phrase[i] //renvoie chaque caractère
}

//on peut séparer une phrase en sous parties à l'aide de la méthode split()
//split permet de séparer une phrase en sous parties délimitées par un caractère précis
//par exemple pour récupérer et séparer tous les mots d'une phrase:
let mots = phrase.split(" ") //on donne l'espace à split
//                      0        1       2           3        4      5   6
//et mots contiendra ["Salut", "les", "copains,", "comment", "ça", "va", ?]
//                                0      1     2
"06.28.49".split(".") //renvoie ["06", "28", "49"]