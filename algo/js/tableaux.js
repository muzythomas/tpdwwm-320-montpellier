//un tableau est une structure de donnée de taille définie
//permettant de stocker un set de données de même type
//en javascript (et en programmation en général) un tableau est définit par des []
//                      0        1             2
let listeCourses = ['éponges', 'pates', 'sauce tomate'];
//pour accéder aux données stockées dans le tableau on utilise son indice
listeCourses[0] //contient 'éponges'
listeCourses[1] //contient 'pates'
listeCourses[2] //contient 'sauce tomate'
listeCourses[3] //contient undefined

//pour parcourir un tableau, la meilleure méthode est d'utiliser une boucle
//et plus exactement une boucle for 
//listeCourses.length permet de récupérer la taille de notre tableau listeCourses
for (let i = 0; i < listeCourses.length; i++) {
    console.log(listeCourses[i]); //pour chaque tour de boucle on affichera une case du tableau
}

//pour ajouter un élément à la fin d'un ta bleau on utilise
//tableau.push()
listeCourses.push("sel"); // ['éponges', 'pates', 'sauce tomate', 'sel']
listeCourses[3] //renvoie 'sel'

//pour ajouter un élément au début de notre tableau
//on utilise tableau.unshift()
listeCourses.unshift('Whisky'); // ['Whisky', 'éponges', 'pates', 'sauce tomate', 'sel']

//tableau.pop() permet de retirer et renvoyer le dernier élement d'un tableau
listeCourses.pop() //renvoie 'sel' ['Whisky', 'éponges', 'pates', 'sauce tomate']
//tableau.shift() effectue la même opération mais au début du tableau
listeCourses.shift() //renvoie 'Whisky' ['éponges', 'pates', 'sauce tomate']

//tableau.splice() permet de raccorder, supprimer et remplacer des éléments dans un tableau
//supprime 1 élément à partir de la case 1 et le remplace par d'autres éléments 
listeCourses.splice(1, 1, 'pates completes', 'linguine') 
// ['éponges', 'pates completes', 'linguine', 'sauce tomate']

//tableau.slice() récupère une partie du tableau 
//en faisant une copie des éléments à partir d'une borne inférieure (incluse)
//jusqu'à une borne supérieure (exclue)
listeCourses.slice(0, 3) // ['éponges', 'pates completes', 'linguine']