# ETAPE 1
## *Création du Projet*

Prérequis : 
* PHP (^7.1) 
* [Symfony](https://symfony.com/download)
* [Composer](https://getcomposer.org)
* Un serveur de bases de données (ex: Mariadb)

Pour créer un projet Symfony, utiliser la commande symfony new dans un terminal

```
symfony new nomDuProjet --full
```

`--full` signifie qu'on veut créer un projet web complet 

Pour spécifier une version particulière on utilise `--version`
La commande suivante créera donc un projet en version 4.4 de Symfony

```
symfony new nomDuProjet --full --version=4.4
```

# ETAPE 2 
## *Lancer le serveur et paramétrer l'application*

### Lancement du serveur : 
Un dossier du nom du projet est créé avec le projet, contenant tous les fichiers nécessaires à l'utilisation de symfony. 
Une fois 
Pour tester l'application, on peut lancer le serveur avec la commande :

``` 
symfony server:start 
```

Une fois l'adresse `127.0.0.1:8000` atteinte dans le navigateur, cet écran ou une variation de cet écran devrait s'afficher : 

![Ecran Welcome Symfony](https://symfony.com/uploads/assets/blog/new-symfony-welcome-page.png)

### Paramétrer l'environnement d'application : 

Certains paramètres d'application tels que l'accès à la base de données sont spécifiés dans un fichier `.env` à la racine du projet.

À la ligne 32 se trouve la chaîne suivante : 
```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
```

Dans cette chaîne on doit préciser les identifiants et paramètres correspondants à notre base de données. 

Pour mariadb, la clé `serverVersion` devait posséder comme valeur `mariadb-x`, `x` étant le numéro de la version de mariadb utilisée. Il est possible d'obtenir cette version de mariadb sur votre page d'accueil phpmyadmin.

Si une base de données n'existe pas déjà, on renseigne les informations de la même façon, mais il faut ensuite demander à l'ORM Doctrine de créer la base pour nous via SQL, avec la commande suivante : 

```
php bin/console doctrine:database:create
```

# ETAPE 3
## Développer en MVC 


Symfony est un framework MVC, le développement de site se fait donc via le principe de Modèle Vue Controleur. 
Tout le code source (modèle, controleur, etc...) est stocké dans le dossier `src` du projet. Toutes les vues sont stockées dans le dossier `templates`. 


### Mise en place du Modèle : 

Une première étape possible au développement de notre application pourrait être de créer nos entités. 
Pour ce faire, on peut utiliser le maker bundle et la commande `make:entity`.

```  
php bin/console make:entity NomEntité
```

Cette commande fera apparaître un utilitaire de crétaion d'entités Symfony, qui vous posera des questions sur les différentes propriétés de votre Entité. 
Une fois l'entité (la classe) créée, elle sera stockée dans `src/Entity/NomEntité.php`.
Un `Repository`, chargé d'effectuer les requêtes en BDD via Doctrine, sera également créé dans `src/Repository/NomEntitéRepository.php`.

Après avoir créé autant d'Entités que nécessaire au fonctionnement du projet, on peut demander à Doctrine de synchroniser le Modèle du projet avec la bdd relationnelle via une migration.

```
php bin/console make:migration
```

Une migration générée par la commande make:migration représente une collection de requêtes SQL visant à reproduire votre Modèle en tant que schéma SQL. 
Pour exécuter les migrations, et donc créer le schéma relationnel en base, il faut lancer la commande suivante : 

```
php bin/console doctrine:migrations:migrate
```

### Mise en place des controleurs 

Pour génerer une page dans Symfony, il faut passer par un controller. Un controller est une classe particulière contenant des méthodes visant à génerer des pages en HTML.
Ces méthodes de controller sont appelées à chaque fois qu'une requête est effectuée sur l'application. 

Pour créer un Controller on peut utiliser 

```
php bin/console make:controller NomController
```

Un nom de controller doit toujours être écrit en UpperCamelCase comme pour les Entités, mais doit toujours se terminer par Controller. 

Une fois un Controller créé, on peut commencer à coder les diverses fonctionnalités.

En général, un Controller est créé par "fonctionnallité" du site. 
Par exemple pour un Blog, on pourrait avoir ArticleController pour la gestion des Articles (affichage, modification, publication...), 
AdminController pour l'interface administrateur, UserController pour la page de configuration de l'utilisateur... etc 


### Utilisation des templates

Chaque page découle d'un rendu en HTML d'un template `twig`.
Pour effectuer le rendu d'un template `twig` dans une méthode de Controller on utilise la méthode `render`. 

Les templates sont écrits à l'aide de [twig](https://twig.symfony.com). 

Les templates `twig` peuvent exécuter des logiques très simples (conditions, blocs) et intégrer des variables venant du Controller ou de l'application en général. 

# ETAPE 4 
## Construire l'application Web 

Pour mettre en place l'application, il faut faire correspondre via des `routes` des `url` avec des méthodes de Controller. 

Ces méthodes exécuteront la logique de l'application avant de renvoyer une page en réponse de la requête. 

# Infos Supplémentaires 

Certains composants de l'application peuvent être générés de la même façon que les entités et les contrôleurs, comme par exemple les `User` de type `UserInterface` permettant de gérer facilement l'authentification 

```
php bin/console make:user
```

Cette commande créera une `App\Entity\User` avec des dispositifs permettant la connexion/déconnexion/inscription et manipulation générale des utilisateurs.

``` 
php bin/console make:auth
```

Cette commande ci permettra quant à elle de générer un `authenticator`, qu'il soit personnalisé ou via formulaire de connexion, permettant ainsi de créer une fonctionnalité d'authentification d'utilisateur.

```
php bin/console make:registration-form
``` 

Quant à cette commande, elle permet de créer un formulaire d'inscription qui sera rattaché aux mêmes mécanismes de gestion d'utilisateurs que les commandes précédentes. 

Il existe bien d'autres composants et commandes disponibles, pour en savoir plus, se diriger vers la [documentation](https://symfony.com/doc/4.4/index.html) correspondant à votre version de Symfony.

## Liens utiles : 
* [Créer sa première page](https://symfony.com/doc/4.4/page_creation.html)
* [Créer une entité via Doctrine](https://symfony.com/doc/4.4/doctrine.html#creating-an-entity-class)
* [Formulaires](https://symfony.com/doc/4.4/forms.html)
* [Validation de Formulaires](https://symfony.com/doc/4.4/validation.html)
* [Module Security](https://symfony.com/doc/4.4/security.html)

## Bundles Utiles
* [VichUploaderBundle](https://github.com/dustin10/VichUploaderBundle/blob/master/docs/usage.md)
* [EasyAdmin v2](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/installation.html)
* [EasyAdmin v3](https://symfony.com/doc/master/bundles/EasyAdminBundle/index.html)
* [HtmlPurifierBundle](https://github.com/Exercise/HTMLPurifierBundle)