# Télécharger des fichiers à l'aide de VichUploaderBundle

## 1 - Installer VichUploaderBundle

[VichUploaderBundle](https://github.com/dustin10/VichUploaderBundle/blob/master/docs/index.md) est un bundle permettant de gérer le téléchargement de fichiers liés à une entité facilement. 
Pour l'installer il suffit d'utiliser composer : 

```
composer require vich/uploader-bundle
```

Une fois le bundle installé, il faut ensuite le configurer.

## 2 - Utiliser VichUpoaderBundle

## 2a - Le cas OneToOne 

Dans le cas où on souhaite lier une entité à une image (typiquement le cas d'un avatar d'utilisateur par exemple), la configuration de VichUploaderBundle est très simple. 

D'abord, il faut enregistrer ce qu'on appelle un `mapping` c'est à dire des instructions pour l'enregistrement des fichiers à télécharger. 
Ce `mapping`, écrit dans le fichier de configuration `config/packages/vich_uploader.yaml`, contient entre autres le chemin de destination de notre fichier après téléchargement.

Par défaut, `vich_uploader.yaml` contient les lignes suivantes : 

```yaml
vich_uploader:
    db_driver: orm
```

Il nous suffit de rajouter  :

```yaml
vich_uploader:
    db_driver: orm
    mappings:
        user_images:
            uri_prefix: /images/users
            upload_destination: '%kernelproject_dir%/public/images/users'
            namer: Vich\UploaderBundle\Naming\SmartUniqueamer
```

Le `mapping` contient donc un nom : `user_images`
Un prefixe uri `uri_prefix`, une destination de téléchargement `upload_destination`, et un `namer` chargé de nommer les fichiers automatiquement de façon unique.  

Ensuite, il faut lier notre `mapping` à notre entité. Ici, l'entité `User`.

Dans l'entité `User`, pour indiquer à VichUploader qu'on souhaite qu'il s'occupe de l'upload de fichier on va y ajouter les propriétés visant à stocker les informations de l'image, ainsi que des annotations spécifiques. 

On doit annoter notre entité avec l'annotation `@Vich\Uploadable` :

```php
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Vich\Uploadable
 */
class User implements UserInterface {
    ###corps de la classe
}
```

L'entité `User` a besoin pour gérer l'upload d'images d'une propriété stockant le nom de l'image et d'une propriété stockant le fichier de l'image. Seul le nom de l'image sera persisté via Doctrine, et donc enregistré en base. 

```php
/**
 * @ORM\Column(type="string", length=255)
 */
private $imageName;

/**
 * @Vich\UploadableField(mapping="user_images", fileNameProperty="imageName")
 */
private $imageFile;
```
 
`$imageFile` est affublée de l'annotation `@Vich\UploadableField` pour indiquer à VichUploader où doit être envoyé le fichier en téléchargement. Dans les paramètres de cette annotation on retrouve le nom de notre mapping permettant d'envoyer le fichier au bon endroit. On y indique également la propriété `$imageName` pour que Vich sache ou enregistrer le nom du fichier. 

Pour `$imageFile`, le setter est légèrement différent qu'à l'accoutumée, dû au lien entre VichUploaderBundle et Doctrine : 

```php
//?string permet d'indiquer qu'on peut ne rien recevoir, et laisser vich se charger du nom plus tard 
public function setImageName(?string $imageName): self
{
    $this->imageName = $imageName;

    return $this;
}

public function setImageFile(?File $imageFile){
    $this->imageFile = $imageFile;

    //pour que vichuploaderbundle soit bien appelé en cas de modification d'une image
    if ($imageFile){
        //il faut changer quelque chose dans l'entité, ici on a ajouté une propriété updatedAt pour pouvoir déclencher le fonctionnement de Doctrine
        $this->setUpdatedAt(new \DateTime);
    }
    return $this;
}
```

### L'upload de fichier via un Formulaire

L'upload de fichier se fait comme d'habitude via un champ File, mais VichUploaderBundle possède également deux FormTypes supplémentaires : `VichImageType` et `VichFileType` qui sont facultatifs.

Dans notre `FormBuilder` il suffit donc de rajouter par exemple : 
```php
->add('imageFile', VichImageType::class)
```

### Upload de fichier via EasyAdmin

Dans le [cas d'EasyAdmin](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/integration/vichuploaderbundle.html) il faut également paramétrer le champ de formulaire pour accepter un upload d'image. EasyAdmin possède également du css particulier pour VichUploaderBundle de façon à ne pas "casser" le style général du back-office. 

Par exemple pour notre `User` dans `config/packages/easy_admin.yaml` : 

```yaml
form:
    fields:
      # reste des champs de formulaire 
      - {property: imageFile, type: vich_image}
```

### Affichage de l'image dans un template 

Pour afficher une image dans un template twig, il suffit d'utiliser `asset` ou `vich_uploader_asset`.

`asset` nécessite d'indiquer le chemin complet de notre image et d'y concaténer le nom de l'image, tandis que `vich_uploader_asset` permet de donner l'entité contenant l'image et il en retrouvera le chemin tout seul. 

Avec `asset` : 
```html
<img src="{{ asset('images/uploads/users/' ~ user.imageName ) }}" alt="{{user.username}} profile picture">
```

Avec `vich_uploader_asset` : 
```html
<img src="{{ vich_uploader_asset(user, 'imageFile')}}" alt="{{user.username}} profile picture">
```

## 2b - Le Cas ManyToOne

Dans le cas où plusieurs fichiers doivent être liés à une même entité, on doit séparer l'entité visée de l'entité qui se chargera du fichier à télécharger. 
On peut créer par exemple dans le cas d'une entité `Advert` qui aurait plusieurs `images`, une entité `AdvertImage` qui contiendrait le code concernant VichUploaderBundle : 

```php
<?php

namespace App\Entity;

use App\Repository\AdvertImageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=AdvertImageRepository::class)
 * @Vich\Uploadable
 */
class AdvertImage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Vich\UploadableField(mapping="advert_images", fileNameProperty="name")
     */
    private $file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Advert::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private $advert;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file)
    {
        $this->file = $file;
        if ($file){
            $this->updatedAt = new \DateTime();
        }

        return $this;
    }

    public function getAdvert(): ?Advert
    {
        return $this->advert;
    }

    public function setAdvert(?Advert $advert): self
    {
        $this->advert = $advert;

        return $this;
    }
}
```

Dans cette entité, on retrouve les mêmes annotations et les mêmes propriétés que dans notre `User` avec son `imageFile`.
La différence est qu'on a également une relation avec notre `Advert`, relation `ManyToOne` qui est inversée dans `Advert` grâce à Doctrine : 

```php
##App/Entity/Advert.php : 
/**
     * @ORM\OneToMany(targetEntity=AdvertImage::class, mappedBy="advert", orphanRemoval=true, cascade={"persist"})
     */
    private $images;

    /**
     * @return Collection|AdvertImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(AdvertImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setAdvert($this);
        }

        return $this;
    }

    public function removeImage(AdvertImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getAdvert() === $this) {
                $image->setAdvert(null);
            }
        }

        return $this;
    }
```

On précise `cascade={"persist"}` dans l'annotation `@ORM\OneToMany` pour enregistrer automatiquement chaque image liée à notre `Advert` via Doctrine lorsqu'elles sont téléchargées. 


### Upload des fichiers via un Formulaire

Pour pouvoir télécharger plusieurs fichiers via notre Formulaire `AdvertType` par exemple, il faut un moyen de rajouter dynamiquement plus ou moins de champs `FileType` (ou `VichFileType` ou `VichImageType`). 
Pour ce faire, il faut préparer notre formulaire à recevoir zéro ou n fois le même champ de formulaire. 

On doit donc créer un champ de formulaire qui permettra l'upload de fichier dans notre `AdvertImage`, puis le lier à un `CollectionType` dans notre `AdvertType`. 

Dans un `App\Form\AdvertImageType` : 
```php
namespace App\Form;

use App\Entity\AdvertImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AdvertImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('file', VichImageType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        //nécessaire pour envoyer l'image dans AdvertImage et pas dans Advert
        $resolver->setDefaults(
            [
                'data_class' => AdvertImage::class
            ]
        );
    }
}
```

Dans notre `AdvertType` il faut maintenant paramétrer la collection de champs `AdvertImageType` : 

```php
# dans AdvertType::buildForm() : 
->add('images', CollectionType::class, [
    'entry_type' => AdvertImageType::class ,
    'allow_add' => true,
    'allow_delete' => true,
    'by_reference' => false    
])
```

[CollectionType](https://symfony.com/doc/current/reference/forms/types/collection.html) possède plusieurs options : 

* `entry_type` indique quel `FormType` sera utilisé
* `allow_add` et `allow_delete` permettent d'ajouter + ou - de FormType à la volée 
*  `by_reference` mis à `false` permet d'assurer que le setter `AdvertImage::setAdvert()` ici soit appelé à tous les coups, et donc que la relation soit faite automatiquement. 

Cette configuration, bien que suffisante pour gérer la logique de téléchargement ne convient pas entièrement pour ce qui est de l'affichage de notre formulaire. 
En effet, `CollectionType` est vide par défaut, et il faut donc utiliser du javascript pour rajouter/enlever des `AdvertImageType` dans notre formulaire : 


```html
<div class="form_container">
		{{form_start(create_form)}}
            {{form_row(create_form.title)}}
            {{form_row(create_form.description)}}
            {{form_row(create_form.price)}}
            {{form_row(create_form.isToBeDiscussed)}}
            {{form_row(create_form.category)}}
            {{form_row(create_form.images)}}
            <div id="advert_image_fields">
            </div>
            <a href="#" id="add_image">+ Add Image</a>
            {{form_row(create_form.submit)}}
        {{form_end(create_form)}}

	</div>

    <script type="text/javascript">
        let addImage = document.getElementById('add_image');
        addImage.addEventListener('click', (event) => {
            let advertImages = document.getElementById('advert_images');
            let prototype = advertImages.getAttribute('data-prototype'); //on récupère le HTML correspondant à notre champ de formulaire
            
            let advertImagesFields = document.getElementById('advert_image_fields');
            let imageFieldsCount = advertImagesFields.childElementCount; 

            prototype = prototype.replace(/__name__/g, imageFieldsCount); //on remplace tous les __name__ dans le html du champ de formulaire par un numéro
            prototype = prototype.replace(/\d+label__/g, `Image ${imageFieldsCount + 1}`); //on remplace tous les nlabel__ par quelque chose de lisible
            
            //on transforme le texte HTML en éléments du DOM pour insertion 
            let formField = new DOMParser().parseFromString(prototype, 'text/html').body.firstChild;

            //on ajoute ensuite notre morceau de formulaire généré à notre div
            advertImagesFields.appendChild(formField);

        });
    </script>
```

Le javascript permet d'ajouter à la volée des nouveaux champs qui sont envoyés par le formulaire dans un attribut `data-prototype` dans une div `advert_images` générée par le formulaire Symfony. 


### Upload des fichiers via EasyAdmin

Dans `easy_admin.yaml` on ajoute une ligne dans notre paramétrage de formulaires : 

```yaml
- {property: images, type: collection, type_options: {entry_type: App\Form\AdvertImageType, allow_add: true, allow_delete: true, by_reference: false}}
```

### Affichage dans un template

Dans un template, on affiche chaque image individuellement de la même façon qu'auparavant (`asset` ou `vich_uploader_asset`). 

```html
<div class="advert-images">
{% for image in advert.images %}
	<img src="{{vich_uploader_asset(image, 'file')}}" alt="{{advert.title}} image">
{% endfor %}
</div>
```