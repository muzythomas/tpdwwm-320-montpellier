# EasyAdmin2, Installation et Configuration

## Etape 0 
Lire la [documentation](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/index.html). Dans l'ordre de préférence. 

## Etape 1 - Installation

Pour installer la version 2.* de EasyAdmin, il faut préciser la version lors de la commande `composer require`.

```
composer require easycorp/easyadmin-bundle:2.*
```

Grâce à Symfony Flex deux fichiers ont été créés par cette commande : `config/packages/easy_admin.yaml` et `config/routes/easy_admin.yaml`.
Le premier sert à configurer EasyAdmin2, le second sert à configurer la route menant au backend. 

Pour vérifier que la route `easyadmin` ait bien été implémentée, on peut utiliser : 
```
php bin/console debug:router
```

qui devrait afficher en fin de liste le paramètre de route suivant  :

```
 easyadmin   ANY      ANY      ANY    /admin/
```  

Une fois cette étape extrêmement compliquée surmontée, on peut passer à l'étape suivante.

## ETAPE 2 - CONFIGURATION 
### CONFIGURATION BASIQUE

Dans le fichier `config/packages/easy_admin.yaml`, il suffit désormais de rajouter les entités qui doivent être suivies par EasyAdmin pour qu'il tente d'établir une première interface d'administration. 
Pour certaines entités, un degré de personalisation supplémentaire pourrait être voulu/nécessaire. 


Attention, dans un fichier yaml, l'indentation est cruciale car déterminant la hierarchie entre les différents éléments de configuration. Ainsi, deux éléments au même niveau d'indentation possèdent le même niveau hiérarchique, etc. 

Dans notre `packages/easy_admin.yaml` on a donc écrit, __PAR EXEMPLE__, pour un site possédant des entités Advert, Category et User : 
```yaml
easy_admin:
  entities:
    - App\Entity\Advert
    - App\Entity\Category
    - App\Entity\User
```

En suivant notre url `/admin` on arrive enfin sur notre backend sommairement configuré. 

### PERSONNALISATION DES ECRANS

Il est possible de personnaliser quels champs vont être disponibles, et sous quel format, dans chaque écran de EasyAdmin, que ce soit l'écran `list` ([documentation](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/list-search-show-configuration.html)), affichant la liste des entités d'une même classe, ou l'écran `edit`/`new` ([documentation](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/edit-new-configuration.html)) et leurs formulaires, etc.

Pour ce faire, dans `packages/easy_admin.yaml` on peut affiner la personnalisation de nos entités, et des fonctions d'administration associées, de la façon suivante : 


```yaml
easy_admin:
  entities:
    Advert: 
      class: App\Entity\Advert
    Category: 
      class: App\Entity\Category
    User: 
      class: App\Entity\User
```

Ici on définit des catégories dans notre panneau d'administration, et on leur associe des entités à manipuler. 
Pour l'instant, le résultat est le même que dans la configuration précédente, mais cette organisation en catégories nous permet d'affiner la personnalisation de nos écrans. 

### Personnalisation des listes

La personnalisation des champs affichés dans l'écran `list` peut se faire de cette façon : 

```yaml
User: 
      class: App\Entity\User
      list: 
        fields: 
          - username
          - email
          - adverts
          - createdOn
          - lastSeenOn
          - roles
```

Ainsi on décide d'afficher certains champs et pas d'autres, de réorganiser leur ordre d'affichage. Pour `User` par exemple, on affiche désormais les `roles` qui n'étaient pas affichés par défaut. On a également supprimé `id` des colonnes affichées. 

Pour personnaliser encore plus les champs, comme changer l'étiquette d'une colonne, on peut ensuite utiliser la syntaxe suivante:

```yaml
User: 
      class: App\Entity\User
      list: 
        fields: 
          - {property: username, label: 'Nickname'}
          - {property: email, label: 'Contact'}
          - adverts
          - createdOn
          - lastSeenOn
          - roles
```

Certains paramètres cependant sont globaux dans EasyAdmin, comme le format de date. Pour ça, il faut définir un format à la "racine" de la configuration EasyAdmin : 

```yaml
easy_admin:
    formats: 
        datetime: 'd/m/Y' 
```

Pour individualiser ce genre de configuration pour un champ particulier, on peut utiliser une syntaxe similaire à notre utilisation de `label` : 
```yaml
User: 
      class: App\Entity\User
      list: 
        fields: 
          - {property: username, label: 'Nickname'}
          - {property: email, label: 'Contact'}
          - adverts
          - {property: createdOn, format: 'd/m/Y H:i:s'}
          - lastSeenOn
          - roles
```

Ainsi, la colonne `createdOn` de `User` sera affichée au format jour/mois/année heure:minute:seconde au lieu de juste jour/mois/année, contrairement au reste de l'application. 

### Personnalisation des formulaires 

Pour personnaliser les formulaires `edit` et `new` de EasyAdmin, la même logique que pour les champs de `list` s'applique. Derrière une clé `form` pour personnaliser tous les formulaires, ou derrière une clé `edit` ou `new` pour personnaliser un formulaire en particulier, on peut définir quels sont les champs qui apparaitront, et selon quels paramètres. 

```yaml
Advert: 
    ##reste de la configuration ici 
    form: 
        fields:
          - title
          - {property: price, type: money, type_options: {divisor: 100, currency: 'EUR'}}
          - location
          - {property: description, type: text_editor}
          - category 
          - isToBeDiscussed
```

Ici on a donc personnalisé notre formulaire pour n'afficher qu'un sous ensemble des propriétés à modifier, et on a également paramétré certains des champs comme `price`, et `description` pour modifier leur comportement. `price` est devenu de type `MoneyType` ([documentation](https://symfony.com/doc/current/reference/forms/types/money.html)), avec ses paramètres `divisor` et `currency`, et description est modifiable avec un éditeur de texte simple. 

Le formulaire ainsi modifié sera présent dans `edit` et `new`. Cependant, étant donné que `new` a besoin de données spécifiques pour la création d'une nouvelle entité, il faudra se tourner vers une personnalisation de Controller pour pouvoir persister de nouvelles entités via EasyAdmin. 

### Personnaliser un template EasyAdmin

Pour revenir sur le `price`, qui dans le formulaire utilise une option `divisor` permettant de passer un prix en centimes stocké en `integer` en un prix en Euros par exemple, il faut également s'occuper de l'afficher en Euros et non pas en centimes d'Euros. 
Un MoneyType n'existant pas pour l'affichage des champs dans `list`, il faut modifier le template d'affichage du champ à la main. 
Bien entendu, modifier le fichier contenu dans `vendor` est hors de question, on va donc sur-implémenter le template d'EasyAdmin avec un des notres. 
Pour ce faire, il suffit de créer un fichier dans notre dossier `templates` et de l'assigner à notre champ `price` dans la personnalisation de `list`. 

On crée donc un fichier `templates/easy_admin/money.html.twig` qui contiendra le format d'affichage de notre champ `price`, ou tout autre champ visant à représenter une valeur monétaire dans EasyAdmin. 

Dans notre template, on peut accéder à la valeur à afficher à l'aide de `value` et des options de notre champ à l'aide de `field_options.nom_de_l'option`.
Une implémentation basique de notre template serait donc : 

```js
{{(value/field_options.divisor) | number_format(2, ',', ' ')}}€
```
Cette implémentation d'affichage permettrait de diviser la valeur à l'aide d'une option `divisor` puis de formater le prix au format Français. 
Il n'y a plus qu'à assigner notre template à notre champ de la manière suivante : 

```yaml
Advert: 
    list:
        fields:
            - {property: price, divisor: 100, template: easy_admin/money.html.twig}
```

## PERSONNALISATION DU ADMINCONTROLLER

Notre back-office est paramétré aux petits oignons cependant, lors de la création d'une nouvelle entité (action `new` de EasyAdmin), une erreur peut survenir de la part de SQL nous disant que certaines colonnes n'ont pas été précisées, même en ayant été paramétrées avec `NOT NULL`. 

Dans l'exemple de nos `Adverts` il peut s'agir de la propriété `createdOn`, qui devrait être remplie automatiquement à la création de l'entité, mais qui ne l'est pas. 

Pour implémenter ce comportement dans EasyAdmin, il faut effectuer une surcharge du contrôleur EasyAdmin afin d'y insérer le traitement des propriétés non gérées par notre formulaire. 
La [documentation](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/complex-dynamic-backends.html) contient des informations sur comment concrétiser cette entreprise. 

### Surcharge d'un contrôleur 

Pour surcharger le contrôleur par défaut d'EasyAdmin avec nos propres comportements de méthodes, il suffit de créer un contrôleur (appelé `AdminController` par exemple) et de l'assigner à easy_admin dans `routes/easy_admin.yaml`, pour que la route easyadmin mène à notre contrôleur, au lieu de celui par défaut. 

On crée donc un controller `Controller\AdminController.php` qui va hériter de `EasyAdminController` : 

```php
<?php

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class AdminController extends EasyAdminController{
    //TODO: changer le comportement de certaines actions du back-office
} 
```

Ensuite, il faut surcharger les actions de EasyAdmin de façon à pouvoir modifier les propriétés (comme `createdOn` ou `seller`) qui devraient être modifiées automatiquement. 

Plusieurs méthodes sont surchargeables, comme `listAction()` qui est la méthode se chargeant d'afficher toutes les entités d'une même classe,
ou encore `persistEntity()` (qui nous intéresse) qui se charge de persister via Doctrine une entité créée via EasyAdmin. 

Il nous suffit donc d'implémenter une méthode `persistEntity()` concernant nos `Adverts`. 

Pour nous aider à séparer le comportement adapté à chaque Entité, EasyAdmin nous permet de nommer des méthodes `persist<NomDeL'entité>Entity()`, ce qui donnerait pour nos Adverts `persistAdvertEntity()`. Ces méthodes seront appelées automatiquement au moment où une Entité du nom choisi sera créée. 

Notre méthode `persistAdvertEntity()` pourrait donc ressembler à ça : 

```php
public function persistAdvertEntity(Advert $advert){
    //on traite les données nécessaires 
    $advert->setCreatedOn(new \DateTime);
    //assigne l'utilisateur connecté comme auteur de l'annonce
    $advert->setSeller($this->getUser());
    
    //on appelle persistEntity($entity) de EasyAdminController pour lui laisser terminer le boulot
    parent::persistEntity($advert);
}
```

Une fois notre `AdminController` écrit, il faut qu'on dise à EasyAdmin de l'utiliser. Dans `config/routes/easy_admin.yaml` : 

```yaml
easy_admin_bundle:
    resource: 'App\Controller\AdminController'
```

La clé `resource` ciblant précédemment `@EasyAdminBundle/Controller/EasyAdminController.php` cible désormais notre `AdminController`, qui ne change le comportement que de certaines actions, et laisse faire le reste par le contrôleur par défaut. 

### Hacher des mots de passe dans notre AdminController

Pour la création d'utilisateur, on doit forcément hacher nos mots de passes avant de les stocker. 
Pour ça, on utilise le même principe que précédemment, et on implémente donc une méthode `persistUserEntity()` dans notre `AdminController`. 
Cependant, pour accéder aux fonctionnalités de `UserPasswordEncoder` fournies par Symfony, il faut appeller une `UserPasswordEncoderInterface` par injection de dépendance. 

```php
class AdminController extends EasyAdminController{
    //on prépare un propriété pour stocker l'UserPasswordEncoder
    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        //on y stocke l'encoder récupéré par injection de dépendances
        $this->encoder = $encoder;
    }

    public function persistUserEntity(User $user){
        $plainPassword = $user->getPassword();
        //on peut ensuite utiliser encodePassword() via notre encoder
        $user->setPassword($this->encoder->encodePassword($user, $plainPassword));

        $user->setCreatedOn(new \DateTime());

        parent::persistEntity($user);
    }
}
```

## Personnalisations supplémentaires

Bien évidemment, il reste encore beaucoup de points du back-office qui sont personnalisables. Du titre de notre panneau admin, aux icones et menus, ainsi que le css de chaque petit composant, tout est modifiable. 
Pour ce faire, n'hésitez pas à jeter un coup d'oeil à ce truc trouvé par terre à carrefour, la [documentation](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/index.html). 

Dans la documentation, on peut trouver entre autres des infos du style : 
* [Chapitre 4 - Design - Ajouter des ressources de style](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/design-configuration.html#adding-custom-web-assets)
* [Chapitre 7 - Configurer des actions](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/actions-configuration.html)
* [Chapitre 8 - Configurer le menu](https://symfony.com/doc/2.x/bundles/EasyAdminBundle/book/menu-configuration.html)

Et bien plus encore. 


## Nota Bene : Sur text_editor et HTMLPurifier

Lorsqu'un texte est modifié via Trix Editor (type `text_editor`) ou [CKEditorBundle](https://symfony.com/doc/current/bundles/EasyAdminBundle/integration/ivoryckeditorbundle.html) il génère du texte formaté en HTML pour être inséré directement la page avec toutes les options de formatage (gras, italique, liens...). Cependant, par défaut, twig échappe (et encore heureux) toutes balises HTML de façon à [éviter les attaques XSS (Cross Site Scripting)](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html). 

Étant donné que ces balises HTML seront échappées, au lieu de voir **notre texte** on verra \<strong> notre texte \</strong>. 

Une solution souvent proposée est d'utiliser le filtre `raw` (qui laisse passer toute donnée sans l'échapper) de twig. Il ne faut **JAMAIS** faire ça avec des données venant d'utilisateurs au risque d'ouvrir son site à des attaques catastrophiques. 

Une solution est d'utiliser un bundle qui se charge d'échapper tous les caractères et balises problématiques d'un texte formaté en HTML pour ne garder que les balises innoffensives (comme strong ou em, ou encore div). [HTMLPurifier](https://github.com/Exercise/HTMLPurifierBundle) fait exactement ça. 


Une fois installé à l'aide de 
```
composer require exercise/htmlpurifier-bundle
```
il ne nous reste plus qu'à l'utiliser dans twig à l'aide du filtre `purify`. 

```js
{{ texte | purify }}
```
va "purifier" notre texte en laissant passer ce qui n'est pas dangereux. 