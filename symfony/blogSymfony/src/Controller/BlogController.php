<?php

//Pour placer ce fichier de controleur dans le contexte de l'application Symfony
//on utilise namespace (espace de nom) pour indiquer l'emplacement de ce fichier dans l'appli
namespace App\Controller; //on précise que ce fichier s'execute en tant que controller au niveau de symfony

use App\Entity\Article;
//pour pouvoir utiliser certaines fonctionnalité du framework, il faudra hériter de certaines classes abstraites déjà écrites dans Symfony
//pour ça on utilisera extends lors de la déclaration de classe 
//mais pour les importer, au lieu d'utiliser include, on utilise use pour dire qu'on veut utiliser un fichier en particulier
//pour créer un controller, il faut que notre class hérite de AbstractController contenu dans les fichiers du framework
//ainsi Symfony sera sûr qu'on a bien un controller dans notre fichier BlogController.php
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
//pour pouvoir utiliser les annotations @Route pour définir nos routes 
//on doit importer le composant suivant
use Symfony\Component\Routing\Annotation\Route;

//on crée enfin notre BlogController sous la forme d'une classe héritant de AbstractController
class BlogController extends AbstractController {

    //une Route définit une URL pointant vers une page, et donc une fonction de notre controller
    //pour définir une Route dans Symfony, une des méthode est d'utiliser une annotation
    //a une route, on associe une fonction pour définir ce qui se passera lorsque cette route sera consultée
    /**
     * @Route("/", name="blog_index")
     */
    public function index(){
        //une fonction de controller doit renvoyer une réponse HTTP, qui sera transmise au navigateur ayant effectué la requête
        //Typiquement, dans un site web, la réponse sera sous forme de texte formaté en HTML
        
        //sur notre page d'accueil, on veut afficher les articles stockés dans notre application
        //pour pouvoir aller chercher un certain type de données, il faut demander à Doctrine et plus particulièrement
        //au Repository
        //Le Repository est le "dépôt" de requêtes propres à un type de donnée 
        //Par exemple ici, on va appeler l'ArticleRepository pour pouvoir récupérer des Articles
        $articleRepository = $this->getDoctrine()->getRepository(Article::class); //::class permet d'obtenir l'identifiant unique d'une classe pour permettre de l'instancier 
        //on demande ensuite à l'ArticleRepository d'aller chercher nos articles pour nous
        $articles = $articleRepository->findAll();
        
        //on fait ensuite passer notre tableau d'Article à notre vue
        return $this->render('blog/index.html.twig', ['articles' => $articles]);
    }

    //on peut faire passer un paramètre dans une URL
    //en précisant entre {} le nom d'une variable
    /**
     * @Route("/article/{id}", name="blog_article")
     */
    public function article($id){
        //on fait passer ensuite cette variable dans les paramètres de notre fonction

        //ensuite on récupère encore le repository
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);

        //pour aller chercher une entité en particulier
        //on utilise find avec un paramètre, son id par défaut
        $article = $articleRepository->find($id);

        return $this->render('blog/article.html.twig', ['article' => $article]);
    }

    /**
     * @Route("/add", name="blog_add")
     */
    public function add(Request $request){

        //pour refuser l'accès à cette fonctionnalité du site aux utilisateurs non connectés
        //on utilise denyAccessUnlessGranted qui, sur la base des rôles, redirige les utilisateurs vers la page de connexion
        //Plusieurs rôles existent déjà de base pour gérer les connexions
        //IS_AUTHENTICATED, IS_AUTHENTICATED_FULLY, IS_AUTHENTICATED_REMEMBERED
        //qui gèrent l'auth anonyme, l'auth par login/mdp, et l'auth par cookies
        //On peut également utiliser ROLE_USER qui est le rôle que tous les utilisateurs connectés ont
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED'); //refuse l'accès aux utilisateurs non connectés, mais laisse passer les utilisateurs connectés via un cookie "se souvenir de moi"

        //Pour récupérer l'utilisateur connecté 
        //on utilise 
        $user = $this->getUser();

        //on crée un objet de classe Article à hydrater
        $article = new Article();
        $article->setCreatedOn(new \DateTime); //on rempli déjà la date de création de l'article
        $article->setAuthor($user); //on assigne l'utilisateur connecté à l'article en tant qu'auteur

        //pour établir un formulaire de création d'Article
        //on va utiliser un composant de symfony appelé FormBuilder
        //on passe l'entité dans laquelle stocker les valeurs du formulaire à notre FormBuilder
        //pour qu'il puisse déterminer le nombre et type de champs à afficher
        //pour ajouter un champ au formulaire on utilise la méthode add() dans laquelle on précise 
        //la propriété concernée par le champ du formulaire, ainsi que le type de champ à afficher
        //TextType afficher un <input type="text"> TextareaType affiche un <textarea></textarea> dans le form
        $form = $this->createFormBuilder($article)
        ->add('title', TextType::class)
        ->add('content', TextareaType::class)
        ->add('submit', SubmitType::class, ['label' => 'Envoyer Article'])
        ->getForm();

        //pour traiter la requête HTTP on utilise la méthode handleRequest de notre form
        //le formulaire recevra la requête HTTP ayant mené a la page, et pourra voir si des paramètres et des en têtes particuliers ont été envoyés (par ex: des paramètres POST de formulaire)
        //pour récupérer la requête HTTP ayant mené à cette page, il faut la passer en paramètre dans la fonction de notre controleur
        $form->handleRequest($request); //à ce moment là, le formulaire essaye d'hydrater notre objet
        //à partir de la requête HTTP le formulaire va pouvoir déterminer si on est en GET ou en POST
        if ($form->isSubmitted() && $form->isValid()){ //on utilise isSubmitted et isValid pour vérifier que notre formulaire ait bien été envoyé dans son intégralité
        
            //on peut maintenant enregistrer notre article hydraté dans notre base 

            //pour persister/enregistrer un objet dans notre base de données
            //on doit faire appel à l'ORM Doctrine, responsable du dialogue entre les objets PHP et la bdd relationnelle MySQL
            //le composant de Doctrine s'occupant des entités/objets s'appelle EntityManager
            $entityManager = $this->getDoctrine()->getManager(); //on demande à Doctrine de nous envoyer l'EntityManager
            //une fois notre entité créée et notre instance d'EntityManager récupérée, on demande à Doctrine de "suivre" cette entité
            $entityManager->persist($article); //à cet instant, on indique à Doctrine que cet objet de classe Article doit être suivi
            //puis on demande à notre EntityManager de valider les changements en base (execution de la requête)
            $entityManager->flush();

            //on redirige ensuite vers l'accueil
            return $this->redirectToRoute('blog_index');
        }

        //si aucun formulaire n'a été envoyé, on affiche donc le template contenant le formulaire
        return $this->render('blog/add.html.twig', ['addForm' => $form->createView()]);
    }

    /**
     * @Route("/update/{id}", name="blog_update")
     */
    public function update($id, Request $request){
        //on redirige les utilisateurs non connectés
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        //on récupère notre article
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        
        //on récupère l'utilisateur 
        $user = $this->getUser();
        //on vérifie que l'utilisateur connecté soit bien l'auteur de l'article
        if ($user != $article->getAuthor()){
            //sinon on redirige 
            return $this->redirectToRoute('blog_index');
        }

        //on crée un formulaire de modification
        $form = $this->createFormBuilder($article)
        ->add('title', TextType::class)
        ->add('content', TextareaType::class)
        ->add('submit', SubmitType::class, ['label' => 'Envoyer Article'])
        ->getForm();

        $form->handleRequest($request); //les modifications de l'article sont prises en compte ici

        if ($form->isSubmitted() && $form->isValid()){
            //on a pas besoin de lancer persist() sur notre Article car il est déjà "suivi" par Doctrine
            //par contre on doit bien utiliser flush() pour que notre UPDATE SQL soit envoyé
            $this->getDoctrine()->getManager()->flush();

            //on redirige vers la vue de l'article, en précisant l'id de l'article
            return $this->redirectToRoute('blog_article', ['id' => $article->getId()]);
        }
        return $this->render('blog/update.html.twig', ['updateForm' => $form->createView()]);
    }

    /**
     * @Route("/delete/{id}", name="blog_delete")
     */
    public function delete($id){
        //on redirige les utilisateurs non connectés
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        //pour supprimer un article/une entité 
        //il faut d'abord récupérer ladite entité via Doctrine
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);
        $article = $articleRepository->find($id);
        
        //on récupère l'utilisateur 
        $user = $this->getUser();
        //on vérifie que l'utilisateur connecté soit bien l'auteur de l'article
        if ($user != $article->getAuthor()){
            //sinon on redirige 
            return $this->redirectToRoute('blog_index');
        }

        //pour retirer le suivi d'une entité sur Doctrine, et donc pour supprimer un enregistrement en bdd
        //on utilise remove() de l'entityManager
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        return $this->redirectToRoute('blog_index');
    }
}