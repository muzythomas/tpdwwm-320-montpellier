<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

//en héritant (extends) de EasyAdminController, on peut altérer le comportement des actions de EasyAdmin
//en surimplémentant (surchargeant/override) les différentes méthodes de la classe EasyAdminController
//cela nous permettra par exemple de hydrater certaines propriétés d'entités comme la date ou l'auteur 
class AdminController extends EasyAdminController 
{

    private $encoder;
    //On récupère l'UserPasswordEncoder via injection de dépendance a l'instanciation de notre classe
    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;    
    }

    //persist<Entité>Entity() est la méthode appelée juste avant l'enregistrement en base (persistance) d'une Entité via EasyAdmin
    //pour cibler l'action persistEntity de Article il faut donc implémenter la fonction persistArticleEntity
    public function persistArticleEntity(Article $entity){
        //$entity contient l'entité Article concernée par cette action
        //et on peut donc la modifier selon notre bon vouloir
        $entity->setCreatedOn(new \DateTime);
        $entity->setAuthor($this->getUser());

        //une fois nos actions personnalisées effectuées, on repasse la main au EasyAdminController 
        //en appelant parent::persistEntity
        parent::persistEntity($entity);
    }

    //pour cibler l'action persistEntity de User
    //il faut l'appeler persistUserEntity
    public function persistUserEntity(User $entity){
        $entity->setCreatedOn(new \DateTime);
        //on récupère notre mot de passe en clair
        $plainPassword = $entity->getPassword();
        //on l'encode et le stocke dans l'entité
        $entity->setPassword($this->encoder->encodePassword($entity, $plainPassword));

        //puis on appelle EasyAdminController pour persister l'entité
        parent::persistEntity($entity);
    }

}