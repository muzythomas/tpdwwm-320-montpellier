<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $user->setCreatedOn(new \DateTime());
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            /*
            Gestion de l'upload manuelle
            //gestion du téléchargement d'image de profil
            //on récupère l'image dans notre formulaire
            $imageFile = $form->get('picture')->getData();
            //pour stocker l'image sur notre serveur, on doit d'abord lui donner un nom unique
            $uniqueName = md5(uniqid("", true)); //génère un id unique de 23 char ensuite hashé en md5 pour un nom de 32 char
            //pour définir le nom de fichier final on a également besoin de l'extension du fichier
            $fileName = $uniqueName . '.' . $imageFile->guessExtension(); //guessExtension renvoie l'extension supposée du fichier téléchargé

            //maintenant qu'on a un nom pour notre fichier, on va tenter de l'enregistrer sur notre serveur
            //move permet de déplacer le fichier vers une destination de notre choix, et de lui donner un nom
            //le premier paramètre de move est le chemin de destination 
            //le second est le nom final du fichier
            $imageFile->move(
                "uploads/userPictures/",
                $fileName
            );
            //on peut ensuite enregistrer le nom du fichier dans notre entité
            $user->setPicture($fileName);
            */


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            return $this->redirectToRoute('blog_index');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
