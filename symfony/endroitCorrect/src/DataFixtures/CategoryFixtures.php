<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture{

    public function load (ObjectManager $manager){
        $categorieVoitures = new Category();
        $categorieVoitures->setLabel("Voitures");
        $categorieVoitures->setName('cars');
        //on ajoute une ref a cette categorie pour utilisation future dans une autre fixture
        $this->addReference($categorieVoitures->getName(), $categorieVoitures);

        $categorieMaisons = new Category();
        $categorieMaisons->setLabel("Maisons");
        $categorieMaisons->setName('housing');
        $this->addReference($categorieMaisons->getName(), $categorieMaisons);
        
        $categorieHighTech = new Category();
        $categorieHighTech->setLabel("High Tech");
        $categorieHighTech->setName('high_tech');
        $this->addReference($categorieHighTech->getName(), $categorieHighTech);
        
        $categorieVetements = new Category();
        $categorieVetements->setLabel("Vêtements");
        $categorieVetements->setName('clothing');
        $this->addReference($categorieVetements->getName(), $categorieVetements);
        
        $manager->persist($categorieVoitures);
        $manager->persist($categorieVetements);
        $manager->persist($categorieHighTech);
        $manager->persist($categorieMaisons);

        $manager->flush();
    }
}