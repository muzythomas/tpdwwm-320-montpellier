<?php

namespace App\Form;

use App\Entity\Advert;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertType extends AbstractType{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class)
        ->add('description', TextareaType::class)
        ->add('location', TextType::class)
        ->add('price', MoneyType::class, ['divisor' => 100])
        ->add('isToBeDiscussed', CheckboxType::class, ['required' => false])
        ->add('category', EntityType::class, ['class' => Category::class])
        ->add('images', CollectionType::class, [
            'entry_type' => AdvertImageType::class ,
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false    
        ])
        ->add('submit', SubmitType::class, ['label' => 'Post Advert']);
    }

    public function configureOptions(OptionsResolver $resolver){
        //setDefaults permet de définir les paramètres par défaut de notre FormTypes
        //data_class est une option qui permet de préciser quelle est la classe d'entité autorisée pour construire le formulaire
        $resolver->setDefaults(
            [
                'data_class' => Advert::class
            ]
        );
    }
}