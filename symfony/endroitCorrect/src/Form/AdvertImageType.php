<?php
namespace App\Form;

use App\Entity\AdvertImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class AdvertImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('file', VichImageType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => AdvertImage::class
            ]
        );
    }
}