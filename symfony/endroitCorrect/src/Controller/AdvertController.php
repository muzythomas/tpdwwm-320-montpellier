<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\User;
use App\Form\AdvertType;
use App\Repository\AdvertRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvertController extends AbstractController
{
    /**
     * @Route("/", name="advert")
     */
    public function index(AdvertRepository $advertRepository)
    {
        $adverts = $advertRepository->findAll();
        return $this->render('advert/index.html.twig', ['adverts' => $adverts]);
    }

    /**
     * @Route("/category/{name}", name="advert_category")
     */
    public function advertByCategory(string $name, AdvertRepository $advertRepository, CategoryRepository $categoryRepository)
    {
        $category = $categoryRepository->findOneBy(['name'=> $name]);
        $adverts = $advertRepository->findBy(['category' => $category]);
        return $this->render('advert/advert_category.html.twig', ['adverts' => $adverts]);
    }

    /**
     * @Route("/create", name="advert_create")
     */
    public function createAdvert(Request $request){
        $advert = new Advert();
        
        $form = $this->createForm(AdvertType::class, $advert);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $advert->setCreatedOn(new \DateTime);

            //TEMPORAIRE LE TEMPS D'AVOIR UN MOYEN DE CONNECTER LES USERS
            $advert->setSeller($this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => 'fake-user' . random_int(1, 25)])); //récupère un fake-user créé via fixture aléatoire 

            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            return $this->redirectToRoute('advert');          
        }

        return $this->render('advert/advert_create.html.twig', ['create_form' => $form->createView()]);
    }

    
    /**
     * @Route("/edit/{id}", name="advert_edit", requirements={
     *  "id" = "\d+"
     * })
     */
    public function editAdvert(Request $request, AdvertRepository $advertRepository, $id){
        $advert = $advertRepository->find($id);

        $form = $this->createForm(AdvertType::class, $advert);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()){

            $advert->setCreatedOn(new \DateTime);

            //TEMPORAIRE LE TEMPS D'AVOIR UN MOYEN DE CONNECTER LES USERS
            $advert->setSeller($this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => 'fake-user' . random_int(1, 25)])); //récupère un fake-user créé via fixture aléatoire 

            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            return $this->redirectToRoute('advert');          
        }

        return $this->render('advert/advert_create.html.twig', ['create_form' => $form->createView()]);
    }
}
