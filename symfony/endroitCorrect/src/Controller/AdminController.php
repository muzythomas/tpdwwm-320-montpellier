<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends EasyAdminController
{

    private UserPasswordEncoderInterface $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function persistUserEntity(User $user)
    {
        $plainPassword = $user->getPassword();
        $user->setPassword($this->encoder->encodePassword($user, $plainPassword));

        $user->setCreatedOn(new \DateTime());

        parent::persistEntity($user);
    }

    public function persistAdvertEntity(Advert $advert)
    {
        $advert->setCreatedOn(new \DateTime);
        //$advert->setSeller($this->getUser());
        
        parent::persistEntity($advert);
    }
}
