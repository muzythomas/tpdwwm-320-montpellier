# SITE D'ANNONCES 
## L'endroit Correct

Réaliser un début de projet Symfony pour un site d'annonces, type "LeBonCoin".

Un utilisateur doit pouvoir poster, consulter, modifier, supprimer des annonces. 
Une annonce contiendrait typiquement 
 * un prix de vente
 * une localisation de vente
 * une date de publication

Mais les propriétés de l'annonce seront à votre discretion.
