# Authentification via JWT (Json Web Token) 

## Qu'est ce que le JWT ? 

JWT est un standard permettant l'échange sécurisé de jetons (tokens) entre plusieurs parties d'une application. Cette sécurité se traduit par la vérification de l'intégrité des données à l'aide d'une signature numérique, souvent RSA. 

JWT dans le contexte de notre application nous servira à implémenter un système d'authentification via notre API en renvoyant un token en échange d'un couple nom d'utilisateur/mot de passe valide, en json. 

## Mise en place du JWT dans Symfony

Pour pouvoir utiliser ce système d'authentification via JWT, on utilise le bundle `lexik/jwt-authentication` :

```
composer require lexik/jwt-authentication-bundle
```

Celui ci génère un fichier `config/packages/lexik_jwt_authentication.yaml`, et modifie également notre .env. 

Pour installer ce bundle et le paramétrer, on suit la [documentation](https://github.com/lexik/LexikJWTAuthenticationBundle/blob/master/Resources/doc/index.md#getting-started).

### Génération de clés SSH 

Pour pouvoir signer en [RSA](https://fr.wikipedia.org/wiki/Chiffrement_RSA) les JWT, le bundle a besoin d'une clé privée et d'une clé publique. 

Pour en générer, on a besoin d'une implémentation de SSL, ou OpenSSL sur notre ordinateur. 
Si git pour windows est déjà installé, on peut utiliser l'executable d'openssl que git utilise pour générer nos clés. 

On ajoute donc dans notre variable d'environnement système `PATH` le chemin suivant : 

```
C:\Program Files\git\usr\bin
```

Avec ce chemin ajouté au path, on peut désormais utiliser la commande `openssl` dans notre terminal. 

Pour générer des clés SSH dans notre projet, on se réfère donc aux commandes données dans la documentation : 

Pour créer un dossier visant à contenir nos clés : 
```
mkdir -p config/jwt
```

Pour générer la clé privée servant à déchiffrer :
(Lors de la génération de la clé privée, un mot de passe sera demandé, notez le quelque part !) 
```
openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
```

Pour générer la clé publique à partir de notre clé privée : 
```
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```

Les deux fichiers générés, `private.pem` et `public.pem` sont ensuite référencés dans le .env : 

```
###> lexik/jwt-authentication-bundle ###
JWT_SECRET_KEY=%kernel.project_dir%/config/jwt/private.pem
JWT_PUBLIC_KEY=%kernel.project_dir%/config/jwt/public.pem
JWT_PASSPHRASE=VOTRE_MOT_DE_PASSE
###< lexik/jwt-authentication-bundle ###
```

### Paramétrage du module Security de Symfony

Une fois le lexik-jwt-authentication bundle installé, il faut paramétrer son fonctionnement dans le fichier `config/packages/security.yaml` :

```yaml

security:

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        #On prépare une règle de firewall pour notre page de login
        login:
            pattern:  ^/login
            stateless: true
            anonymous: true
            json_login:
                #cette route est définie dans le config/routes.yaml
                check_path:               /login_check
                #ces handlers sont ceux proposés par le bundle 
                success_handler:          lexik_jwt_authentication.handler.authentication_success
                failure_handler:          lexik_jwt_authentication.handler.authentication_failure
        #On prépare également une règle de firewall pour notre api
        api:
            pattern: ^/
            stateless: true
            guard:
                authenticators:
                    #Ici on indique que l'authentification doit se faire à l'aide du bundle
                    - lexik_jwt_authentication.jwt_token_authenticator
        main:
            anonymous: lazy
            provider: app_user_provider

    access_control:
        #Ces lignes permettent de définir quelle partie du site demande de s'authentifier via le bundle, et quelle partie du site n'en a pas besoin
        #par exemple, pour /login on ne demande pas à être déjà connecté
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        #Pour le reste de l'api cependant, il faut être authentifié 
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY }

```

### Utiliser l'authentification

Pour tester notre authentification, en partant du principe qu'on possède un utilisateur dans notre base, il faut tenter de se connecter.

Pour cela, on envoie une requête POST sur notre route `login_check` avec comme paramètres le login et le mot de passe de notre utilisateur. 
En réponse, si le couple login et mot de passe est correct, on devrait recevoir 
`200 OK` ainsi que l'objet JSON suivant :

```json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTkxMzQzNTksImV4cCI6MTU5OTEzNzk1OSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdCJ..."
}
```
Après avoir récupéré notre token, il suffit d'envoyer une requête HTTP sur une des actions de notre API, et d'agrémenter cette requête d'un en-tête particulier.

Cet en-tête est Authorization : 

```
Authorization : Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTkxMzQzNTksImV4cCI6MTU5OTEzNzk1OSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdCJ...
```

Lors de l'envoi de notre requête, si le token suivant le mot clé Bearer est un token valide, la requête sera acceptée. Sinon, la réponse sera `401 Unauthorized` 

```json
{
  "code": 401,
  "message": "JWT Token not valid"
}
```

### Autoriser certaines actions d'API sans authentification

Nos routes d'API sont désormais protégées par notre système de JWT, mais certaines actions devraient pouvoir être accessibles même sans authentification (lister, voir une ressource... etc).

Pour autoriser ces actions là, il faut rajouter des règles de pare-feu dans le `security.yaml` :
 
```yaml
security:

    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false

        #On rajoute cette règle ici pour autoriser les accès à genre et format si la méthode est GET ou HEAD
        lists: 
            pattern: ^/(genre|format)
            methods: [GET, HEAD]
            anonymous: true
            stateless: true

        login:
            pattern:  ^/login
            stateless: true
            anonymous: true
            json_login:
                check_path:               /login_check
                success_handler:          lexik_jwt_authentication.handler.authentication_success
                failure_handler:          lexik_jwt_authentication.handler.authentication_failure
        api:
            pattern: ^/
            stateless: true
            guard:
                authenticators:
                    - lexik_jwt_authentication.jwt_token_authenticator
        main:
            anonymous: lazy
            provider: app_user_provider

    access_control:
        - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        # Ici, on décide de laisser un utilisateur "anonyme" accéder à ces actions si elles sont en GET ou HEAD
        - { path: ^/(genre|format), roles: IS_AUTHENTICATED_ANONYMOUSLY, methods: [GET, HEAD] }
        - { path: ^/, roles: IS_AUTHENTICATED_FULLY }

```
## Implémentation de la connexion sur Angular
Pour que notre application front-end puisse s'authentifier auprès de notre API, elle doit donc se procurer le JWT valide à la connexion, puis l'ajouter en en-tête `Authorization` à chaque requête HTTP qui suivra. 

### Mettre en place un service d'authentification
Pour effectuer une requête de connexion sur `login_check`, on crée donc un service 
```
ng g s auth
```
Et on met en place une requête HTTP post dans ce service vers notre route login_check`
```ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = "https://localhost:8000/login_check";

  //on stocke nos en tête de requête dans une propriété headers de type HttpHeaders
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    //lors de l'initialisation du service on crée les headers
    this.headers = new HttpHeaders();
    //et on ajoute un en-tête Content-Type : application/json
    this.headers.append('Content-Type', 'application/json');
  }

  public auth(username: string, password: string): Observable<any>{
    //on construit le corps de notre requête
    const body = {
      username: username,
      password: password
    };
    return this.http.post(this.url, body, {headers: this.headers});
  }
}
```

On appelle ensuite ce service dans un component pour pouvoir l'utiliser. 

### Stockage du JWT dans le localStorage

Pour pouvoir insérer le JWT dans chacune des requêtes à suivre, on stocke, à la récéption du JWT, celui-ci dans le localStorage: 

Par exemple, dans une méthode `auth()` d'un component :

```ts
auth(): void{
    this.authService.auth('test', 'test').subscribe(
      (result: any) => {
        //si on reçoit une réponse positive (200 OK), on stocke le token récupéré dans le localStorage pour utilisation ultérieure
        localStorage.setItem('jwt', result.token);
      }, 
      (error: any) => {
        if (error.status == 401) {
          console.log('wrong username or password')
        } else {
          console.log(error);
        }
      }
    )
}

```

### Utilisation du JWT dans toutes nos requêtes

Pour pouvoir ajouter `Authorization` comme en-tête de toutes les requêtes de notre application, au lieu d'ajouter chaque en-tête à chaque service, on utilise un [HttpInterceptor](https://angular.io/api/common/http/HttpInterceptor).

Un `HttpInterceptor` a pour rôle d'intercepter toutes les requêtes sortantes de notre application pour pouvoir appliquer un traitement supplémentaire avant de les rediriger vers leur destination originelle.

Pour mettre en place un `HttpInterceptor`, il faut donc créer une class qui implémente l'interface `HttpInterceptor`.

L'interface demande d'implémenter une méthode `intercept` qui possède comme paramètres la requête originelle `req: HttpRequest<any>` et un gestionnaire Http `next: HttpHandler`. 

Le traitement à effectuer est très simple, il faut cloner la requête entrante et y ajouter le JWT dans un en-tête Authorization, si le JWT est trouvé dans le localStorage.

Sinon, on se contente de laisser passer la requête originelle sans modifications. 

Une implémentation de ce traitement pourraît donc ressembler à ça : 


```ts
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    //Un HTTPInterceptor sert à intercepter les requêtes sortantes de notre application
    //la méthode intercept nous permet de définir ce qui doit arriver à la requête lorsqu'elle est interceptée
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //le rôle de notre authinterceptor est de vérifier la présence d'un jwt dans le localStorage, et si c'est le cas, le rajouter dans un en-tête Authorization avant d'envoyer la requête interceptée
        //en premier lieu, on vérifie si notre token est présent dans le localStorage
        const jwt = localStorage.getItem('jwt');
        if (jwt){
            //un objet HttpRequest étant immuable, on ne peut y ajouter directement le header Authorization de cette façon :
            //req.headers.append('Authorization', `Bearer ${jwt}`);
            //A la place, on doit cloner la requête sortante et y ajouter par la même occasion le header
            const cloned = req.clone({ headers: req.headers.append('Authorization', `Bearer ${jwt}`) });
            //on envoie ensuite la requête clonée à notre httpHandler
            return next.handle(cloned);
        }
        //dans le cas par défaut, sans jwt donc, on envoie la requête sans modifications
        return next.handle(req); //on la laisse passer sans rien faire 
    }

}
```

Une fois notre HttpInterceptor implémenté, il faut ensuite l'enregistrer comme Provider de notre module dans le `app.module.ts` : 

```ts

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/authInterceptor';

@NgModule({
  declarations: [
    AppComponent,
    //reste des declarations...
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    //reste des imports...
  ],
  providers: [
    //on enregistre notre AuthInterceptor comme HTTP_INTERCEPTOR de l'application
    //multi: true permet d'utiliser de multiples intercepteurs (dont celui par défaut d'angular) sans risque de conflits 
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

```

[Plus d'informations sur les JWT](https://jwt.io/)

### Décoder la charge utile d'un JWT : 

Dans un JWT, on reçoit certes une clé de chiffrement générée par le serveur à l'aide de ses clés SSH, permettant ainsi de vérifier l'authenticité d'un JWT, mais on reçoit également certaines méta données, et données d'application, encodées dans ce JWT en base64.

L'anatomie d'un JWT se découpe de la façon suivante : 
`header.payload.signature`
Par exemple, l'en-tête ressemblerait à ça :

```json
{
  "typ": "JWT",
  "alg": "RS256"
}
```
La charge utile venant de notre API Symfony:

```json
{
  "iat": 1599138164,
  "exp": 1599141764,
  "roles": [
    "ROLE_USER"
  ],
  "username": "test"
}
```

et la signature serait encodée via RSA. 

Les valeurs encodées en base64, séparées par des ., ressemblent donc à ça : 

`
eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1OTkxMzgxNjQsImV4cCI6MTU5OTE0MTc2NCwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidGVzdCJ9.M5ppxoDTrxW09p_dxE7EoVrmi_x3X_Pbs-IB68UdkP1Ns46HtB4d9z85lRydJuMXA0SdaPo1PN1apsM3rFkxlspsTTjsv_g-1GWsAglhrNk8M2on1fu5U8FsMv1a0fpVKFq3ITXH3n8_A_TJjm-NfruZW_AdKhL2gfpTqWs_iw0aR523edAjw9vazGUZJtYcoKFh8zCsld4vwZTVUxOZCJ9d1xHGwlrOYLMneljG7EBbmBlrM6i3WCgowqvv3xKoeYiPPAlHEh3pYCV3Zfv7p_86tZXTkqAUCWaqUCSoC2bTCK13wr73x17WfGtK0gxi4zaG0kEq-JcXA-oLZZz74PxjoRqivPLZAG9nDCU8aac2hgKo9ot5vUMAzTjIsphHVu1RIUZ-5-uO-W43yd2SYdu7qh_bxb2I_l5UW5EVKJT49jVwFTpYIjgPGtD7CGwDsHJFY68qMPOnhX98K2VJ1IDh9rm7SikM0H2QXoXADnrArYwvdUWOOum5cr-xTHbN2GnfFmvmpaTtxWG_VUs2nUef4Cohl5BfK7lDYuHoEHz41k2pM-I5Pnem1Ec0CferIXcy1qcoSfYxDfGzBeDgCH6i4kW9A4sFB5ZwM_PBhIrjzgD4EpU9WzGyvHBiauwMchrcn8ExOGRXkybC6YV-G9Alnc-CtHdFlxHuLcXLgBw
`

Pour décoder ces informations, on peut couper la chaine de caractère au niveau des . et décoder en base64 les informations d'en-tête et de charge utile. 
On peut également utiliser un module node : 

```
npm install --save jwt-decode
```
