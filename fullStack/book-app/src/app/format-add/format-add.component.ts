import { Component, OnInit } from '@angular/core';
import { FormatService } from '../services/format.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Format } from '../model/format';

@Component({
  selector: 'app-format-add',
  templateUrl: './format-add.component.html',
  styleUrls: ['./format-add.component.css']
})
export class FormatAddComponent implements OnInit {

  constructor(private fs: FormatService, private fb: FormBuilder) { }

  formCreate: FormGroup;
  ngOnInit(): void {
    this.formCreate = this.initForm();
  }

  initForm(): FormGroup {
    return this.fb.group(
      {
        name: [''],
        width: [''],
        height: [''],
        landscape: [true],
      }
    )
  }

  create(): void {
    const format:Format = this.formCreate.value;
    this.fs.create(format).subscribe(data => console.log(data));
  }

}
