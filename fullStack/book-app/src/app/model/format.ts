export interface Format {
    id: number;
    name: string;
    width: number;
    height: number;
    landscape: boolean;
    books: any[];
}