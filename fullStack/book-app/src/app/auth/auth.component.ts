import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import * as jwtDecode from "jwt-decode";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.auth();
  }

  auth(): void{
    this.authService.auth('test', 'test').subscribe(
      (result: any) => {
        //si on reçoit une réponse positive (200 OK), on stocke le token récupéré dans le localStorage pour utilisation ultérieure
        const jwt = {
          token: result.token,
          //payload (charge utile) correspond au corps de notre JWT
          //contenant les informations comme la date d'expiration ou les infos du user
          payload: jwtDecode(result.token)
        }
        //on enregistre notre nouvel objet sous forme de JSON dans le localstorage
        localStorage.setItem('jwt', JSON.stringify(jwt));
      }, 
      (error: any) => {
        if (error.status == 401) {
          console.log('wrong username or password')
        } else {
          console.log(error);
        }
      }
    )
  }

}
