import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = "https://localhost:8000/login_check";

  //on stocke nos en tête de requête dans une propriété headers de type HttpHeaders
  private headers: HttpHeaders;

  constructor(private http: HttpClient) {
    //lors de l'initialisation du service on crée les headers
    this.headers = new HttpHeaders();
    //et on ajoute un en-tête Content-Type : application/json
    this.headers.append('Content-Type', 'application/json');
  }

  public auth(username: string, password: string): Observable<any>{
    //on construit le corps de notre requête
    const body = {
      username: username,
      password: password
    };
    return this.http.post(this.url, body, {headers: this.headers});
  }

  public logout(): void{
    //en cas de deconnexion on supprime le jwt du local storage
    localStorage.removeItem('jwt');
  }

  //abstraction de la récupération de notre JWT dans le localStorage
  public getJWT() : any {
    const jwt = JSON.parse(localStorage.getItem('jwt'));
    if (jwt){
      //cela nous permet de tester si le JWT est expiré avant de le renvoyer
      if (Date.now()/1000 > jwt.payload.exp) {
        //s'il est expiré on le retire du localStorage et on renvoie null
        localStorage.removeItem('jwt');
        console.log("jwt expiré");
        return null;
      }
    }
    return jwt; 
  }

}
