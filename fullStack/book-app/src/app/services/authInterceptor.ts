import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService){}

    //Un HTTPInterceptor sert à intercepter les requêtes sortantes de notre application
    //la méthode intercept nous permet de définir ce qui doit arriver à la requête lorsqu'elle est interceptée
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        //le rôle de notre authinterceptor est de vérifier la présence d'un jwt dans le localStorage, et si c'est le cas, le rajouter dans un en-tête Authorization avant d'envoyer la requête interceptée
        //en premier lieu, on vérifie si notre token est présent dans le localStorage
        //étant donné que notre token est stocké dans un objet JSON dans le local storage
        //on doit d'abord décoder le JSON
        const jwt = this.authService.getJWT();
        if (jwt){
            //puis récupérer le token dans l'objet du localstorage
            const token = jwt.token;
            //un objet HttpRequest étant immuable, on ne peut y ajouter directement le header Authorization de cette façon :
            //req.headers.append('Authorization', `Bearer ${jwt}`);
            //A la place, on doit cloner la requête sortante et y ajouter par la même occasion le header
            const cloned = req.clone({ headers: req.headers.append('Authorization', `Bearer ${token}`) });
            //on envoie ensuite la requête clonée à notre httpHandler
            return next.handle(cloned);
        }
        //dans le cas par défaut, sans jwt donc, on envoie la requête sans modifications
        return next.handle(req); //on la laisse passer sans rien faire 
    }

}