<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserController extends AbstractController
{
    /**
     * @Route("/create/user", name="create_user")
     */
    public function index(UserPasswordEncoderInterface $encoder, EntityManagerInterface $em)
    {
        $user = new User();
        $user->setUsername("test2");
        $user->setPassword($encoder->encodePassword($user, "test2"));
        $em->persist($user);
        $em->flush();
        return $this->json($user);
    }
}
