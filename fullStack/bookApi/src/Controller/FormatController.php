<?php

namespace App\Controller;

use App\Entity\Format;
use App\Repository\FormatRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/format")
 */
class FormatController extends AbstractController
{
    //avec le paramètre methods dans l'annotation de la route
    //on peut préciser quelles sont les méthodes acceptées pour une route de controleur
    //ainsi, seules les requêtes faites avec ces méthodes http là feront exécuter le code correspondant
    /**
     * @Route("", name="format_list", methods={"GET", "HEAD"})
     */
    public function list(FormatRepository $repository)
    {
        $formats = $repository->findAll();
        return $this->json($formats);
    }

    /**
     * @Route("/{id}", name="format_view", methods={"GET", "HEAD"})
     */
    public function view($id, FormatRepository $repository)
    {
        $format = $repository->find($id);
        if ($format) {
            return $this->json($format);
        } else {
            return $this->json(
                [
                    'status' => 404,
                    'title' => 'Not Found',
                    'detail' => sprintf("Resource Format with id %d not found", $id),
                    'type' => 'about:blank'
                ],
                404,
                [
                    'content-type' => 'application/problem+json'
                ]
            );
        }
    }

    /**
     * @Route("", name="format_create", methods={"POST"})
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        if (!empty($data)) {
            //on demande au serializer de deserializer (transformer du json en objet) pour créer notre entité format
            try {
                $format = $serializer->deserialize($data, Format::class, 'json');
            } catch (\Exception $e) {
                return $this->json(
                    [
                        'status' => 400,
                        'title' => 'Bad request',
                        'type' => 'about:blank',
                    ],
                    400,
                    ['content-type' => 'application/problem+json']
                );
            }
            //TODO : vérifier que l'entité deserializée corresponde bien à une entity format
            $em = $this->getDoctrine()->getManager();
            $em->persist($format);
            $em->flush();

            return $this->json($format, 201);
        } else {
            return $this->json(
                [
                    'status' => 400,
                    'title' => 'Bad request',
                    'type' => 'about:blank',
                ],
                400,
                ['content-type' => 'application/problem+json']
            );
        }
    }

    /**
     * @Route("/{id}", name="format_update", methods={"PUT"})
     */
    public function update($id, FormatRepository $repository,  Request $request, SerializerInterface $serializer)
    {
        //pour modifier une entité, il faut d'abord la récupérer
        $format = $repository->find($id);
        if ($format) {
            //on récupère les nouvelles données
            $data = $request->getContent();
            //lors de la deserialization, on peut préciser une option object_to_populate
            //de façon à indiquer au serializer dans quelle entité "ranger" les données reçues 
            $serializer->deserialize($data, Format::class, 'json', [
                'object_to_populate' => $format
            ]);
            //comme l'entité existait déjà, on a pas besoin de faire persist à nouveau
            $this->getDoctrine()->getManager()->flush();
            //on renvoie l'objet nouvellement modifié
            return $this->json($format);
        } else {
            return $this->json(
                [
                    'status' => 404,
                    'title' => 'Not Found',
                    'detail' => sprintf("Resource Format with id %d not found", $id),
                    'type' => 'about:blank'
                ],
                404,
                [
                    'content-type' => 'application/problem+json'
                ]
            );
        }
    }

    /**
     * @Route("/{id}", name="format_delete", methods={"DELETE"})
     */
    public function delete($id, FormatRepository $repository)
    {
        $format = $repository->find($id);
        if ($format) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($format);
            $em->flush();
            return $this->json(
                [
                    "success" => sprintf("format %d:%s successfully deleted", $id, $format->getName())
                ]
            );
        } else {
            return $this->json(
                [
                    'status' => 404,
                    'title' => 'Not Found',
                    'detail' => sprintf("Resource Format with id %d not found", $id),
                    'type' => 'about:blank'
                ],
                404,
                [
                    'content-type' => 'application/problem+json'
                ]
            );
        }
    }
}
