import { Key, EasingFunctions, getRandNum } from "./Utils.mjs";

export default class Game {
    constructor(canvas, context, bouee) {
        this.canvas = canvas;
        this.context = context;
        this.bouee = bouee;

        //fps calculations
        this._timestamp = 0;
        this.lastDrawTime = 0;
        this.drawTime = 0;
        this.fps = 0;

        //handle buoyancy effect through canvas transform
        //each key has a value = next transform matrix
        //time = duration it takes to interpolate towards the matrix's values
        this.buoyKeys = [
            new Key([1, 0, 0, 1, 0, 0], 0),
            new Key([0.995, 0.005, 0.01, 0.995, 0, 0], 1),
            new Key([1, -0.005, 0.005, 1, 0, 0], 2),
            new Key([0.995, 0.005, 0.01, 0.995, 0, 0], 3),
            new Key([1, 0, 0, 1, 0, 0], 4),
        ];

        //allows me to keep track of frames lapsed
        this.animationCount = 0;

        this.rotationAngle = 0;
    }

    set timestamp(timestamp) {
        //each time the timestmap is saved calculate the draw time and the fps count
        this.lastDrawTime = this._timestamp;
        this._timestamp = timestamp;
        this.drawTime = (timestamp - this.lastDrawTime) / 1000;

        this.fps = Math.round(1 / this.drawTime);
    }
    get timestamp() {
        return this._timestamp;
    }

    draw() {
        //Clear canvas and draw water
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        const poolImage = new Image();
        poolImage.src = "./images/pool.jpg";
        const poolPattern = this.context.createPattern(poolImage, "repeat");
        this.context.fillStyle = poolPattern;
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);

        //save allows to restore canvas state later, allowing for transform a of a portion of objects
        this.context.save();

        //finding the next key transform matrix according to animation frame counts
        let animationKeyIndex = this.buoyKeys.findIndex(
            (k) => k.time >= Math.floor(this.animationCount)
        );

        //allowing for time to elapse
        this.animationCount += this.drawTime;

        //if a key was found
        if (animationKeyIndex != -1) {
            let nextKeyIndex = animationKeyIndex + 1;

            //looping
            if (nextKeyIndex >= this.buoyKeys.length) {
                nextKeyIndex = 0;
                this.animationCount = 0;
            }

            //get the interpolation matrix from transformInterpolation for the found key
            const interpolationMatrix = this.buoyKeys[
                animationKeyIndex
            ].transformInterpolation(
                this.animationCount,
                this.buoyKeys[nextKeyIndex],
                EasingFunctions.easeInOutQuint
            );

            //transform the canvas from the matrix
            this.context.setTransform(...interpolationMatrix);
        }

        this.context.drawImage(
            this.bouee.shadowImage,
            this.bouee.position.x + 15,
            this.bouee.position.y + -60,
            this.bouee.dimension,
            this.bouee.dimension
        );

        //rotate buoy
        this.context.translate(this.bouee.center.x, this.bouee.center.y);

        this.context.rotate(
            (this.rotationAngle +=
                (this.bouee.inertia != 0
                    ? this.bouee.inertia
                    : getRandNum(0.05, 0.1)) * Math.PI) / 180
        );
        this.context.translate(-this.bouee.center.x, -this.bouee.center.y);

        this.context.drawImage(
            this.bouee.image,
            this.bouee.position.x,
            this.bouee.position.y,
            this.bouee.dimension,
            this.bouee.dimension
        );

        //restoring the canvas context before drawing anything else
        this.context.restore();

        //this.drawCenterPoint(15, "white");
        if (this.timestamp != 0) {
            this.drawFpsCounter(10);
        }
    }

    drawCenterPoint(size, color = "pink") {
        //draw center point
        this.context.beginPath();
        this.context.strokeStyle = color;

        this.context.moveTo(this.bouee.center.x - size, this.bouee.center.y);
        this.context.lineTo(this.bouee.center.x + size, this.bouee.center.y);
        this.context.moveTo(this.bouee.center.x, this.bouee.center.y - size);
        this.context.lineTo(this.bouee.center.x, this.bouee.center.y + size);

        this.context.stroke();
    }

    drawFpsCounter(fontSize) {
        this.context.font = `${fontSize}px Arial`;
        this.context.fillStyle = "black";
        this.context.fillText(
            "FPS: " + this.fps,
            this.canvas.width - 6 * fontSize,
            this.canvas.height - fontSize
        );
    }
}
