import Position from "./Position.mjs";
import Buoy from "./Buoy.mjs";
import Game from "./Game.mjs";
import { EasingFunctions } from "./Utils.mjs";

document.addEventListener("DOMContentLoaded", () => {
    const gameCanvas = document.getElementById("gameCanvas");
    gameCanvas.width = document.body.clientWidth;
    gameCanvas.height = document.body.clientHeight;

    const ctx = gameCanvas.getContext("2d");

    const bouee = new Buoy(new Position(150, 150), "./images/bouee1.png", 128);

    const game = new Game(gameCanvas, ctx, bouee);

    const step = (timestamp) => {
        game.timestamp = timestamp;
        game.bouee.moveTowardsDestination(
            EasingFunctions.easeOutCubic,
            game.drawTime
        );
        game.draw();

        window.requestAnimationFrame(step);
    };
    window.requestAnimationFrame(step);

    window.addEventListener("resize", (event) => {
        gameCanvas.width = document.body.clientWidth;
        gameCanvas.height = document.body.clientHeight;
    });

    document.addEventListener("click", (event) => {
        game.bouee.destination = new Position(event.x, event.y);
        game.bouee.inertia = 3;
        game.bouee.launchTime = 0;
        console.log(game.bouee);
    });
});
