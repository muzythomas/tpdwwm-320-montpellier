const EasingFunctions = {
    // no easing, no acceleration
    linear: (t) => t,
    // accelerating from zero velocity
    easeInQuad: (t) => t * t,
    // decelerating to zero velocity
    easeOutQuad: (t) => t * (2 - t),
    // acceleration until halfway, then deceleration
    easeInOutQuad: (t) => (t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t),
    // accelerating from zero velocity
    easeInCubic: (t) => t * t * t,
    // decelerating to zero velocity
    easeOutCubic: (t) => --t * t * t + 1,
    // acceleration until halfway, then deceleration
    easeInOutCubic: (t) =>
        t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1,
    // accelerating from zero velocity
    easeInQuart: (t) => t * t * t * t,
    // decelerating to zero velocity
    easeOutQuart: (t) => 1 - --t * t * t * t,
    // acceleration until halfway, then deceleration
    easeInOutQuart: (t) =>
        t < 0.5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t,
    // accelerating from zero velocity
    easeInQuint: (t) => t * t * t * t * t,
    // decelerating to zero velocity
    easeOutQuint: (t) => 1 + --t * t * t * t * t,
    // acceleration until halfway, then deceleration
    easeInOutQuint: (t) =>
        t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t,
};

class Key {
    constructor(value, time) {
        this.value = value;
        this.time = time;
    }

    //from https://stackoverflow.com/a/37627537
    transformInterpolation(time, to, ease = EasingFunctions.linear) {
        const f = this.value;
        const t = to.value;
        //timing with ease applied
        //clamps it between 0 and 1
        const nt = Math.max(
            0,
            Math.min(1, ease((time - this.time) / (to.time - this.time)))
        );
        //return the interpolation values for each member of the matrix
        let i = 0;
        return [
            (t[i] - f[i]) * nt + f[i++],
            (t[i] - f[i]) * nt + f[i++],
            (t[i] - f[i]) * nt + f[i++],
            (t[i] - f[i]) * nt + f[i++],
            (t[i] - f[i]) * nt + f[i++],
            (t[i] - f[i]) * nt + f[i++],
        ];
    }
}

function getRandNum(min, max) {
    //toFixed + pareseFloat allows to conserve some floating point accuracy while generating numbers
    return parseFloat((Math.random() * (max - min) + min).toFixed(5));
}

export { Key, EasingFunctions, getRandNum };
