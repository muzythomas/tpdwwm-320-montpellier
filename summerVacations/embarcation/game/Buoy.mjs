import Position from "./Position.mjs";

export default class Buoy {
    constructor(position, imageSrc, dimension) {
        this._position = position;
        this._dimension = dimension;

        this._velX = 0;
        this._velY = 0;

        this.center = new Position(
            this.position.x + dimension / 2,
            this.position.y + dimension / 2
        );

        this._destination = this.center;

        this.image = new Image();
        this.image.src = imageSrc;

        this.shadowImage = new Image();
        this.shadowImage.src = "./images/shadow.png";

        //calculate inertia
        this.inertia = 0;
        this.launchTime = 0;
    }

    set position(position) {
        this._position = position;

        //Determine a new center each time the position is changed
        this.center = new Position(
            this.position.x + this.dimension / 2,
            this.position.y + this.dimension / 2
        );
    }
    get position() {
        return this._position;
    }

    set destination(destination) {
        this._destination = destination;
    }
    get destination() {
        return this._destination;
    }

    set dimension(dimension) {
        this._dimension = dimension;
    }
    get dimension() {
        return this._dimension;
    }

    set velX(velX) {
        this._velX = velX;
    }
    get velX() {
        return this._velX;
    }

    set velY(velY) {
        this._velY = velY;
    }
    get velY() {
        return this._velY;
    }

    moveTowardsDestination(ease, drawTime) {
        //position deltas
        const dx = this.destination.x - this.center.x;
        const dy = this.destination.y - this.center.y;

        //determine the angle of approach towards the position
        const angleOfApproach = Math.atan2(dy, dx);

        //decrease time each frame
        this.launchTime += drawTime;

        //calculate the easing
        const nt = Math.max(
            0,
            Math.min(1, ease(this.launchTime / Math.abs(dx + dy / 100)))
        );
        this.inertia = Math.max(0, this.inertia - nt);

        //set course towards the angle determined before
        this.velX = Math.cos(angleOfApproach) * this.inertia;
        this.velY = Math.sin(angleOfApproach) * this.inertia;

        //advance slowly towards the destination
        this.position = new Position(
            this.position.x + this.velX,
            this.position.y + this.velY
        );
    }
}
