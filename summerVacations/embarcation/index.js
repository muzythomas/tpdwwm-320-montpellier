const express = require("express");
const app = express();

app.use(express.static("game"));
const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log(`game client launched on port http://localhost:${port}`);
});
