import "./App.css";

import socket from "./socketConnect";
import { useState, useEffect } from "react";

function App() {
    const [inputValue, setInputValue] = useState("");
    const [messages, setMessages] = useState([]);

    const [nickname, setNickname] = useState("John Doe");

    useEffect(() => {
        socket.on("chat-history", (messages) => {
            setMessages(messages);
        });

        socket.on("new-message", (message) => {
            setMessages([...messages, message]);
        });

        return () => {
            socket.off("new-message");
        };
    });

    const handleSubmit = (event) => {
        event.preventDefault();
        socket.emit("new-message", inputValue, nickname);
        setInputValue("");
    };

    const messageList = messages.map((m, i) => {
        return (
            <div key={i}>
                {`${new Date(m.time).toLocaleTimeString("fr-FR")} - ${
                    m.author
                } : ${m.text}`}
            </div>
        );
    });

    return (
        <div className="App">
            {messageList}

            <input
                value={nickname}
                onChange={(e) => setNickname(e.target.value)}
                type="text"
            />

            <form onSubmit={handleSubmit}>
                <input
                    value={inputValue}
                    onChange={(e) => setInputValue(e.target.value)}
                    type="text"
                />
            </form>
        </div>
    );
}

export default App;
