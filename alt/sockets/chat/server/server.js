const app = require("express")();
const http = require("http");
const socketIo = require("socket.io");

const port = process.env.PORT || 4000;

const server = http.createServer(app);

const io = socketIo(server, {
    cors: {
        origin: "*",
        methods: "*",
    },
});
server.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

const messages = [];
io.on("connection", (socket) => {
    console.log(`${socket.id} connected`);

    socket.emit("chat-history", messages);

    socket.on("new-message", (message, author) => {
        const newMessage = {
            text: message,
            time: Date.now(),
            author: author,
        };

        messages.push(newMessage);
        io.emit("new-message", newMessage);
    });
});
