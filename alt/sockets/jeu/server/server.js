const app = require("express")();
const http = require("http");
const socketIo = require("socket.io");

const port = process.env.PORT || 4000;

const server = http.createServer(app);

const io = socketIo(server, {
    cors: {
        origin: "*",
        methods: "*",
    },
});

server.listen(port, () => {
    console.log(`listening on http://localhost:${port}`);
});

//on se prépare une liste de joueurs
const players = [];

io.on("connection", (socket) => {
    console.log(`connection ${socket.id}`);

    //quand un joueur se connecte on le push avec une couleur aléatoire
    const randomColor = [
        Math.floor(Math.random() * 255 + 1),
        Math.floor(Math.random() * 255 + 1),
        Math.floor(Math.random() * 255 + 1),
    ];
    players.push({ id: socket.id, x: 50, y: 50, color: randomColor });
    //et on envoie la mise à jour de la liste des joueurs aux clients
    io.emit("game-update", players);

    socket.on("player-position-update", (x, y) => {
        const player = players.find((p) => p.id === socket.id);
        //si on a déjà le joueur
        if (player) {
            //on met à jour sa position
            player.x = x;
            player.y = y;
        }
        console.log(players);

        //on envoie la mise à jour à tout le monde
        io.emit("game-update", players);
    });
});
