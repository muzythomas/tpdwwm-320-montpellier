const DIRECTIONS = {
    LEFT: "LEFT",
    RIGHT: "RIGHT",
    UP: "UP",
    DOWN: "DOWN",
};

//classe joueur permettant de gérer chaque joueur sur la carte
class Player {
    constructor(x, y, id, color) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.color = color;
    }

    move(direction, distance) {
        switch (direction) {
            case DIRECTIONS.UP: {
                this.y -= distance;
                break;
            }
            case DIRECTIONS.RIGHT: {
                this.x += distance;
                break;
            }
            case DIRECTIONS.DOWN: {
                this.y += distance;
                break;
            }
            case DIRECTIONS.LEFT: {
                this.x -= distance;
                break;
            }
        }
    }
}

//classe de jeu permettant de stocker le canvas et son contexte, ainsi que les joueurs
//se charge du dessin à l'écran des joueurs
class Game {
    constructor(ctx, canvas) {
        this.ctx = ctx;
        this.canvas = canvas;
        this.players = [];
    }

    //drawPlayers dessine chaque joueur dans le canvas
    drawPlayers() {
        //on commence par effacer
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        //pour chaque joueur
        this.players.forEach((player) => {
            //on dessine un rectangle avec la couleur du joueur
            this.ctx.fillStyle = `rgb(${player.color[0]},${player.color[1]},${player.color[2]})`;
            this.ctx.fillRect(player.x, player.y, 20, 50);
        });
    }
}

document.addEventListener("DOMContentLoaded", () => {
    //initialisation du canvas
    const canvas = document.getElementById("gameCanvas");
    const ctx = canvas.getContext("2d");

    //initialisation du jeu
    const game = new Game(ctx, canvas);

    //connexion au serveur
    const socket = io("localhost:4000");

    //lorsqu'on reçoit une mise à jour de notre serveur
    socket.on("game-update", (players) => {
        //on met à jour la liste des joueurs (plus tard on mettrait à jour le GameState)
        game.players = players.map((p) => new Player(p.x, p.y, p.id, p.color));

        //on stocke le joueur qui correspond à notre client dans une variable de type Player
        game.player = game.players.find((p) => p.id === socket.id);
        // on dessine tous les joueurs
        game.drawPlayers();
        console.log(game);
    });

    //eventlistener permettant le déplacement avec les touches fléchées
    document.addEventListener("keydown", (event) => {
        switch (event.key) {
            case "ArrowUp": {
                game.player.move(DIRECTIONS.UP, 10);
                break;
            }
            case "ArrowRight": {
                game.player.move(DIRECTIONS.RIGHT, 10);
                break;
            }
            case "ArrowDown": {
                game.player.move(DIRECTIONS.DOWN, 10);
                break;
            }
            case "ArrowLeft": {
                game.player.move(DIRECTIONS.LEFT, 10);
                break;
            }
        }
        //on envoie la nouvelle position de notre joueur au serveur
        socket.emit("player-position-update", game.player.x, game.player.y);
    });
});
