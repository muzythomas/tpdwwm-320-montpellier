import "./Task.css";

function Task({ task, completeTask, removeTask }) {
    //lorsqu'on réagit à un évènement, on peut choisir d'exécuter une des fonctions qui ont été données dans les props de notre Task
    //ici, ces fonctions sont completeTask et removeTask qui appartiennent à la todolist
    return (
        <div>
            <span
                onClick={() => completeTask(task)}
                className={`${task.complete ? "complete" : ""}`}
            >
                {task.label}
            </span>

            <button type="button" onClick={() => removeTask(task)}>
                X
            </button>
        </div>
    );
}

export default Task;
