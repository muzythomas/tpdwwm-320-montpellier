import React from "react";
import TodoInput from "./TodoInput";
import Task from "./Task";

function TodoList() {
    //useState permet de déclarer un état pour notre composant
    //il nous met à disposition une variable permettant de stocker une propriété de l'état (ici notre liste de taches)
    //ainsi qu'un moyen de la modifier (setTasks) qui permet de notifier React que l'état change, de façon à pouvoir déclencher un dessin du composant
    const [tasks, setTasks] = React.useState([
        //ici on définit un état par défaut de façon à pouvoir tester plus facilement
        { label: "Faire les courses", complete: true },
        { label: "Nourrir le chien", complete: false },
        { label: "Boire", complete: false },
    ]);

    //ces méthodes (addTask, completeTask, removeTask) sont là pour entreprendre de modifier l'état avec setTasks de façon à pouvoir prodiguer un moyen de modifier l'état de ToDoList à ses enfants
    const addTask = (taskLabel) => {
        const newTasks = [...tasks, { label: taskLabel, complete: false }];
        setTasks(newTasks);
    };

    const completeTask = (task) => {
        const completed = tasks.find((t) => t === task);
        completed.complete = !completed.complete;
        setTasks([...tasks]);
    };

    const removeTask = (task) => {
        const newTasks = tasks.filter((t) => t !== task);
        setTasks(newTasks);
    };

    const taskElements = tasks.map((task, index) => (
        //on donne les méthodes nécessaires à la modification de l'état dans les props (les attributs) de notre element Enfant
        //de façon à ce qu'il puisse les appeler dans le contexte de la ToDoList
        <Task
            removeTask={removeTask}
            completeTask={completeTask}
            task={task}
            key={index}
        />
    ));
    return (
        //dans le return on met en place le layout de notre ToDoList, elle comprend donc une liste de Task et un TaskInput
        <div className="TodoList">
            {taskElements}

            <TodoInput addTask={addTask} />
        </div>
    );
}

export default TodoList;
