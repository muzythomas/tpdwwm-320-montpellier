import React from "react";

function TodoInput({ addTask }) {
    //todoinput possède son propre état, qui permet de gérer la valeur du champ input
    const [inputValue, setInputValue] = React.useState("");

    //pour gérer l'ajout d'une tâche, on défère le comportement à la fonction addTask qui appartient à TodoList
    const handleSubmit = (event) => {
        event.preventDefault();
        addTask(inputValue);
        setInputValue("");
    };

    //pour que le champ input soit modifiable et puisse mettre à jour l'état au fur et a mesure de sa modification
    //on lui demande de réagir au changement en utilisant setInputValue, qui va changer notre valeur dans l'état du composant
    return (
        <form onSubmit={handleSubmit}>
            <input
                value={inputValue}
                onChange={(e) => setInputValue(e.target.value)}
                type="text"
            />
        </form>
    );
}

export default TodoInput;
