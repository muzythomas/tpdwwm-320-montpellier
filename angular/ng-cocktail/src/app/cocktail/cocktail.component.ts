import { Component, OnInit } from '@angular/core';
import { CocktailService } from "../services/cocktail.service";
@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.css']
})
export class CocktailComponent implements OnInit {

  constructor(private cs: CocktailService) { }

  results;
  ngOnInit(): void {
    this.cs.getAllCocktails().subscribe(data => this.results = data)
  }

}
