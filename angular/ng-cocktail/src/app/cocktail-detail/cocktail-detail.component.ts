import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CocktailService } from '../services/cocktail.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-cocktail-detail',
    templateUrl: './cocktail-detail.component.html',
    styleUrls: ['./cocktail-detail.component.css'],
})
export class CocktailDetailComponent implements OnInit {
    constructor(private route: ActivatedRoute, private cs: CocktailService, private location: Location) {}

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.getDetails(params.id)
        })
    }

    details;
    ingredients = []; 
    getDetails(id: string){
        this.cs.getCocktailById(id).subscribe(data => {
            this.details = data.drinks[0]

            //récupération des ingrédients
            for (let index = 1; index <= 15; index++) {
                let ingredientName = this.details["strIngredient"+index];
                let ingredientMeasure = this.details["strMeasure"+index];
                if (ingredientName && ingredientMeasure){
                    this.ingredients.push({name: ingredientName, measure: ingredientMeasure});
                }
            }
            console.log(this.details, this.ingredients)
        })
    }

    goBack(){
        this.location.back();
    }
}
