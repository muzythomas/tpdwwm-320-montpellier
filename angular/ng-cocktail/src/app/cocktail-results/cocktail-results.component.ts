import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CocktailService } from '../services/cocktail.service';

@Component({
  selector: 'app-cocktail-results',
  templateUrl: './cocktail-results.component.html',
  styleUrls: ['./cocktail-results.component.css']
})
export class CocktailResultsComponent implements OnInit {

  constructor(private route: ActivatedRoute, private cs: CocktailService) { }

  searchTerm = "";
  results;
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.cs.getCocktailByName(params.name).subscribe(data => this.results = data)
      this.searchTerm = params.name;
    })
  }

}
