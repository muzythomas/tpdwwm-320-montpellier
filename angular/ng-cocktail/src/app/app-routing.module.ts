import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CocktailComponent } from './cocktail/cocktail.component';
import { CocktailDetailComponent } from './cocktail-detail/cocktail-detail.component';
import { CocktailResultsComponent } from './cocktail-results/cocktail-results.component';

const routes: Routes = [
  { path: '', redirectTo: '/cocktails', pathMatch: 'full'},
  { path: '', redirectTo: '/cocktails/search', pathMatch: 'full'},
  {path: 'cocktails', component: CocktailComponent},
  {path: 'cocktails/:id', component: CocktailDetailComponent},
  {path: 'cocktails/search/:name', component: CocktailResultsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
