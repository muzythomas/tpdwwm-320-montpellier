import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.css']
})
export class CocktailListComponent implements OnInit {
  @Input() cocktails;
  constructor() { }

  ngOnInit(): void {
  }
      private emojis = [
        '😀', '😁', '😂', '🤣', '😃', '😄', '😅', '😆', '😉', '😊', '😋', '😎', '😍', '😘', '😗', '😙', '😚', '☺', '🙂', '🤗', '🍇', '🍈', '🍉', '🍊', '🍋', '🍌', '🍍', '🍎', '🍏', '🍐', '🍑', '🍒', '🍓', '🥝', '🍅', '🍶', '🍾', '🍷', '🍸', '🍹', '🍺', '🍻', '🥂', '🥃', '🌞', '⭐', '🌟', '🌠', '🔥', '💧', '🌊', '⛱', '🌈'
      ];
      
      getRandomEmoji(){
        return this.emojis[Math.floor(Math.random() * this.emojis.length)];
      };

}
