import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cocktail-search',
  templateUrl: './cocktail-search.component.html',
  styleUrls: ['./cocktail-search.component.css']
})
export class CocktailSearchComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  search(name: string){
    if (name){
      this.router.navigateByUrl('cocktails/search/'+name);
    }
  }

}
