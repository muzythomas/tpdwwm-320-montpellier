import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CocktailComponent } from './cocktail/cocktail.component';
import { HttpClientModule } from '@angular/common/http';
import { CocktailDetailComponent } from './cocktail-detail/cocktail-detail.component';
import { CocktailSearchComponent } from './cocktail-search/cocktail-search.component';
import { CocktailResultsComponent } from './cocktail-results/cocktail-results.component';
import { CocktailListComponent } from './cocktail-list/cocktail-list.component';
@NgModule({
    declarations: [AppComponent, CocktailComponent, CocktailDetailComponent, CocktailSearchComponent, CocktailResultsComponent, CocktailListComponent],
    imports: [BrowserModule, AppRoutingModule, HttpClientModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
