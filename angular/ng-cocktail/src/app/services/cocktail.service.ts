import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root',
})
export class CocktailService {
    constructor(private http: HttpClient) {}

    private apiKey = '1';
    private baseUrl = `https://www.thecocktaildb.com/api/json/v1/${this.apiKey}/`;
    private ACTIONS = {
        COCKTAIL_SEARCH_BY_NAME: 'search.php?s=',
        COCKTAIL_SEARCH_BY_FIRST_LETTER: 'search.php?f=',
        INGREDIENT_SEARCH_BY_NAME: 'search.php?i=',
        COCKTAIL_LOOKUP_BY_ID: 'lookup.php?i=',
        INGREDIENT_LOOKUP_BY_ID: 'lookup.php?iid=',
        LOOKUP_RANDOM_COCKTAIL: 'random.php',
        COCKTAIL_SEARCH_BY_INGREDIENT: 'filter.php?i=',
        COCKTAIL_FILTER_ALCOHOLIC: 'filter.php?a=Alcoholic',
        COCKTAIL_FILTER_NON_ALCOHOLIC: 'filter.php?a=Non_Alcoholic',
        COCKTAIL_FILTER_ORDINARY_DRINK: 'filter.php?c=',
        COCKTAIL_FILTER_COCKTAIL: 'filter.php?c=',
        COCKTAIL_FILTER_BY_GLASS: 'filter.php?g',
        LIST_GLASSES: 'list.php?c=list',
        LIST_CATEGORIES: 'list.php?g=list',
        LIST_INGREDIENTS: 'list.php?i=list',
    };

    private get(action: string, parameter: string = ""): Observable<any> {
        return this.http.get(`${this.baseUrl}${action}${parameter}`);
    }

    getCocktailByName(name: string): Observable<any> {
        return this.get(this.ACTIONS.COCKTAIL_SEARCH_BY_NAME, name);
    }

    getCocktailById(id: string): Observable<any>{
        return this.get(this.ACTIONS.COCKTAIL_LOOKUP_BY_ID, id);
    }

    getAllCocktails(): Observable<any>{
        return this.get(this.ACTIONS.COCKTAIL_FILTER_ALCOHOLIC);
    }

    getByGlass(glass: string): Observable<any>{
        return this.get(this.ACTIONS.COCKTAIL_FILTER_BY_GLASS, glass);
    }

}
