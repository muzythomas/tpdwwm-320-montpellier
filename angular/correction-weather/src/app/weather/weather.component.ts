import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-weather',
    templateUrl: './weather.component.html',
    styleUrls: ['./weather.component.css'],
})
export class WeatherComponent implements OnInit {
    constructor(private weatherService: WeatherService, private route: ActivatedRoute) {}

    currentData;

    forecastData;

    ngOnInit(): void {
      this.route.params.subscribe( params => {
        this.getForecast(params.lat, params.lon)
      });
    }
    
    getCurrent(city: string): void{
      this.weatherService.getCurrent(city).subscribe(data => this.currentData = data);
    }

    getForecast(lat:string, lon:string): void{
      this.weatherService.getForecast(lat, lon).subscribe(data => this.forecastData = data);
    }
}
