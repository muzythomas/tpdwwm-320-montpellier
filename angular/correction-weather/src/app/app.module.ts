import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { HttpClientModule } from '@angular/common/http';
import { GeoComponent } from './geo/geo.component';

import { registerLocaleData } from '@angular/common';
//les informations de langue sont stockées dans angular/common/locales/[code de langue]
import fr from "@angular/common/locales/fr";
//registerLocaleData permet d'enregistrer des informations de langue dans l'application 
registerLocaleData(fr);

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    GeoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    //permet de définir la locale par défaut sur Français
    { provide: LOCALE_ID, useValue: 'fr-FR'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
