import { Component, OnInit } from '@angular/core';
import { GeoService } from "../services/geo.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-geo',
  templateUrl: './geo.component.html',
  styleUrls: ['./geo.component.css']
})
export class GeoComponent implements OnInit {

  constructor(private geoService: GeoService, private router: Router) { }

  ngOnInit(): void {
  }
  
  getLocations(city: string){
    this.geoService.getLocations(city).subscribe(
      data => {
        this.router.navigateByUrl(`/weather/${data[0].lat}/${data[0].lon}`);
      }
    )
  }

}
