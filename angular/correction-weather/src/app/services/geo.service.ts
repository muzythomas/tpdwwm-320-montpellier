import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  constructor(private http: HttpClient) { }

  getLocations(city: string): Observable<Object>{
    return this.http.get<Object>(`https://nominatim.openstreetmap.org/search?format=json&q=${city}`)
  }
}
