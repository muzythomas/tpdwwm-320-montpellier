import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private apiKey: string = "a4abf199236c5bdd5155eeb8ce48a37a";
  constructor(private http: HttpClient) { }

  getCurrent(city: string): Observable<Object>{
    return this.http.get<Object>(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${this.apiKey}&units=metric&lang=fr`);
  }

  getForecast(lat: string, lon: string): Observable<Object>{
    return this.http.get<Object>(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=minutely&units=metric&lang=fr&appid=${this.apiKey}`)
  }
}
