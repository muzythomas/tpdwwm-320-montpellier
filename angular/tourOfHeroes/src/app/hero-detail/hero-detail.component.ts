import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  id: number;
  hero: Hero;

  constructor(private route : ActivatedRoute, private heroService: HeroService) { }

  ngOnInit(): void {
    //pour récuperer l'id dans la route on utilise paramMap qui contient les paramètres
    //snapshost permet de prendre une "photo" de la route au moment où le component est chargé
    //de façon à être sûr d'avoir la route initiale, et pas une route potentiellement modifiée
    //le + au début convertis la string en number
    this.id = +this.route.snapshot.paramMap.get('id');
    //on va chercher le Hero dont l'id correspond à l'id récupéré
    this.getHero();
  }

  getHero(){
    this.heroService.getHero(this.id).subscribe((data) => this.hero = data);
  }

  save(){
    this.heroService.updateHero(this.hero).subscribe( () => console.log(this.hero, "enregistré")); 
  }
}
