//on crée une interface pour organiser les données de nos héros 
//export signifie que cette interface sera disponible à l'import dans toute l'application 
export interface Hero {
    id: number;
    name: string;
}