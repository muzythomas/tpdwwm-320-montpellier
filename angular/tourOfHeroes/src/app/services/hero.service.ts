import { Injectable } from '@angular/core';
import { Hero } from '../hero';

import { Observable } from 'rxjs';

//permet d'effectuer des requêtes http
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private http: HttpClient) { }

  getHeroes(): Observable<Hero[]>{
    //on effectue une requête get vers l'uri api/heroes 
    //et on indique à get que les données vont être sous la forme de tableau de Hero
    return this.http.get<Hero[]>("api/heroes");
  }

  getHero(id: number): Observable<Hero>{
    //on effectue une requête get vers la route api/heroes/id
    return this.http.get<Hero>(`api/heroes/${id}`);
  }

  updateHero(hero: Hero): Observable<any>{
    //HTTP PUT permet de remplacer une ressource avec une ressource du même type
    //utile pour mettre à jour une donnée
    return this.http.put("api/heroes", hero);
  }

  createHero(hero: Hero): Observable<Hero>{
    //pour créer une ressource on utilise communément POST 
    //dans les API Web, il est coutumier de la part du serveur de renvoyer la ressource qui vient d'être créée 
    return this.http.post<Hero>("api/heroes", hero);
  }

  deleteHero(hero: Hero): Observable<any> {
    return this.http.delete(`api/heroes/${hero.id}`);
  }
  
}
