import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {

  constructor() { }

  createDb() {
    let heroes =  [
      { id : 1, name : 'Patman'},
      { id : 2, name : 'Chocomousseman'},
      { id : 3, name : 'Zimmerman'},
      { id : 4, name : 'Goldman'},
      { id : 5, name : 'Strauss-man'},
      { id : 6, name : 'Dr Manathan'},
      { id : 7, name : 'Guy Carlier'},
    ];
    return {heroes};
  }
}
