import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[];

  //en indiquant la présence d'un service dans le constructor de notre component
  //on indique à angular la nécessiter d'injecter ledit service dans une propriété 
  //pour pouvoir l'utiliser 
  //en précisant private, angular rangera automatiquement le service dans une propriété privée qu'on pourra utiliser avec this.heroService 
  constructor(private heroService: HeroService) { }

  //ngOnInit est un "hook" qui permet de lancer du code lors de l'initialisation du composant
  //il existe d'autres hooks, l'équivalent de DOMContentLoaded serait par exemple ngAfterViewInit()
  ngOnInit(): void {
    //au lancement de notre composant on va chercher les heroes
    this.getHeroes();
  }

  getHeroes(): void{
    //en souscrivant à l'observable, on peut définir un observer (une fonction) 
    //définissant ainsi le comportant à adopter lorsque la donnée sera reçue
    //cela nous permet de gérer un comportement asynchrone de la part de notre service
    this.heroService.getHeroes().subscribe( (data) => this.heroes = data );
  }

  addHero(name: string): void {
    name = name.trim();//trim retire les espaces avant et après une string

    if (!name) {
      return;
    }

    //on envoie au service un objet contenant le nom de notre Hero
    //cependant, le service s'attend à recevoir un objet de type Hero 
    //donc on lui précise de considérer cette objet contenant le nom comme un Hero
    this.heroService.createHero({ name } as Hero).subscribe( 
      hero => this.heroes.push(hero) //lorsque le Hero créé nous revient, on l'ajoute a la liste pour mise à jour 
    );
    //si on voulait avoir un retour immédiat sur notre action
    //on pourrait forcer l'illusion avec un ajout en local direct, pour ne pas avoir la latence de la requête
    //this.heroes.push({id: this.heroes.length+1, name: name});
  }

  deleteHero(hero: Hero): void {    
    this.heroService.deleteHero(hero).subscribe(
      () => this.heroes = this.heroes.filter(heroItem => heroItem.id !== hero.id)
    );
  }

}
