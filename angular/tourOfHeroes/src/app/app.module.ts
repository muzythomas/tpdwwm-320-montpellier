import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';

//nécessaire pour utiliser certaines fonctionnalités de formulaire
//dont ngModel
import { FormsModule } from '@angular/forms';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';

//HttpClientModule permet a l'application d'utiliser des fonctionnalités en rapport avec le protocole HTTP, comme par exemple la gestion des requêtes (get, post, etc...)
import { HttpClientModule } from '@angular/common/http';

//HttpClientInMemoryWebApiModule permet de simuler un serveur d'api stocké dans la mémoire de l'application pour pouvoir tester des requêtes HTTP comme si un véritable serveur API existait 
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './services/in-memory-data.service';

@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    //le HttpClientInMemoryWebApiModule interceptera les requêtes HTTP de l'application angular
    //et sevira à la place les données définies dans le inMemoryDataService. 
    //Pour ça, il faut préciser au httpClientInMemoryWebApiModule qu'il faut utiliser le InMemoryDataService
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService
    ),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
