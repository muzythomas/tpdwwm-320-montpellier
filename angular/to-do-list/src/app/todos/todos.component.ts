import { Component, OnInit } from '@angular/core';
import { Task } from '../task';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  tasks : Task[] = [];
  taskCount : number = 0;
  constructor() { }

  ngOnInit(): void {
  }

  addTask(task: string){
    //taskCount nous permet d'attributer des id uniques autoincrement a nos tasks
    this.taskCount++;
    //a chaque ajout on rajoute un objet Task contenant un id et un label 
    this.tasks.push({id: this.taskCount, label: task, complete: false});
  }

  deleteTask(task: Task){
    //finIndex permet de récupérer l'index d'un élément de tableau avec un prédicat
    //ce prédicat est une façon de préciser le filtre selon lequel on veut trouver l'élément
    //ici, on demande à chercher l'élément de tableau dont l'id correspond à l'id de notre task
    let index = this.tasks.findIndex((item) => item.id === task.id);
    //une fois qu'on a l'index, il ne nous reste plus qu'à appeler splice
    this.tasks.splice(index, 1);
  }

  completeTask(task: Task){
    task.complete = !task.complete;
  }
}
