import { Component, OnInit } from '@angular/core';
import { CocktailService } from "../services/cocktail.service";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cocktail',
  templateUrl: './cocktail.component.html',
  styleUrls: ['./cocktail.component.css']
})
export class CocktailComponent implements OnInit {

  constructor(private cs: CocktailService, private route: ActivatedRoute) { }

  drinks; 
  ngOnInit(): void {
    //pour être au courant de changement de paramètres sur une même route 
    //il faut souscrire à l'observable params de route
    this.route.params.subscribe( parameters => {
      //a chaque fois que les paramètres on les récupère et on les utilise pour notre page
      const name = parameters.name;
      this.getByName(name);
    })
  }
  
  getByName(name: string){
    this.cs.getByName(name).subscribe(data => {
      this.drinks = data.drinks;
      console.log(this.drinks);
    });
  }

}
