import { Component, OnInit } from '@angular/core';
import { CocktailService } from '../services/cocktail.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cocktail-detail',
  templateUrl: './cocktail-detail.component.html',
  styleUrls: ['./cocktail-detail.component.css']
})
export class CocktailDetailComponent implements OnInit {

  constructor(private cs: CocktailService, private route: ActivatedRoute, private location: Location) { }

  drink;
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.cs.getById(id).subscribe(data => {
      console.log(data)
      //ici, on récupère directement notre drink en allant dans le tableau drinks de nos données
      this.drink = data.drinks[0];
    })
  }

  goBack(){
    //location est un service permettant de gérer l'endroit de l'application où l'on se trouve
    //la méthode back() permet ainsi de revenir une page en arrière 
    this.location.back();
  }
}
