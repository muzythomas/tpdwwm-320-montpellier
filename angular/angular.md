# Démarrage d'un projet Angular

## Pré-requis

* installer [nodejs](https://nodejs.org/en/)

## Installation d'Angular

Pour lancer un projet angular il faut d'abord installer `angular-cli` via `npm`.

```
    npm install -g @angular/cli
```


#### note: 
Pour lancer une commande venant d'une source externe depuis `powershell`, il faut autoriser les sources externes signées au niveau du système. Dans une fenêtre powershell ouvertes en tant qu'**administrateur** lancer la commande suivante : 
```powershell
    set-executionpolicy remotesigned
```
Et valider la commande pour tous (`T`);


Une fois `angular-cli` installé, la commande `ng` devient accesible.
`ng` permet d'accéder à plusieurs outils de gestion de projets angular.

## Création du projet

Pour créer un nouveau projet via `ng` on utilise la commande suivante :

```
    ng new nom-du-projet
```

Puis, une fois la commande executée, vous pouvez lancer le serveur : 
```
    cd nom-du-projet
    ng serve --open
```
`ng serve` (peut être abrégé en `ng s`) compile l'application, et lance le serveur. L'option `--open` permet d'ouvrir directement une fenêtre de navigateur vers l'adresse du serveur (par défaut `localhost:4200`).
Le port peut être précisé à l'aide de l'option `--port`.

## Composition d'un projet

Un projet angular possède un dossier `src` contenant le code source de l'application.

Dans ce dossier `src`, on retrouve le dossier `app` dans lequel les différents composants dde l'application sont stockés, ainsi qu'un dossier `assets` permettant de stocker des ressources comme des images, enfin un `index.html` qui représente la base du document, un `styles.css` représentant la feuille de style globale de l'application. 

## Création d'un Component

Angular fonctionne selon l'architecture `MVW` (Model View Whatever), qui est un équivalent de Model View Controller, et sépare les différentes partie de l'application dans des `components`. 

Chaque `component` possède un template (html) et un fichier `typescript` contenant la logique applicative du `component`. 
Par défaut, ces fichiers se nomment `nom-du-component.component.html` et `nom-du-component.component.ts.`. Chaque `component` possède aussi sa propre feuille de style `nom-du-component.component.css`.

De base, un component `app.component` est créé, et sert de point d'entrée, et fondation, et de notre application. 

Pour créer un autre `component` on peut utiliser la commande suivante :
```
    ng generate component nom-du-component
```

## Présentation d'un component 

Un component se présente de base sous la forme suivante : 

```ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.css']
})
export class ExampleComponent implements OnInit {
    constructor() {}
    
    ngOnInit(){}
}
```

Chaque component est définit dans une classe et possède la notation 
`@Component` au dessus de sa déclaration qui permet de préciser certains paramètres.
`selector` permet de définir quel est le nom global du component dans l'application. 
`templateUrl` permet de définir où est stocké le template lié au component. 
`styleUrls` est un tableau d'uri menant aux feuilles de style du component. 

Si le component implémente `OnInit`, il doit posséder une méthode `ngOnInit` qui sert de "[lifecycle Hook](https://angular.io/guide/lifecycle-hooks)" pour exécuter du code lors de l'initialisation du component (équivalent d'un eventListener sur `load`).
Pour avoir l'équivalent de `DOMContentLoaded` on utilisera plutôt `ngAfterViewInit`. 

Si une propriété est définie de façon publique dans le fichier .ts d'un component, cette propriété est accessible en tant que variable dans le template de celui ci. 

Exemple, si dans le component on définit une variable `foo` de type `string`
```ts
export class ExampleComponent implements OnInit {

    foo: string = "Bar"; 

    constructor() {}
    
    ngOnInit(){}
}
```

on peut l'utiliser dans le template de cette façon : 

```html 
    <h2>{{foo}}</h2>
```

Qui affichera ainsi à l'écran "Bar";


## Présentation du Moteur de Vues 

### [Displaying Data In Views - Angular Documentation](https://angular.io/guide/displaying-data)

Angular possède un moteur de template permettant de décrire de la logique dans l'établissement de notre document HTML, comme des boucles ou des conditions. 

Par exemple, pour utiliser un `for` permettant de répeter un certain élément autant de fois qu'il y a d'objets dans un tableau `items` : 

```html
    <div *ngFor="let item of items">
        {{item.label}}
    </div>
```

Ce qui répetera la div contenant le label de chaque `item` contenu dans le tableau `items`.

Pour utiliser `if` on peut dicter l'existence ou non d'un élément en lui donnant un attribut `*ngIf` :

```html
    <div *ngIf="machin">
        {{machin.bidule}}
    </div>
```

Si machin existe, la div s'affichera ainsi que son contenu, sinon la div ne sera pas créée. 


## Utilisation des services

Un service dans Angular sert à prodiguer des fonctionnalités ou des données à plusieurs components. 
Pour créer un service on peut appeler la commande suivante : 

```
    ng generate service nom-du-service
```

Dans le fichier `nom-du-service.service.ts` on peut ensuite écrire la logique du service. 


Pour appeler un service, il faut d'abord qu'il soit enregistré en tant que service auprès d'Angular. En utilisant la commande `ng g s`, cela est déjà fait par défaut. 
Sinon, pour ce faire il faut préciser dans le code du service :

```ts
@Injectable({
  providedIn: 'root'
})
export class ExampleService {
}
```

La mention `@Injectable` indique à Angular que le service peut être utilisé via injection de dépendances. 


Dans un component, on peut ensuite appeler le service de la façon suivante: 

```ts
export class ExampleComponent implements OnInit {
    //l'injection du service se fait directement dans le constructeur
    //la mention private en fait une propriété privée du component
    constructor(private exampleService: ExampleService) {}
    
    ngOnInit(){}
}
```

Ensuite pour utiliser le service dans le component on utilise `this.exampleService` et ses méthodes. 


## Routage 

Pour mettre en place le routage, si on a dit "oui" lors de la création du projet à la question `Would You Like To Implement Angular Routing` il n'y a rien de plus à faire. 

Sinon, on doit suivre la procédure suivante : [Add The App Routing Module - Tour of Heroes Tutorial](https://angular.io/tutorial/toh-pt5#add-the-approutingmodule).

Une fois le module installé, on possède un fichier a la racine de notre dossier `app` qui s'appelle `app-routing.module.ts`. 


```ts
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

Pour rajouter une route dans notre application, on rajoute un objet `Route` dans le tableau contenu dans la constante `routes`. 

Une `Route` contient obligatoirement un `path` qui est l'uri qu'on veut faire correspondre à un `component`. 
Exemple : 
```ts
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExampleComponent } from './example/example.component';
const routes: Routes = [
  { path: 'example', component: ExampleComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```
Pour effectuer une redirection on définit une `Route` avec un `path` mais pas de `component`, au lieu de ça on indique une redirection vers une autre `Route`.
```ts
{ path: '', redirectTo: '/example', pathMatch: 'full'},
```

`pathMatch` indique comment la détection de route doit prendre en compte la recherche d'une correspondance. 
Si `pathMatch` est définit à `prefix`, cela veut dire que le `path` représente un début de route, et matchera donc toute route commençant par le `path` indiqué.
Si on utilise `full` on oblige la route à correspondre à 100% au `path` indiqué. 

### Utilisation d'une route dans un template :

Pour produire un lien menant vers un component dans un template, on utilise l'attribut `routerLink` :

```html
    <a routerLink="/example">Lien vers ExampleComponent </a>
```



## Que faire ensuite ? 

Lire la [documentation officielle](https://angular.io/docs#introduction-to-the-angular-docs).

Suivre le guide [Tour Of Heroes](https://angular.io/tutorial#tour-of-heroes-app-and-tutorial).

[Glossaire des termes Angular](https://angular.io/guide/glossary)