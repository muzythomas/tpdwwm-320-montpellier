import { Component, OnInit } from '@angular/core';
import { GeoService } from "../services/geo.service";
@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.css']
})
export class CitySearchComponent implements OnInit {

  cityName = "Montpellier";
  cities = [];

  constructor(private geo: GeoService) { }

  ngOnInit(): void {
    this.getCity()
  }

  getCity(){
    if (this.cityName){
      this.geo.getCity(this.cityName).subscribe( data => this.cities = data);
    }
  }

}
