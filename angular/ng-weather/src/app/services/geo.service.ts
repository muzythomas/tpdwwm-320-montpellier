import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeoService {

  apiUrl = "https://nominatim.openstreetmap.org/search?format=json&q="
  constructor(private http: HttpClient) {}

  getCity(name: string): Observable<Object[]>{
    return this.http.get<Object[]>(this.apiUrl + name);
  }
}
