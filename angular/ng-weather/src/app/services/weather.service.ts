import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {OldCurrentWeather} from '../model/OldCurrentWeather';

import { OneCallForecast, CurrentForecast, HourlyForecast, DailyForecast } from '../model/Forecast';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  apiKey: string = "a4abf199236c5bdd5155eeb8ce48a37a";
  apiUrl: string = `http://api.openweathermap.org/data/2.5/weather?appid=${this.apiKey}&units=metric&lang=fr&q=`;
  oneCallApiUrl: string = `https://api.openweathermap.org/data/2.5/onecall?appid=${this.apiKey}&units=metric`;
  constructor(private http: HttpClient) { }

  /**
   * @deprecated
   * @param city 
   */
  oldGetCurrent(city: string): Observable<OldCurrentWeather>{
    return this.http.get<OldCurrentWeather>(`${this.apiUrl}${city}`);
  }

  getOneCall(lat: string, lon: string): Observable<OneCallForecast>{
    return this.http.get<OneCallForecast>(`${this.oneCallApiUrl}&lat=${lat}&lon=${lon}`)
  }

}
