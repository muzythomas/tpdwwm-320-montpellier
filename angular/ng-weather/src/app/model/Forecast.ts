export interface OneCallForecast {
    current: CurrentForecast;
    daily: DailyForecast[];
    hourly: HourlyForecast[];
    lat: number;
    lon: number;
    timezone: string;
    timezone_offset: number;
}

export interface CurrentForecast {
    clouds: number;
    dew_point: number;
    dt: number;
    feels_like: number;
    humidity: number;
    pressure: number;
    sunrise: number;
    sunset: number;
    temp: number;
    uvi: number;
    visibility: number;
    weather: Weather[];
    wind_deg: number;
    wind_speed: number;
}

export interface HourlyForecast {
    dt:number,
      temp:number,
      feels_like:number,
      pressure:number,
      humidity:number,
      dew_point:number,
      clouds:number,
      visibility:number,
      wind_speed:number,
      wind_deg:number,
      weather: Weather[],
      pop: number,
}


export interface DailyForecast {
        dt: number,
        sunrise: number,
        sunset: number,
        temp: {
          day: number,
          min: number,
          max: number,
          night: number,
          eve: number,
          morn: number
        },
        feels_like: {
          day: number,
          night: number,
          eve: number,
          morn: number
        },
        pressure: number,
        humidity: number,
        dew_point: number,
        wind_speed: number,
        wind_deg: number,
        weather: Weather[],
        clouds: number,
        pop: number,
        uvi: number
}

export interface Weather {
    description: string;
    icon: string;
    id: number;
    main: string;
}