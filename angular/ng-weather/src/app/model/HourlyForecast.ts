import {Weather} from './Weather';
export interface HourlyForecast {
    dt:number,
      temp:number,
      feels_like:number,
      pressure:number,
      humidity:number,
      dew_point:number,
      clouds:number,
      visibility:number,
      wind_speed:number,
      wind_deg:number,
      weather: Weather[],
      pop: number,
}