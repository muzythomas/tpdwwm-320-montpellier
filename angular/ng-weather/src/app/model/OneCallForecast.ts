import { CurrentForecast, DailyForecast, HourlyForecast } from '../model/Forecast';

export interface OneCallForecast {
    current: CurrentForecast;
    daily: DailyForecast[];
    hourly: HourlyForecast[];
    lat: number;
    lon: number;
    timezone: string;
    timezone_offset: number;
}
