import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { ActivatedRoute } from "@angular/router";

import { CurrentForecast, DailyForecast, HourlyForecast } from '../model/Forecast';
import { Chart, ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  current: CurrentForecast = null;
  daily: DailyForecast[] = [];
  hourly: HourlyForecast[] = [];


  dailyChart = {
    lineChartData: [
      { data: [], label: 'Temperatures' },
    ],
  
    lineChartLabels: [],
  
    lineChartOptions : {
      responsive: true,
    },
  
    lineChartColors:  [
      {
        borderColor: 'black',
        backgroundColor: 'rgba(255,255,0,0.28)',
      },
    ],
  
    lineChartLegend : true,
    lineChartPlugins : [],
    lineChartType : 'line',
  }

  hourlyChart = {
    lineChartData: [
      { data: [], label: 'Temperatures' },
    ],
  
    lineChartLabels: [],
  
    lineChartOptions : {
      responsive: true,
    },
  
    lineChartColors:  [
      {
        borderColor: 'black',
        backgroundColor: 'rgba(255,255,0,0.28)',
      },
    ],
  
    lineChartLegend : true,
    lineChartPlugins : [],
    lineChartType : 'line',
  }

  constructor(private weatherService: WeatherService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe( routeParams => {
      this.getForecast(routeParams.lat, routeParams.lon);
    })
  }

  
  getForecast(lat: string, lon: string): void{
    this.weatherService.getOneCall(lat, lon).subscribe(
      (data) => {
        console.log(data);
        this.current = data.current;
        this.daily = data.daily;
        this.hourly = data.hourly;

        this.hourlyChart.lineChartData[0].data = this.hourly.map(x => x.temp);
        this.hourlyChart.lineChartLabels = this.hourly.map(x =>  new Date(x.dt * 1000).getHours() + "h");

        this.dailyChart.lineChartData[0].data = this.daily.map(x => x.temp.max);
        this.dailyChart.lineChartLabels = this.daily.map(x =>  new Date(x.dt * 1000).toLocaleDateString("fr-FR"));
      }
    )
  }
  
  /**
   * @deprecated
   */
  oldGetCurrentWeather(){
    
    this.weatherService.oldGetCurrent("").subscribe( (res) => {
      console.log(res);
    });
  }
}
